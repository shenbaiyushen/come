let data = {
	"code": 0,
	"data": {
		"batch_buy_option": [],
		"buy_option": [
			{
				"list": [
					{
						"icon_color": "",
						"icon_img": "",
						"is_stock": false,
						"name": "8GB+128GB",
						"price": "2399元",
						"prop_desc": "",
						"prop_value_id": 883
					},
					{
						"icon_color": "",
						"icon_img": "",
						"is_stock": false,
						"name": "12GB+128GB",
						"price": "2699元",
						"prop_desc": "",
						"prop_value_id": 882
					},
					{
						"icon_color": "",
						"icon_img": "",
						"is_stock": true,
						"name": "12GB+256GB",
						"price": "3299元起",
						"prop_desc": "",
						"prop_value_id": 884
					}
				],
				"name": "版本",
				"prop_cfg_id": 8
			},
			{
				"list": [
					{
						"icon_color": "",
						"icon_img": "",
						"is_stock": false,
						"name": "银翼",
						"price": "",
						"prop_desc": "",
						"prop_value_id": 10
					},
					{
						"icon_color": "",
						"icon_img": "",
						"is_stock": false,
						"name": "暗影",
						"price": "",
						"prop_desc": "",
						"prop_value_id": 3
					},
					{
						"icon_color": "",
						"icon_img": "",
						"is_stock": false,
						"name": "冰斩",
						"price": "",
						"prop_desc": "",
						"prop_value_id": 4
					},
					{
						"icon_color": "",
						"icon_img": "",
						"is_stock": false,
						"name": "冠军版",
						"price": "",
						"prop_desc": "",
						"prop_value_id": 1
					}
				],
				"name": "颜色",
				"prop_cfg_id": 2
			}
		],
		"default_goods_id": "2220600009",
		"goods_info": [
			{
				"action_button": {
					"bigtap_source": "bigtap",
					"button_title": "等待到货",
					"is_bigtap": false,
					"mode_change_time": 0,
					"mode_end_time": 0,
					"mode_start_time": 1668051600,
					"occ_timeout": 5000,
					"sale_mode": "webview",
					"sale_mode_line": "first",
					"sale_mode_title": "立即购买",
					"source": "common",
					"use_occ": false,
					"webview": {
						"is_hide_stock": true,
						"url": ""
					}
				},
				"address_type": "common",
				"class_parameters": {
					"list": [
						{
							"bottom_title": "骁龙8 Gen 1",
							"icon": "https://cdn.cnbj0.fds.api.mi-img.com/b2c-mimall-media/f0c04e138bfed2b1ebb589de615236d1.png",
							"is_page_show": true,
							"name": "CPU型号",
							"top_title": "CPU",
							"value": "骁龙8 Gen 1"
						},
						{
							"name": "CPU主频",
							"value": "最高3.0GHz"
						},
						{
							"bottom_title": "6400万像素+800万像素+200万像素",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/7692726e7a1dd34a3b1b4ede8aca020d.png",
							"is_page_show": true,
							"name": "后置摄像头",
							"top_title": "三摄像头",
							"value": "6400万像素+800万像素+200万像素"
						},
						{
							"name": "前置摄像头",
							"value": "2000万像素"
						},
						{
							"name": "屏幕",
							"value": "OLED"
						},
						{
							"bottom_title": "6.67英寸",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/86a3bd46cf4f7f19daa2c3250cf30604.png",
							"is_page_show": true,
							"name": "屏幕尺寸",
							"top_title": "超大屏",
							"value": "6.67英寸"
						},
						{
							"bottom_title": "2400×1080",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/a5ab24dcb527e49f970f13b11e000ab1.png",
							"is_page_show": true,
							"name": "屏幕分辨率",
							"top_title": "屏幕分辨率",
							"value": "2400×1080"
						},
						{
							"bottom_title": "最高12GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/c8ec0829241324e401744da627482560.png",
							"is_page_show": true,
							"name": "运行内存",
							"top_title": "极速畅玩",
							"value": "最高12GB"
						},
						{
							"bottom_title": "最高256GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/8941adac25333e785b9cc78ca11f4f27.png",
							"is_page_show": true,
							"name": "存储容量",
							"top_title": "存储容量",
							"value": "最高256GB"
						},
						{
							"name": "NFC",
							"value": "支持"
						},
						{
							"name": "红外遥控",
							"value": "支持"
						},
						{
							"name": "独立AI键",
							"value": "磁动力弹出式肩键 2.0"
						},
						{
							"name": "指纹识别",
							"value": "侧边指纹"
						},
						{
							"bottom_title": "8.5mm",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/52ad10a73685342e437e44ea3d29cbff.png",
							"is_page_show": true,
							"name": "机身厚度",
							"top_title": "普通厚度",
							"value": "8.5mm"
						},
						{
							"bottom_title": "4700mAh",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/0b4ea0fb21dde2f29df3c20de73539b9.png",
							"is_page_show": true,
							"name": "电池容量",
							"top_title": "超长待机",
							"value": "4700mAh"
						},
						{
							"name": "有线快充",
							"value": "120W"
						},
						{
							"bottom_title": "5G全网通7.0",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/d1b67a407fb2a1ed42c2c0ce15af3318.png",
							"is_page_show": true,
							"name": "网络类型",
							"top_title": "运营商网络",
							"value": "5G全网通7.0"
						},
						{
							"bottom_title": "双卡双待",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/bfd5ba9ae72c365dee42db14dfae4b0f.png",
							"is_page_show": true,
							"name": "网络模式",
							"top_title": "网络模式",
							"value": "双卡双待"
						},
						{
							"name": "数据接口",
							"value": "Type-C"
						}
					],
					"name": "关键参数"
				},
				"commodity_id": 1220600013,
				"customize_template_id": 0,
				"discount_price": "",
				"ext_buy_option": [],
				"gallery_v3": [
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/7ba11fdfecac708e7dcc3b98b537a83a.jpeg",
						"img_url_webp": "",
						"type": "video"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/5061b08df9de9ca1c23ba97b58a340c8.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/cdd534269f9326beaa4a493eab827070.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/22bbbda34d4535698318fc9cf40d0334.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bb7e0537300b002657566cd68920f176.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c090f46e0d07ed6db835fe8f96cf41c1.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0542966b26815adcb450e47b7bca70cc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/eab9a225caeae1c6c9f0673915ad295e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/a9ccfcac5415437e23127e1219966a44.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/1d1d0ab8b94208e69ec929f4d95b3c17.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8348f5f8d83643c9fcfc16dd240c4adc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bd48dcfd217a949798f2748a94bb0b41.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/93d64b29aa8aaff8fc9db0e9a665ad9e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3e3d8b8212a6cc53d5db756531e1ba1d.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0ce722c3e15ed76a9654c20fca0c7faf.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/e85d8a06e1b0ea43d3b8fa397aaf5ebc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/68bb35c26ff3f358362652df9b132d2a.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/577d8e3b1ba50a967e423c8562654b57.jpg",
						"img_url_webp": "",
						"type": "img"
					}
				],
				"goods_id": 2220600009,
				"image_share": "http://cdn.cnbj1.fds.api.mi-img.com/mi-mall/8ec65803d049f8d68b6a092fb051590a.png",
				"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8ec65803d049f8d68b6a092fb051590a.png",
				"is_customize": false,
				"is_sale": true,
				"is_stock": false,
				"market_price": "3299",
				"multi_buy_limit": {
					"first": 10
				},
				"multi_instalment": {
					"first": {
						"default_instal": "antinstal_m",
						"list": [
							{
								"Bank": "mifinanceinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 73.24,
										"is_differential_rate": false,
										"rate": 2.22,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1124.08,
										"stage_interest": 24.41,
										"tab_detail": "手续费24.41元/期起，费率2.22%起，单利年化利率约为13.32%",
										"tab_title": "1124.08元x3期",
										"total_cost": 3372.24
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 128.66,
										"is_differential_rate": false,
										"rate": 3.9,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 571.27,
										"stage_interest": 21.44,
										"tab_detail": "手续费21.44元/期起，费率3.9%起，单利年化利率约为13.37%",
										"tab_title": "571.27元x6期",
										"total_cost": 3427.66
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 237.53,
										"is_differential_rate": false,
										"rate": 7.2,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 294.71,
										"stage_interest": 19.79,
										"tab_detail": "手续费19.79元/期起，费率7.2%起，单利年化利率约为13.29%",
										"tab_title": "294.71元x12期",
										"total_cost": 3536.53
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 593.82,
										"is_differential_rate": false,
										"rate": 18,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 162.2,
										"stage_interest": 24.74,
										"tab_detail": "手续费24.74元/期起，费率18%起，单利年化利率约为17.28%",
										"tab_title": "162.2元x24期",
										"total_cost": 3892.82
									}
								],
								"Installments": null,
								"Title": "小米分期"
							},
							{
								"Bank": "antinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 75.88,
										"is_differential_rate": true,
										"rate": 2.3,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1124.95,
										"stage_interest": 25.29,
										"tab_detail": "手续费25.29元/期起，费率2.3%起，单利年化利率约为13.70%",
										"tab_title": "1124.95元x3期",
										"total_cost": 3374.88
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 148.46,
										"is_differential_rate": true,
										"rate": 4.5,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 574.57,
										"stage_interest": 24.74,
										"tab_detail": "手续费24.74元/期起，费率4.5%起，单利年化利率约为15.30%",
										"tab_title": "574.57元x6期",
										"total_cost": 3447.46
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 247.42,
										"is_differential_rate": true,
										"rate": 7.5,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 295.52,
										"stage_interest": 20.61,
										"tab_detail": "手续费20.61元/期起，费率7.5%起，单利年化利率约为13.60%",
										"tab_title": "295.52元x12期",
										"total_cost": 3546.42
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 412.38,
										"is_differential_rate": true,
										"rate": 12.5,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 154.63,
										"stage_interest": 17.18,
										"tab_detail": "手续费17.18元/期起，费率12.5%起，单利年化利率约为13.80%",
										"tab_title": "154.63元x24期",
										"total_cost": 3711.38
									}
								],
								"Installments": null,
								"Title": "花呗分期"
							}
						]
					}
				},
				"multi_instalment_info": {
					"first": {
						"detail_info": "低至143.45元 x 24期"
					}
				},
				"multi_insurance": {
					"first": {
						"list": [
							{
								"goods_id": 2220600025,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548202.10119327.jpg",
								"insurance_type": "accident",
								"market_price": 399,
								"name": "Redmi K50 电竞版 意外保障服务",
								"onsale_price": 0,
								"price": 399,
								"short_name": "Redmi K50 电竞版 意外保障服务",
								"sku": 40962,
								"type": "2"
							},
							{
								"goods_id": 2220700001,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644809331.80464581.jpg",
								"insurance_type": "micare",
								"market_price": 599,
								"name": "Redmi K50 电竞版 MiCare保障服务",
								"onsale_price": 0,
								"price": 599,
								"short_name": "Redmi K50 电竞版 MiCare保障服务",
								"sku": 41034,
								"type": "2"
							},
							{
								"goods_id": 2220600026,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548371.81671372.jpg",
								"insurance_type": "prolong",
								"market_price": 119,
								"name": "Redmi K50 电竞版 延长保修服务",
								"onsale_price": 59,
								"price": 59,
								"short_name": "Redmi K50 电竞版 延长保修服务",
								"sku": 40964,
								"type": "2"
							},
							{
								"goods_id": 2220600024,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548009.85713550.jpg",
								"insurance_type": "accident",
								"market_price": 249,
								"name": "Redmi K50 电竞版 碎屏保障服务",
								"onsale_price": 124,
								"price": 124,
								"short_name": "Redmi K50 电竞版 碎屏保障服务",
								"sku": 40963,
								"type": "2"
							}
						]
					}
				},
				"multi_market_price": {
					"first": "3299",
					"second": "3299"
				},
				"multi_price": {
					"first": "2399",
					"second": "2399"
				},
				"multi_service_bargins": {
					"first": [
						{
							"can_fold": true,
							"mutex_type": [
								"service_insurance",
								"service_prolong"
							],
							"service_info": [
								{
									"act_diff": "碎屏延保换新",
									"goods_id": 2220600009,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D41034%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D41034%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "享碎屏、延保、换电池、保值换新4大权益",
									"service_goods_id": "2220700001",
									"service_name": "Redmi K50 电竞版 MiCare保障服务",
									"service_price": "599",
									"service_short_name": "MiCare保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
									"source": "common"
								}
							],
							"service_type_name": "service_micare",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
							"type_name": "尊享服务"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "意外损免费修",
									"goods_id": 2220600009,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40962%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40962%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次意外损坏 官方原厂 免费维修",
									"service_goods_id": "2220600025",
									"service_name": "Redmi K50 电竞版 意外保障服务",
									"service_price": "399",
									"service_short_name": "意外保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
									"source": "common"
								},
								{
									"act_diff": "已省125元",
									"act_price": "124",
									"goods_id": 2220600009,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40963%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40963%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次碎屏 官方原厂 免费维修",
									"service_goods_id": "2220600024",
									"service_name": "Redmi K50 电竞版 碎屏保障服务",
									"service_price": "124",
									"service_short_name": "碎屏保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40342",
									"source": "common"
								}
							],
							"service_type_name": "service_insurance",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
							"type_name": "意外保护"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "已省60元",
									"act_price": "59",
									"goods_id": 2220600009,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40964%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40964%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "性能故障 官方原厂 多次免费维修",
									"service_goods_id": "2220600026",
									"service_name": "Redmi K50 电竞版 延长保修服务",
									"service_price": "59",
									"service_short_name": "延长保修服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
									"source": "common"
								}
							],
							"service_type_name": "service_prolong",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
							"type_name": "延长保修"
						},
						{
							"can_fold": true,
							"service_info": [
								{
									"act_price": "208",
									"goods_id": 2220600009,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400049",
									"service_name": "云空间年卡 200G",
									"service_price": "208",
									"service_short_name": "云空间年卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "58",
									"goods_id": 2220600009,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400048",
									"service_name": "云空间年卡50G",
									"service_price": "58",
									"service_short_name": "云空间年卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "21",
									"goods_id": 2220600009,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2182300057",
									"service_name": "小米云服务空间月卡 200G",
									"service_price": "21",
									"service_short_name": "云空间月卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "6",
									"goods_id": 2220600009,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400054",
									"service_name": "小米云服务空间月卡 50G",
									"service_price": "6",
									"service_short_name": "云空间月卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								}
							],
							"service_type_name": "service_micloud",
							"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
							"type_name": "云空间服务"
						}
					]
				},
				"multi_service_refound_policy": {
					"first": {
						"desc": "支持7天无理由退货",
						"is_support": true
					}
				},
				"multi_stock_channel": {
					"first": "3"
				},
				"name": "Redmi K50 电竞版 8GB+128GB 银翼",
				"next_action_button": null,
				"next_onsale_action_button": null,
				"onsale_action_button": null,
				"page_id": 23164,
				"price": "2399",
				"product_desc": "",
				"product_desc_ext": "",
				"product_gallery_icon": {},
				"product_id": 15615,
				"prop_list": [
					{
						"prop_cfg_id": 8,
						"prop_value_id": 883
					},
					{
						"prop_cfg_id": 2,
						"prop_value_id": 10
					}
				],
				"reduce_price": "直降900元",
				"seckill_action_button": null,
				"sell_point_desc": null,
				"service_refound_policy_list": {
					"list": [
						{
							"goods_id": 2220600009,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/a32fabc4c2cecf39e88b9b018fb8d6ec.png",
							"is_support": true,
							"item_content": "7日内，无人为损坏，包装配件齐全，可以无理由退货。",
							"item_id": 1,
							"item_name": "7天无理由退货",
							"sku": 37744
						},
						{
							"goods_id": 2220600009,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/1769a228b31fb6b994a77c4c4ca16448.png",
							"is_support": true,
							"item_content": "15日内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费更换。",
							"item_id": 2,
							"item_name": "15天质量问题换货",
							"sku": 37744
						},
						{
							"goods_id": 2220600009,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/de2196c347411bab09e7e46e0df5c547.png",
							"is_support": true,
							"item_content": "1年内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费维修。",
							"item_id": 3,
							"item_name": "365天保修",
							"sku": 37744
						}
					],
					"list_new": [
						{
							"is_check": true,
							"is_page_show": true,
							"title": "小米自营"
						},
						{
							"desc": "由小米发货",
							"is_check": true,
							"is_page_show": true,
							"title": "小米发货"
						},
						{
							"is_check": true,
							"is_page_show": true,
							"title": "7天无理由退货"
						},
						{
							"desc": "由小米发货商品(不含有品),单笔满69元免运费;\n由小米有品发货的商品,免运费;\n由第三方商家发货的商品，运费以实际结算金额为准;\n特殊商品需要单独收取运费,具体以实际结算金额为准;优惠券等不能抵扣运费金额;如需无理由退货,用户将承担该商品的退货物流费用;\n使用门店闪送服务，需单独支付10元运费。",
							"is_check": true,
							"is_page_show": false,
							"title": "运费说明"
						},
						{
							"desc": "企业名称：小米通讯技术有限公司\n企业执照注册号：91110108558521630L\n企业地址：北京市海淀区西二旗中路33号院6 号楼9层019号\n企业电话：400-100-5678\n营业期限：2010年08月25日 至 2040年08月24日\n经营范围：开发手机技术、计算机软件及信息技术；技术检测、技术咨询、技术服务、技术转让；计算机技术培训；系统集成；货物进出口、技术进出口、代理进出口；家用电器、通信设备、广播电视设备（不含卫星电视广播、地面接收装置）、机械设备、电子产品、文化用品的批发零售；维修仪器仪表；销售医疗器械I类、II、III类、针纺织品（含家纺家饰）、服装鞋帽、日用杂货、工艺品、文化用品、体育用品、照相器材、卫生用品（含个人护理用品）、钟表眼镜、箱包、家具（不从事实体店铺经营）、小饰品、日用品、乐器、自行车、智能卡；计算机、通讯设备、家用电器、电子产品、机械设备的技术开发、技术服务；销售金银饰品（不含金银质地纪念币）；家用空调的委托生产；委托生产翻译机；销售翻译机、五金交电（不含电动自行车）、厨房用品、陶瓷制品、玻璃制品、玩具、汽车零配件、食用农产品、花卉、苗木、宠物用品、建筑材料、装饰材料、化妆品、珠宝首饰、通讯设备、卫生间用品、农药；生产手机（仅限在海淀区永捷北路2号三层生产及外埠生产）；出版物批发；出版物零售；销售食品。（销售第三类医疗器械以及销售食品以及依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动。）",
							"is_check": true,
							"is_page_show": false,
							"title": "企业信息"
						},
						{
							"is_check": true,
							"is_page_show": false,
							"title": "7天价格保护",
							"url": "https://s1.mi.com/m/faq/giving/index.html?project_id=44"
						}
					],
					"url": ""
				},
				"sku": 37744
			},
			{
				"action_button": {
					"bigtap_source": "bigtap",
					"button_title": "等待到货",
					"is_bigtap": false,
					"mode_change_time": 0,
					"mode_end_time": 0,
					"mode_start_time": 1668051600,
					"occ_timeout": 5000,
					"sale_mode": "webview",
					"sale_mode_line": "first",
					"sale_mode_title": "立即购买",
					"source": "common",
					"use_occ": false,
					"webview": {
						"is_hide_stock": true,
						"url": ""
					}
				},
				"address_type": "common",
				"class_parameters": {
					"list": [
						{
							"bottom_title": "骁龙8 Gen 1",
							"icon": "https://cdn.cnbj0.fds.api.mi-img.com/b2c-mimall-media/f0c04e138bfed2b1ebb589de615236d1.png",
							"is_page_show": true,
							"name": "CPU型号",
							"top_title": "CPU",
							"value": "骁龙8 Gen 1"
						},
						{
							"name": "CPU主频",
							"value": "最高3.0GHz"
						},
						{
							"bottom_title": "6400万像素+800万像素+200万像素",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/7692726e7a1dd34a3b1b4ede8aca020d.png",
							"is_page_show": true,
							"name": "后置摄像头",
							"top_title": "三摄像头",
							"value": "6400万像素+800万像素+200万像素"
						},
						{
							"name": "前置摄像头",
							"value": "2000万像素"
						},
						{
							"name": "屏幕",
							"value": "OLED"
						},
						{
							"bottom_title": "6.67英寸",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/86a3bd46cf4f7f19daa2c3250cf30604.png",
							"is_page_show": true,
							"name": "屏幕尺寸",
							"top_title": "超大屏",
							"value": "6.67英寸"
						},
						{
							"bottom_title": "2400×1080",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/a5ab24dcb527e49f970f13b11e000ab1.png",
							"is_page_show": true,
							"name": "屏幕分辨率",
							"top_title": "屏幕分辨率",
							"value": "2400×1080"
						},
						{
							"bottom_title": "最高12GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/c8ec0829241324e401744da627482560.png",
							"is_page_show": true,
							"name": "运行内存",
							"top_title": "极速畅玩",
							"value": "最高12GB"
						},
						{
							"bottom_title": "最高256GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/8941adac25333e785b9cc78ca11f4f27.png",
							"is_page_show": true,
							"name": "存储容量",
							"top_title": "存储容量",
							"value": "最高256GB"
						},
						{
							"name": "NFC",
							"value": "支持"
						},
						{
							"name": "红外遥控",
							"value": "支持"
						},
						{
							"name": "独立AI键",
							"value": "磁动力弹出式肩键 2.0"
						},
						{
							"name": "指纹识别",
							"value": "侧边指纹"
						},
						{
							"bottom_title": "8.5mm",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/52ad10a73685342e437e44ea3d29cbff.png",
							"is_page_show": true,
							"name": "机身厚度",
							"top_title": "普通厚度",
							"value": "8.5mm"
						},
						{
							"bottom_title": "4700mAh",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/0b4ea0fb21dde2f29df3c20de73539b9.png",
							"is_page_show": true,
							"name": "电池容量",
							"top_title": "超长待机",
							"value": "4700mAh"
						},
						{
							"name": "有线快充",
							"value": "120W"
						},
						{
							"bottom_title": "5G全网通7.0",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/d1b67a407fb2a1ed42c2c0ce15af3318.png",
							"is_page_show": true,
							"name": "网络类型",
							"top_title": "运营商网络",
							"value": "5G全网通7.0"
						},
						{
							"bottom_title": "双卡双待",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/bfd5ba9ae72c365dee42db14dfae4b0f.png",
							"is_page_show": true,
							"name": "网络模式",
							"top_title": "网络模式",
							"value": "双卡双待"
						},
						{
							"name": "数据接口",
							"value": "Type-C"
						}
					],
					"name": "关键参数"
				},
				"commodity_id": 1220600013,
				"customize_template_id": 0,
				"discount_price": "",
				"ext_buy_option": [],
				"gallery_v3": [
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/7ba11fdfecac708e7dcc3b98b537a83a.jpeg",
						"img_url_webp": "",
						"type": "video"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/5061b08df9de9ca1c23ba97b58a340c8.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/cdd534269f9326beaa4a493eab827070.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/22bbbda34d4535698318fc9cf40d0334.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bb7e0537300b002657566cd68920f176.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c090f46e0d07ed6db835fe8f96cf41c1.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0542966b26815adcb450e47b7bca70cc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/eab9a225caeae1c6c9f0673915ad295e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/a9ccfcac5415437e23127e1219966a44.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/1d1d0ab8b94208e69ec929f4d95b3c17.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8348f5f8d83643c9fcfc16dd240c4adc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bd48dcfd217a949798f2748a94bb0b41.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/93d64b29aa8aaff8fc9db0e9a665ad9e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3e3d8b8212a6cc53d5db756531e1ba1d.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0ce722c3e15ed76a9654c20fca0c7faf.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/e85d8a06e1b0ea43d3b8fa397aaf5ebc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/68bb35c26ff3f358362652df9b132d2a.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/577d8e3b1ba50a967e423c8562654b57.jpg",
						"img_url_webp": "",
						"type": "img"
					}
				],
				"goods_id": 2220600008,
				"image_share": "http://cdn.cnbj1.fds.api.mi-img.com/mi-mall/063c4055e2befc30c8042dd9cff559f6.png",
				"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/063c4055e2befc30c8042dd9cff559f6.png",
				"is_customize": false,
				"is_sale": true,
				"is_stock": false,
				"market_price": "3299",
				"multi_buy_limit": {
					"first": 10
				},
				"multi_instalment": {
					"first": {
						"default_instal": "antinstal_m",
						"list": [
							{
								"Bank": "mifinanceinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 73.24,
										"is_differential_rate": false,
										"rate": 2.22,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1124.08,
										"stage_interest": 24.41,
										"tab_detail": "手续费24.41元/期起，费率2.22%起，单利年化利率约为13.32%",
										"tab_title": "1124.08元x3期",
										"total_cost": 3372.24
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 128.66,
										"is_differential_rate": false,
										"rate": 3.9,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 571.27,
										"stage_interest": 21.44,
										"tab_detail": "手续费21.44元/期起，费率3.9%起，单利年化利率约为13.37%",
										"tab_title": "571.27元x6期",
										"total_cost": 3427.66
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 237.53,
										"is_differential_rate": false,
										"rate": 7.2,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 294.71,
										"stage_interest": 19.79,
										"tab_detail": "手续费19.79元/期起，费率7.2%起，单利年化利率约为13.29%",
										"tab_title": "294.71元x12期",
										"total_cost": 3536.53
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 593.82,
										"is_differential_rate": false,
										"rate": 18,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 162.2,
										"stage_interest": 24.74,
										"tab_detail": "手续费24.74元/期起，费率18%起，单利年化利率约为17.28%",
										"tab_title": "162.2元x24期",
										"total_cost": 3892.82
									}
								],
								"Installments": null,
								"Title": "小米分期"
							},
							{
								"Bank": "antinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 75.88,
										"is_differential_rate": true,
										"rate": 2.3,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1124.95,
										"stage_interest": 25.29,
										"tab_detail": "手续费25.29元/期起，费率2.3%起，单利年化利率约为13.70%",
										"tab_title": "1124.95元x3期",
										"total_cost": 3374.88
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 148.46,
										"is_differential_rate": true,
										"rate": 4.5,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 574.57,
										"stage_interest": 24.74,
										"tab_detail": "手续费24.74元/期起，费率4.5%起，单利年化利率约为15.30%",
										"tab_title": "574.57元x6期",
										"total_cost": 3447.46
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 247.42,
										"is_differential_rate": true,
										"rate": 7.5,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 295.52,
										"stage_interest": 20.61,
										"tab_detail": "手续费20.61元/期起，费率7.5%起，单利年化利率约为13.60%",
										"tab_title": "295.52元x12期",
										"total_cost": 3546.42
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 412.38,
										"is_differential_rate": true,
										"rate": 12.5,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 154.63,
										"stage_interest": 17.18,
										"tab_detail": "手续费17.18元/期起，费率12.5%起，单利年化利率约为13.80%",
										"tab_title": "154.63元x24期",
										"total_cost": 3711.38
									}
								],
								"Installments": null,
								"Title": "花呗分期"
							}
						]
					}
				},
				"multi_instalment_info": {
					"first": {
						"detail_info": "低至143.45元 x 24期"
					}
				},
				"multi_insurance": {
					"first": {
						"list": [
							{
								"goods_id": 2220700001,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644809331.80464581.jpg",
								"insurance_type": "micare",
								"market_price": 599,
								"name": "Redmi K50 电竞版 MiCare保障服务",
								"onsale_price": 0,
								"price": 599,
								"short_name": "Redmi K50 电竞版 MiCare保障服务",
								"sku": 41034,
								"type": "2"
							},
							{
								"goods_id": 2220600024,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548009.85713550.jpg",
								"insurance_type": "accident",
								"market_price": 249,
								"name": "Redmi K50 电竞版 碎屏保障服务",
								"onsale_price": 124,
								"price": 124,
								"short_name": "Redmi K50 电竞版 碎屏保障服务",
								"sku": 40963,
								"type": "2"
							},
							{
								"goods_id": 2220600026,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548371.81671372.jpg",
								"insurance_type": "prolong",
								"market_price": 119,
								"name": "Redmi K50 电竞版 延长保修服务",
								"onsale_price": 59,
								"price": 59,
								"short_name": "Redmi K50 电竞版 延长保修服务",
								"sku": 40964,
								"type": "2"
							},
							{
								"goods_id": 2220600025,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548202.10119327.jpg",
								"insurance_type": "accident",
								"market_price": 399,
								"name": "Redmi K50 电竞版 意外保障服务",
								"onsale_price": 0,
								"price": 399,
								"short_name": "Redmi K50 电竞版 意外保障服务",
								"sku": 40962,
								"type": "2"
							}
						]
					}
				},
				"multi_market_price": {
					"first": "3299",
					"second": "3299"
				},
				"multi_price": {
					"first": "2399",
					"second": "2399"
				},
				"multi_service_bargins": {
					"first": [
						{
							"can_fold": true,
							"mutex_type": [
								"service_insurance",
								"service_prolong"
							],
							"service_info": [
								{
									"act_diff": "碎屏延保换新",
									"goods_id": 2220600008,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D41034%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D41034%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "享碎屏、延保、换电池、保值换新4大权益",
									"service_goods_id": "2220700001",
									"service_name": "Redmi K50 电竞版 MiCare保障服务",
									"service_price": "599",
									"service_short_name": "MiCare保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
									"source": "common"
								}
							],
							"service_type_name": "service_micare",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
							"type_name": "尊享服务"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "意外损免费修",
									"goods_id": 2220600008,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40962%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40962%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次意外损坏 官方原厂 免费维修",
									"service_goods_id": "2220600025",
									"service_name": "Redmi K50 电竞版 意外保障服务",
									"service_price": "399",
									"service_short_name": "意外保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
									"source": "common"
								},
								{
									"act_diff": "已省125元",
									"act_price": "124",
									"goods_id": 2220600008,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40963%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40963%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次碎屏 官方原厂 免费维修",
									"service_goods_id": "2220600024",
									"service_name": "Redmi K50 电竞版 碎屏保障服务",
									"service_price": "124",
									"service_short_name": "碎屏保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40342",
									"source": "common"
								}
							],
							"service_type_name": "service_insurance",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
							"type_name": "意外保护"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "已省60元",
									"act_price": "59",
									"goods_id": 2220600008,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40964%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40964%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "性能故障 官方原厂 多次免费维修",
									"service_goods_id": "2220600026",
									"service_name": "Redmi K50 电竞版 延长保修服务",
									"service_price": "59",
									"service_short_name": "延长保修服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
									"source": "common"
								}
							],
							"service_type_name": "service_prolong",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
							"type_name": "延长保修"
						},
						{
							"can_fold": true,
							"service_info": [
								{
									"act_price": "208",
									"goods_id": 2220600008,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400049",
									"service_name": "云空间年卡 200G",
									"service_price": "208",
									"service_short_name": "云空间年卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "58",
									"goods_id": 2220600008,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400048",
									"service_name": "云空间年卡50G",
									"service_price": "58",
									"service_short_name": "云空间年卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "21",
									"goods_id": 2220600008,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2182300057",
									"service_name": "小米云服务空间月卡 200G",
									"service_price": "21",
									"service_short_name": "云空间月卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "6",
									"goods_id": 2220600008,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400054",
									"service_name": "小米云服务空间月卡 50G",
									"service_price": "6",
									"service_short_name": "云空间月卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								}
							],
							"service_type_name": "service_micloud",
							"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
							"type_name": "云空间服务"
						}
					]
				},
				"multi_service_refound_policy": {
					"first": {
						"desc": "支持7天无理由退货",
						"is_support": true
					}
				},
				"multi_stock_channel": {
					"first": "3"
				},
				"name": "Redmi K50 电竞版 8GB+128GB 暗影",
				"next_action_button": null,
				"next_onsale_action_button": null,
				"onsale_action_button": null,
				"page_id": 23164,
				"price": "2399",
				"product_desc": "",
				"product_desc_ext": "",
				"product_gallery_icon": {},
				"product_id": 15615,
				"prop_list": [
					{
						"prop_cfg_id": 8,
						"prop_value_id": 883
					},
					{
						"prop_cfg_id": 2,
						"prop_value_id": 3
					}
				],
				"reduce_price": "直降900元",
				"seckill_action_button": null,
				"sell_point_desc": null,
				"service_refound_policy_list": {
					"list": [
						{
							"goods_id": 2220600008,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/a32fabc4c2cecf39e88b9b018fb8d6ec.png",
							"is_support": true,
							"item_content": "7日内，无人为损坏，包装配件齐全，可以无理由退货。",
							"item_id": 1,
							"item_name": "7天无理由退货",
							"sku": 37742
						},
						{
							"goods_id": 2220600008,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/1769a228b31fb6b994a77c4c4ca16448.png",
							"is_support": true,
							"item_content": "15日内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费更换。",
							"item_id": 2,
							"item_name": "15天质量问题换货",
							"sku": 37742
						},
						{
							"goods_id": 2220600008,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/de2196c347411bab09e7e46e0df5c547.png",
							"is_support": true,
							"item_content": "1年内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费维修。",
							"item_id": 3,
							"item_name": "365天保修",
							"sku": 37742
						}
					],
					"list_new": [
						{
							"is_check": true,
							"is_page_show": true,
							"title": "小米自营"
						},
						{
							"desc": "由小米发货",
							"is_check": true,
							"is_page_show": true,
							"title": "小米发货"
						},
						{
							"is_check": true,
							"is_page_show": true,
							"title": "7天无理由退货"
						},
						{
							"desc": "由小米发货商品(不含有品),单笔满69元免运费;\n由小米有品发货的商品,免运费;\n由第三方商家发货的商品，运费以实际结算金额为准;\n特殊商品需要单独收取运费,具体以实际结算金额为准;优惠券等不能抵扣运费金额;如需无理由退货,用户将承担该商品的退货物流费用;\n使用门店闪送服务，需单独支付10元运费。",
							"is_check": true,
							"is_page_show": false,
							"title": "运费说明"
						},
						{
							"desc": "企业名称：小米通讯技术有限公司\n企业执照注册号：91110108558521630L\n企业地址：北京市海淀区西二旗中路33号院6 号楼9层019号\n企业电话：400-100-5678\n营业期限：2010年08月25日 至 2040年08月24日\n经营范围：开发手机技术、计算机软件及信息技术；技术检测、技术咨询、技术服务、技术转让；计算机技术培训；系统集成；货物进出口、技术进出口、代理进出口；家用电器、通信设备、广播电视设备（不含卫星电视广播、地面接收装置）、机械设备、电子产品、文化用品的批发零售；维修仪器仪表；销售医疗器械I类、II、III类、针纺织品（含家纺家饰）、服装鞋帽、日用杂货、工艺品、文化用品、体育用品、照相器材、卫生用品（含个人护理用品）、钟表眼镜、箱包、家具（不从事实体店铺经营）、小饰品、日用品、乐器、自行车、智能卡；计算机、通讯设备、家用电器、电子产品、机械设备的技术开发、技术服务；销售金银饰品（不含金银质地纪念币）；家用空调的委托生产；委托生产翻译机；销售翻译机、五金交电（不含电动自行车）、厨房用品、陶瓷制品、玻璃制品、玩具、汽车零配件、食用农产品、花卉、苗木、宠物用品、建筑材料、装饰材料、化妆品、珠宝首饰、通讯设备、卫生间用品、农药；生产手机（仅限在海淀区永捷北路2号三层生产及外埠生产）；出版物批发；出版物零售；销售食品。（销售第三类医疗器械以及销售食品以及依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动。）",
							"is_check": true,
							"is_page_show": false,
							"title": "企业信息"
						},
						{
							"is_check": true,
							"is_page_show": false,
							"title": "7天价格保护",
							"url": "https://s1.mi.com/m/faq/giving/index.html?project_id=44"
						}
					],
					"url": ""
				},
				"sku": 37742
			},
			{
				"action_button": {
					"bigtap_source": "bigtap",
					"button_title": "等待到货",
					"is_bigtap": false,
					"mode_change_time": 0,
					"mode_end_time": 0,
					"mode_start_time": 1668051600,
					"occ_timeout": 5000,
					"sale_mode": "webview",
					"sale_mode_line": "first",
					"sale_mode_title": "立即购买",
					"source": "common",
					"use_occ": false,
					"webview": {
						"is_hide_stock": true,
						"url": ""
					}
				},
				"address_type": "common",
				"class_parameters": {
					"list": [
						{
							"bottom_title": "骁龙8 Gen 1",
							"icon": "https://cdn.cnbj0.fds.api.mi-img.com/b2c-mimall-media/f0c04e138bfed2b1ebb589de615236d1.png",
							"is_page_show": true,
							"name": "CPU型号",
							"top_title": "CPU",
							"value": "骁龙8 Gen 1"
						},
						{
							"name": "CPU主频",
							"value": "最高3.0GHz"
						},
						{
							"bottom_title": "6400万像素+800万像素+200万像素",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/7692726e7a1dd34a3b1b4ede8aca020d.png",
							"is_page_show": true,
							"name": "后置摄像头",
							"top_title": "三摄像头",
							"value": "6400万像素+800万像素+200万像素"
						},
						{
							"name": "前置摄像头",
							"value": "2000万像素"
						},
						{
							"name": "屏幕",
							"value": "OLED"
						},
						{
							"bottom_title": "6.67英寸",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/86a3bd46cf4f7f19daa2c3250cf30604.png",
							"is_page_show": true,
							"name": "屏幕尺寸",
							"top_title": "超大屏",
							"value": "6.67英寸"
						},
						{
							"bottom_title": "2400×1080",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/a5ab24dcb527e49f970f13b11e000ab1.png",
							"is_page_show": true,
							"name": "屏幕分辨率",
							"top_title": "屏幕分辨率",
							"value": "2400×1080"
						},
						{
							"bottom_title": "最高12GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/c8ec0829241324e401744da627482560.png",
							"is_page_show": true,
							"name": "运行内存",
							"top_title": "极速畅玩",
							"value": "最高12GB"
						},
						{
							"bottom_title": "最高256GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/8941adac25333e785b9cc78ca11f4f27.png",
							"is_page_show": true,
							"name": "存储容量",
							"top_title": "存储容量",
							"value": "最高256GB"
						},
						{
							"name": "NFC",
							"value": "支持"
						},
						{
							"name": "红外遥控",
							"value": "支持"
						},
						{
							"name": "独立AI键",
							"value": "磁动力弹出式肩键 2.0"
						},
						{
							"name": "指纹识别",
							"value": "侧边指纹"
						},
						{
							"bottom_title": "8.5mm",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/52ad10a73685342e437e44ea3d29cbff.png",
							"is_page_show": true,
							"name": "机身厚度",
							"top_title": "普通厚度",
							"value": "8.5mm"
						},
						{
							"bottom_title": "4700mAh",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/0b4ea0fb21dde2f29df3c20de73539b9.png",
							"is_page_show": true,
							"name": "电池容量",
							"top_title": "超长待机",
							"value": "4700mAh"
						},
						{
							"name": "有线快充",
							"value": "120W"
						},
						{
							"bottom_title": "5G全网通7.0",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/d1b67a407fb2a1ed42c2c0ce15af3318.png",
							"is_page_show": true,
							"name": "网络类型",
							"top_title": "运营商网络",
							"value": "5G全网通7.0"
						},
						{
							"bottom_title": "双卡双待",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/bfd5ba9ae72c365dee42db14dfae4b0f.png",
							"is_page_show": true,
							"name": "网络模式",
							"top_title": "网络模式",
							"value": "双卡双待"
						},
						{
							"name": "数据接口",
							"value": "Type-C"
						}
					],
					"name": "关键参数"
				},
				"commodity_id": 1220600013,
				"customize_template_id": 0,
				"discount_price": "",
				"ext_buy_option": [],
				"gallery_v3": [
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/7ba11fdfecac708e7dcc3b98b537a83a.jpeg",
						"img_url_webp": "",
						"type": "video"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/5061b08df9de9ca1c23ba97b58a340c8.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/cdd534269f9326beaa4a493eab827070.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/22bbbda34d4535698318fc9cf40d0334.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bb7e0537300b002657566cd68920f176.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c090f46e0d07ed6db835fe8f96cf41c1.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0542966b26815adcb450e47b7bca70cc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/eab9a225caeae1c6c9f0673915ad295e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/a9ccfcac5415437e23127e1219966a44.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/1d1d0ab8b94208e69ec929f4d95b3c17.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8348f5f8d83643c9fcfc16dd240c4adc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bd48dcfd217a949798f2748a94bb0b41.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/93d64b29aa8aaff8fc9db0e9a665ad9e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3e3d8b8212a6cc53d5db756531e1ba1d.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0ce722c3e15ed76a9654c20fca0c7faf.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/e85d8a06e1b0ea43d3b8fa397aaf5ebc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/68bb35c26ff3f358362652df9b132d2a.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/577d8e3b1ba50a967e423c8562654b57.jpg",
						"img_url_webp": "",
						"type": "img"
					}
				],
				"goods_id": 2220600007,
				"image_share": "http://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f5d5806d6807671fd2a28d9a944b5358.png",
				"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/f5d5806d6807671fd2a28d9a944b5358.png",
				"is_customize": false,
				"is_sale": true,
				"is_stock": false,
				"market_price": "3299",
				"multi_buy_limit": {
					"first": 10
				},
				"multi_instalment": {
					"first": {
						"default_instal": "antinstal_m",
						"list": [
							{
								"Bank": "mifinanceinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 73.24,
										"is_differential_rate": false,
										"rate": 2.22,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1124.08,
										"stage_interest": 24.41,
										"tab_detail": "手续费24.41元/期起，费率2.22%起，单利年化利率约为13.32%",
										"tab_title": "1124.08元x3期",
										"total_cost": 3372.24
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 128.66,
										"is_differential_rate": false,
										"rate": 3.9,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 571.27,
										"stage_interest": 21.44,
										"tab_detail": "手续费21.44元/期起，费率3.9%起，单利年化利率约为13.37%",
										"tab_title": "571.27元x6期",
										"total_cost": 3427.66
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 237.53,
										"is_differential_rate": false,
										"rate": 7.2,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 294.71,
										"stage_interest": 19.79,
										"tab_detail": "手续费19.79元/期起，费率7.2%起，单利年化利率约为13.29%",
										"tab_title": "294.71元x12期",
										"total_cost": 3536.53
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 593.82,
										"is_differential_rate": false,
										"rate": 18,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 162.2,
										"stage_interest": 24.74,
										"tab_detail": "手续费24.74元/期起，费率18%起，单利年化利率约为17.28%",
										"tab_title": "162.2元x24期",
										"total_cost": 3892.82
									}
								],
								"Installments": null,
								"Title": "小米分期"
							},
							{
								"Bank": "antinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 75.88,
										"is_differential_rate": true,
										"rate": 2.3,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1124.95,
										"stage_interest": 25.29,
										"tab_detail": "手续费25.29元/期起，费率2.3%起，单利年化利率约为13.70%",
										"tab_title": "1124.95元x3期",
										"total_cost": 3374.88
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 148.46,
										"is_differential_rate": true,
										"rate": 4.5,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 574.57,
										"stage_interest": 24.74,
										"tab_detail": "手续费24.74元/期起，费率4.5%起，单利年化利率约为15.30%",
										"tab_title": "574.57元x6期",
										"total_cost": 3447.46
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 247.42,
										"is_differential_rate": true,
										"rate": 7.5,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 295.52,
										"stage_interest": 20.61,
										"tab_detail": "手续费20.61元/期起，费率7.5%起，单利年化利率约为13.60%",
										"tab_title": "295.52元x12期",
										"total_cost": 3546.42
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 412.38,
										"is_differential_rate": true,
										"rate": 12.5,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 154.63,
										"stage_interest": 17.18,
										"tab_detail": "手续费17.18元/期起，费率12.5%起，单利年化利率约为13.80%",
										"tab_title": "154.63元x24期",
										"total_cost": 3711.38
									}
								],
								"Installments": null,
								"Title": "花呗分期"
							}
						]
					}
				},
				"multi_instalment_info": {
					"first": {
						"detail_info": "低至143.45元 x 24期"
					}
				},
				"multi_insurance": {
					"first": {
						"list": [
							{
								"goods_id": 2220700001,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644809331.80464581.jpg",
								"insurance_type": "micare",
								"market_price": 599,
								"name": "Redmi K50 电竞版 MiCare保障服务",
								"onsale_price": 0,
								"price": 599,
								"short_name": "Redmi K50 电竞版 MiCare保障服务",
								"sku": 41034,
								"type": "2"
							},
							{
								"goods_id": 2220600024,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548009.85713550.jpg",
								"insurance_type": "accident",
								"market_price": 249,
								"name": "Redmi K50 电竞版 碎屏保障服务",
								"onsale_price": 124,
								"price": 124,
								"short_name": "Redmi K50 电竞版 碎屏保障服务",
								"sku": 40963,
								"type": "2"
							},
							{
								"goods_id": 2220600026,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548371.81671372.jpg",
								"insurance_type": "prolong",
								"market_price": 119,
								"name": "Redmi K50 电竞版 延长保修服务",
								"onsale_price": 59,
								"price": 59,
								"short_name": "Redmi K50 电竞版 延长保修服务",
								"sku": 40964,
								"type": "2"
							},
							{
								"goods_id": 2220600025,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548202.10119327.jpg",
								"insurance_type": "accident",
								"market_price": 399,
								"name": "Redmi K50 电竞版 意外保障服务",
								"onsale_price": 0,
								"price": 399,
								"short_name": "Redmi K50 电竞版 意外保障服务",
								"sku": 40962,
								"type": "2"
							}
						]
					}
				},
				"multi_market_price": {
					"first": "3299",
					"second": "3299"
				},
				"multi_price": {
					"first": "2399",
					"second": "2399"
				},
				"multi_service_bargins": {
					"first": [
						{
							"can_fold": true,
							"mutex_type": [
								"service_insurance",
								"service_prolong"
							],
							"service_info": [
								{
									"act_diff": "碎屏延保换新",
									"goods_id": 2220600007,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D41034%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D41034%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "享碎屏、延保、换电池、保值换新4大权益",
									"service_goods_id": "2220700001",
									"service_name": "Redmi K50 电竞版 MiCare保障服务",
									"service_price": "599",
									"service_short_name": "MiCare保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
									"source": "common"
								}
							],
							"service_type_name": "service_micare",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
							"type_name": "尊享服务"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "意外损免费修",
									"goods_id": 2220600007,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40962%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40962%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次意外损坏 官方原厂 免费维修",
									"service_goods_id": "2220600025",
									"service_name": "Redmi K50 电竞版 意外保障服务",
									"service_price": "399",
									"service_short_name": "意外保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
									"source": "common"
								},
								{
									"act_diff": "已省125元",
									"act_price": "124",
									"goods_id": 2220600007,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40963%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40963%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次碎屏 官方原厂 免费维修",
									"service_goods_id": "2220600024",
									"service_name": "Redmi K50 电竞版 碎屏保障服务",
									"service_price": "124",
									"service_short_name": "碎屏保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40342",
									"source": "common"
								}
							],
							"service_type_name": "service_insurance",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
							"type_name": "意外保护"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "已省60元",
									"act_price": "59",
									"goods_id": 2220600007,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40964%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40964%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "性能故障 官方原厂 多次免费维修",
									"service_goods_id": "2220600026",
									"service_name": "Redmi K50 电竞版 延长保修服务",
									"service_price": "59",
									"service_short_name": "延长保修服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
									"source": "common"
								}
							],
							"service_type_name": "service_prolong",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
							"type_name": "延长保修"
						},
						{
							"can_fold": true,
							"service_info": [
								{
									"act_price": "208",
									"goods_id": 2220600007,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400049",
									"service_name": "云空间年卡 200G",
									"service_price": "208",
									"service_short_name": "云空间年卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "58",
									"goods_id": 2220600007,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400048",
									"service_name": "云空间年卡50G",
									"service_price": "58",
									"service_short_name": "云空间年卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "21",
									"goods_id": 2220600007,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2182300057",
									"service_name": "小米云服务空间月卡 200G",
									"service_price": "21",
									"service_short_name": "云空间月卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "6",
									"goods_id": 2220600007,
									"goods_name": "Redmi K50 电竞版 8GB+128GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400054",
									"service_name": "小米云服务空间月卡 50G",
									"service_price": "6",
									"service_short_name": "云空间月卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								}
							],
							"service_type_name": "service_micloud",
							"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
							"type_name": "云空间服务"
						}
					]
				},
				"multi_service_refound_policy": {
					"first": {
						"desc": "支持7天无理由退货",
						"is_support": true
					}
				},
				"multi_stock_channel": {
					"first": "3"
				},
				"name": "Redmi K50 电竞版 8GB+128GB 冰斩",
				"next_action_button": null,
				"next_onsale_action_button": null,
				"onsale_action_button": null,
				"page_id": 23164,
				"price": "2399",
				"product_desc": "",
				"product_desc_ext": "",
				"product_gallery_icon": {},
				"product_id": 15615,
				"prop_list": [
					{
						"prop_cfg_id": 8,
						"prop_value_id": 883
					},
					{
						"prop_cfg_id": 2,
						"prop_value_id": 4
					}
				],
				"reduce_price": "直降900元",
				"seckill_action_button": null,
				"sell_point_desc": null,
				"service_refound_policy_list": {
					"list": [
						{
							"goods_id": 2220600007,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/a32fabc4c2cecf39e88b9b018fb8d6ec.png",
							"is_support": true,
							"item_content": "7日内，无人为损坏，包装配件齐全，可以无理由退货。",
							"item_id": 1,
							"item_name": "7天无理由退货",
							"sku": 37740
						},
						{
							"goods_id": 2220600007,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/1769a228b31fb6b994a77c4c4ca16448.png",
							"is_support": true,
							"item_content": "15日内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费更换。",
							"item_id": 2,
							"item_name": "15天质量问题换货",
							"sku": 37740
						},
						{
							"goods_id": 2220600007,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/de2196c347411bab09e7e46e0df5c547.png",
							"is_support": true,
							"item_content": "1年内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费维修。",
							"item_id": 3,
							"item_name": "365天保修",
							"sku": 37740
						}
					],
					"list_new": [
						{
							"is_check": true,
							"is_page_show": true,
							"title": "小米自营"
						},
						{
							"desc": "由小米发货",
							"is_check": true,
							"is_page_show": true,
							"title": "小米发货"
						},
						{
							"is_check": true,
							"is_page_show": true,
							"title": "7天无理由退货"
						},
						{
							"desc": "由小米发货商品(不含有品),单笔满69元免运费;\n由小米有品发货的商品,免运费;\n由第三方商家发货的商品，运费以实际结算金额为准;\n特殊商品需要单独收取运费,具体以实际结算金额为准;优惠券等不能抵扣运费金额;如需无理由退货,用户将承担该商品的退货物流费用;\n使用门店闪送服务，需单独支付10元运费。",
							"is_check": true,
							"is_page_show": false,
							"title": "运费说明"
						},
						{
							"desc": "企业名称：小米通讯技术有限公司\n企业执照注册号：91110108558521630L\n企业地址：北京市海淀区西二旗中路33号院6 号楼9层019号\n企业电话：400-100-5678\n营业期限：2010年08月25日 至 2040年08月24日\n经营范围：开发手机技术、计算机软件及信息技术；技术检测、技术咨询、技术服务、技术转让；计算机技术培训；系统集成；货物进出口、技术进出口、代理进出口；家用电器、通信设备、广播电视设备（不含卫星电视广播、地面接收装置）、机械设备、电子产品、文化用品的批发零售；维修仪器仪表；销售医疗器械I类、II、III类、针纺织品（含家纺家饰）、服装鞋帽、日用杂货、工艺品、文化用品、体育用品、照相器材、卫生用品（含个人护理用品）、钟表眼镜、箱包、家具（不从事实体店铺经营）、小饰品、日用品、乐器、自行车、智能卡；计算机、通讯设备、家用电器、电子产品、机械设备的技术开发、技术服务；销售金银饰品（不含金银质地纪念币）；家用空调的委托生产；委托生产翻译机；销售翻译机、五金交电（不含电动自行车）、厨房用品、陶瓷制品、玻璃制品、玩具、汽车零配件、食用农产品、花卉、苗木、宠物用品、建筑材料、装饰材料、化妆品、珠宝首饰、通讯设备、卫生间用品、农药；生产手机（仅限在海淀区永捷北路2号三层生产及外埠生产）；出版物批发；出版物零售；销售食品。（销售第三类医疗器械以及销售食品以及依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动。）",
							"is_check": true,
							"is_page_show": false,
							"title": "企业信息"
						},
						{
							"is_check": true,
							"is_page_show": false,
							"title": "7天价格保护",
							"url": "https://s1.mi.com/m/faq/giving/index.html?project_id=44"
						}
					],
					"url": ""
				},
				"sku": 37740
			},
			{
				"action_button": {
					"bigtap_source": "bigtap",
					"button_title": "等待到货",
					"is_bigtap": false,
					"mode_change_time": 0,
					"mode_end_time": 0,
					"mode_start_time": 1668051600,
					"occ_timeout": 5000,
					"sale_mode": "webview",
					"sale_mode_line": "first",
					"sale_mode_title": "立即购买",
					"source": "common",
					"use_occ": false,
					"webview": {
						"is_hide_stock": true,
						"url": ""
					}
				},
				"address_type": "common",
				"class_parameters": {
					"list": [
						{
							"bottom_title": "骁龙8 Gen 1",
							"icon": "https://cdn.cnbj0.fds.api.mi-img.com/b2c-mimall-media/f0c04e138bfed2b1ebb589de615236d1.png",
							"is_page_show": true,
							"name": "CPU型号",
							"top_title": "CPU",
							"value": "骁龙8 Gen 1"
						},
						{
							"name": "CPU主频",
							"value": "最高3.0GHz"
						},
						{
							"bottom_title": "6400万像素+800万像素+200万像素",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/7692726e7a1dd34a3b1b4ede8aca020d.png",
							"is_page_show": true,
							"name": "后置摄像头",
							"top_title": "三摄像头",
							"value": "6400万像素+800万像素+200万像素"
						},
						{
							"name": "前置摄像头",
							"value": "2000万像素"
						},
						{
							"name": "屏幕",
							"value": "OLED"
						},
						{
							"bottom_title": "6.67英寸",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/86a3bd46cf4f7f19daa2c3250cf30604.png",
							"is_page_show": true,
							"name": "屏幕尺寸",
							"top_title": "超大屏",
							"value": "6.67英寸"
						},
						{
							"bottom_title": "2400×1080",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/a5ab24dcb527e49f970f13b11e000ab1.png",
							"is_page_show": true,
							"name": "屏幕分辨率",
							"top_title": "屏幕分辨率",
							"value": "2400×1080"
						},
						{
							"bottom_title": "最高12GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/c8ec0829241324e401744da627482560.png",
							"is_page_show": true,
							"name": "运行内存",
							"top_title": "极速畅玩",
							"value": "最高12GB"
						},
						{
							"bottom_title": "最高256GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/8941adac25333e785b9cc78ca11f4f27.png",
							"is_page_show": true,
							"name": "存储容量",
							"top_title": "存储容量",
							"value": "最高256GB"
						},
						{
							"name": "NFC",
							"value": "支持"
						},
						{
							"name": "红外遥控",
							"value": "支持"
						},
						{
							"name": "独立AI键",
							"value": "磁动力弹出式肩键 2.0"
						},
						{
							"name": "指纹识别",
							"value": "侧边指纹"
						},
						{
							"bottom_title": "8.5mm",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/52ad10a73685342e437e44ea3d29cbff.png",
							"is_page_show": true,
							"name": "机身厚度",
							"top_title": "普通厚度",
							"value": "8.5mm"
						},
						{
							"bottom_title": "4700mAh",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/0b4ea0fb21dde2f29df3c20de73539b9.png",
							"is_page_show": true,
							"name": "电池容量",
							"top_title": "超长待机",
							"value": "4700mAh"
						},
						{
							"name": "有线快充",
							"value": "120W"
						},
						{
							"bottom_title": "5G全网通7.0",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/d1b67a407fb2a1ed42c2c0ce15af3318.png",
							"is_page_show": true,
							"name": "网络类型",
							"top_title": "运营商网络",
							"value": "5G全网通7.0"
						},
						{
							"bottom_title": "双卡双待",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/bfd5ba9ae72c365dee42db14dfae4b0f.png",
							"is_page_show": true,
							"name": "网络模式",
							"top_title": "网络模式",
							"value": "双卡双待"
						},
						{
							"name": "数据接口",
							"value": "Type-C"
						}
					],
					"name": "关键参数"
				},
				"commodity_id": 1220600014,
				"customize_template_id": 0,
				"discount_price": "",
				"ext_buy_option": [],
				"gallery_v3": [
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/7ba11fdfecac708e7dcc3b98b537a83a.jpeg",
						"img_url_webp": "",
						"type": "video"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/5061b08df9de9ca1c23ba97b58a340c8.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/cdd534269f9326beaa4a493eab827070.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/22bbbda34d4535698318fc9cf40d0334.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bb7e0537300b002657566cd68920f176.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c090f46e0d07ed6db835fe8f96cf41c1.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0542966b26815adcb450e47b7bca70cc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/eab9a225caeae1c6c9f0673915ad295e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/a9ccfcac5415437e23127e1219966a44.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/1d1d0ab8b94208e69ec929f4d95b3c17.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8348f5f8d83643c9fcfc16dd240c4adc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bd48dcfd217a949798f2748a94bb0b41.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/93d64b29aa8aaff8fc9db0e9a665ad9e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3e3d8b8212a6cc53d5db756531e1ba1d.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0ce722c3e15ed76a9654c20fca0c7faf.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/e85d8a06e1b0ea43d3b8fa397aaf5ebc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/68bb35c26ff3f358362652df9b132d2a.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/577d8e3b1ba50a967e423c8562654b57.jpg",
						"img_url_webp": "",
						"type": "img"
					}
				],
				"goods_id": 2220600012,
				"image_share": "http://cdn.cnbj1.fds.api.mi-img.com/mi-mall/8ec65803d049f8d68b6a092fb051590a.png",
				"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8ec65803d049f8d68b6a092fb051590a.png",
				"is_customize": false,
				"is_sale": true,
				"is_stock": false,
				"market_price": "3599",
				"multi_buy_limit": {
					"first": 10
				},
				"multi_instalment": {
					"first": {
						"default_instal": "antinstal_m",
						"list": [
							{
								"Bank": "mifinanceinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 79.9,
										"is_differential_rate": false,
										"rate": 2.22,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1226.3,
										"stage_interest": 26.63,
										"tab_detail": "手续费26.63元/期起，费率2.22%起，单利年化利率约为13.32%",
										"tab_title": "1226.3元x3期",
										"total_cost": 3678.9
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 140.36,
										"is_differential_rate": false,
										"rate": 3.9,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 623.22,
										"stage_interest": 23.39,
										"tab_detail": "手续费23.39元/期起，费率3.9%起，单利年化利率约为13.37%",
										"tab_title": "623.22元x6期",
										"total_cost": 3739.36
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 259.13,
										"is_differential_rate": false,
										"rate": 7.2,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 321.51,
										"stage_interest": 21.59,
										"tab_detail": "手续费21.59元/期起，费率7.2%起，单利年化利率约为13.29%",
										"tab_title": "321.51元x12期",
										"total_cost": 3858.13
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 647.82,
										"is_differential_rate": false,
										"rate": 18,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 176.95,
										"stage_interest": 26.99,
										"tab_detail": "手续费26.99元/期起，费率18%起，单利年化利率约为17.28%",
										"tab_title": "176.95元x24期",
										"total_cost": 4246.82
									}
								],
								"Installments": null,
								"Title": "小米分期"
							},
							{
								"Bank": "antinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 82.78,
										"is_differential_rate": true,
										"rate": 2.3,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1227.25,
										"stage_interest": 27.59,
										"tab_detail": "手续费27.59元/期起，费率2.3%起，单利年化利率约为13.70%",
										"tab_title": "1227.25元x3期",
										"total_cost": 3681.78
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 161.96,
										"is_differential_rate": true,
										"rate": 4.5,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 626.82,
										"stage_interest": 26.99,
										"tab_detail": "手续费26.99元/期起，费率4.5%起，单利年化利率约为15.30%",
										"tab_title": "626.82元x6期",
										"total_cost": 3760.96
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 269.92,
										"is_differential_rate": true,
										"rate": 7.5,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 322.4,
										"stage_interest": 22.49,
										"tab_detail": "手续费22.49元/期起，费率7.5%起，单利年化利率约为13.60%",
										"tab_title": "322.4元x12期",
										"total_cost": 3868.92
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 449.88,
										"is_differential_rate": true,
										"rate": 12.5,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 168.69,
										"stage_interest": 18.74,
										"tab_detail": "手续费18.74元/期起，费率12.5%起，单利年化利率约为13.80%",
										"tab_title": "168.69元x24期",
										"total_cost": 4048.88
									}
								],
								"Installments": null,
								"Title": "花呗分期"
							}
						]
					}
				},
				"multi_instalment_info": {
					"first": {
						"detail_info": "低至157.01元 x 24期"
					}
				},
				"multi_insurance": {
					"first": {
						"list": [
							{
								"goods_id": 2220700001,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644809331.80464581.jpg",
								"insurance_type": "micare",
								"market_price": 599,
								"name": "Redmi K50 电竞版 MiCare保障服务",
								"onsale_price": 0,
								"price": 599,
								"short_name": "Redmi K50 电竞版 MiCare保障服务",
								"sku": 41034,
								"type": "2"
							},
							{
								"goods_id": 2220600026,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548371.81671372.jpg",
								"insurance_type": "prolong",
								"market_price": 119,
								"name": "Redmi K50 电竞版 延长保修服务",
								"onsale_price": 59,
								"price": 59,
								"short_name": "Redmi K50 电竞版 延长保修服务",
								"sku": 40964,
								"type": "2"
							},
							{
								"goods_id": 2220600024,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548009.85713550.jpg",
								"insurance_type": "accident",
								"market_price": 249,
								"name": "Redmi K50 电竞版 碎屏保障服务",
								"onsale_price": 124,
								"price": 124,
								"short_name": "Redmi K50 电竞版 碎屏保障服务",
								"sku": 40963,
								"type": "2"
							},
							{
								"goods_id": 2220600025,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548202.10119327.jpg",
								"insurance_type": "accident",
								"market_price": 399,
								"name": "Redmi K50 电竞版 意外保障服务",
								"onsale_price": 0,
								"price": 399,
								"short_name": "Redmi K50 电竞版 意外保障服务",
								"sku": 40962,
								"type": "2"
							}
						]
					}
				},
				"multi_market_price": {
					"first": "3599",
					"second": "3599"
				},
				"multi_price": {
					"first": "2699",
					"second": "2699"
				},
				"multi_service_bargins": {
					"first": [
						{
							"can_fold": true,
							"mutex_type": [
								"service_insurance",
								"service_prolong"
							],
							"service_info": [
								{
									"act_diff": "碎屏延保换新",
									"goods_id": 2220600012,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D41034%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D41034%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "享碎屏、延保、换电池、保值换新4大权益",
									"service_goods_id": "2220700001",
									"service_name": "Redmi K50 电竞版 MiCare保障服务",
									"service_price": "599",
									"service_short_name": "MiCare保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
									"source": "common"
								}
							],
							"service_type_name": "service_micare",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
							"type_name": "尊享服务"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "意外损免费修",
									"goods_id": 2220600012,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40962%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40962%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次意外损坏 官方原厂 免费维修",
									"service_goods_id": "2220600025",
									"service_name": "Redmi K50 电竞版 意外保障服务",
									"service_price": "399",
									"service_short_name": "意外保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
									"source": "common"
								},
								{
									"act_diff": "已省125元",
									"act_price": "124",
									"goods_id": 2220600012,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40963%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40963%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次碎屏 官方原厂 免费维修",
									"service_goods_id": "2220600024",
									"service_name": "Redmi K50 电竞版 碎屏保障服务",
									"service_price": "124",
									"service_short_name": "碎屏保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40342",
									"source": "common"
								}
							],
							"service_type_name": "service_insurance",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
							"type_name": "意外保护"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "已省60元",
									"act_price": "59",
									"goods_id": 2220600012,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40964%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40964%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "性能故障 官方原厂 多次免费维修",
									"service_goods_id": "2220600026",
									"service_name": "Redmi K50 电竞版 延长保修服务",
									"service_price": "59",
									"service_short_name": "延长保修服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
									"source": "common"
								}
							],
							"service_type_name": "service_prolong",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
							"type_name": "延长保修"
						},
						{
							"can_fold": true,
							"service_info": [
								{
									"act_price": "208",
									"goods_id": 2220600012,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400049",
									"service_name": "云空间年卡 200G",
									"service_price": "208",
									"service_short_name": "云空间年卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "58",
									"goods_id": 2220600012,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400048",
									"service_name": "云空间年卡50G",
									"service_price": "58",
									"service_short_name": "云空间年卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "21",
									"goods_id": 2220600012,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2182300057",
									"service_name": "小米云服务空间月卡 200G",
									"service_price": "21",
									"service_short_name": "云空间月卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "6",
									"goods_id": 2220600012,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400054",
									"service_name": "小米云服务空间月卡 50G",
									"service_price": "6",
									"service_short_name": "云空间月卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								}
							],
							"service_type_name": "service_micloud",
							"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
							"type_name": "云空间服务"
						}
					]
				},
				"multi_service_refound_policy": {
					"first": {
						"desc": "支持7天无理由退货",
						"is_support": true
					}
				},
				"multi_stock_channel": {
					"first": "3"
				},
				"name": "Redmi K50 电竞版 12GB+128GB 银翼",
				"next_action_button": null,
				"next_onsale_action_button": null,
				"onsale_action_button": null,
				"page_id": 23164,
				"price": "2699",
				"product_desc": "",
				"product_desc_ext": "",
				"product_gallery_icon": {},
				"product_id": 15615,
				"prop_list": [
					{
						"prop_cfg_id": 8,
						"prop_value_id": 882
					},
					{
						"prop_cfg_id": 2,
						"prop_value_id": 10
					}
				],
				"reduce_price": "直降900元",
				"seckill_action_button": null,
				"sell_point_desc": null,
				"service_refound_policy_list": {
					"list": [
						{
							"goods_id": 2220600012,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/a32fabc4c2cecf39e88b9b018fb8d6ec.png",
							"is_support": true,
							"item_content": "7日内，无人为损坏，包装配件齐全，可以无理由退货。",
							"item_id": 1,
							"item_name": "7天无理由退货",
							"sku": 38977
						},
						{
							"goods_id": 2220600012,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/1769a228b31fb6b994a77c4c4ca16448.png",
							"is_support": true,
							"item_content": "15日内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费更换。",
							"item_id": 2,
							"item_name": "15天质量问题换货",
							"sku": 38977
						},
						{
							"goods_id": 2220600012,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/de2196c347411bab09e7e46e0df5c547.png",
							"is_support": true,
							"item_content": "1年内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费维修。",
							"item_id": 3,
							"item_name": "365天保修",
							"sku": 38977
						}
					],
					"list_new": [
						{
							"is_check": true,
							"is_page_show": true,
							"title": "小米自营"
						},
						{
							"desc": "由小米发货",
							"is_check": true,
							"is_page_show": true,
							"title": "小米发货"
						},
						{
							"is_check": true,
							"is_page_show": true,
							"title": "7天无理由退货"
						},
						{
							"desc": "由小米发货商品(不含有品),单笔满69元免运费;\n由小米有品发货的商品,免运费;\n由第三方商家发货的商品，运费以实际结算金额为准;\n特殊商品需要单独收取运费,具体以实际结算金额为准;优惠券等不能抵扣运费金额;如需无理由退货,用户将承担该商品的退货物流费用;\n使用门店闪送服务，需单独支付10元运费。",
							"is_check": true,
							"is_page_show": false,
							"title": "运费说明"
						},
						{
							"desc": "企业名称：小米通讯技术有限公司\n企业执照注册号：91110108558521630L\n企业地址：北京市海淀区西二旗中路33号院6 号楼9层019号\n企业电话：400-100-5678\n营业期限：2010年08月25日 至 2040年08月24日\n经营范围：开发手机技术、计算机软件及信息技术；技术检测、技术咨询、技术服务、技术转让；计算机技术培训；系统集成；货物进出口、技术进出口、代理进出口；家用电器、通信设备、广播电视设备（不含卫星电视广播、地面接收装置）、机械设备、电子产品、文化用品的批发零售；维修仪器仪表；销售医疗器械I类、II、III类、针纺织品（含家纺家饰）、服装鞋帽、日用杂货、工艺品、文化用品、体育用品、照相器材、卫生用品（含个人护理用品）、钟表眼镜、箱包、家具（不从事实体店铺经营）、小饰品、日用品、乐器、自行车、智能卡；计算机、通讯设备、家用电器、电子产品、机械设备的技术开发、技术服务；销售金银饰品（不含金银质地纪念币）；家用空调的委托生产；委托生产翻译机；销售翻译机、五金交电（不含电动自行车）、厨房用品、陶瓷制品、玻璃制品、玩具、汽车零配件、食用农产品、花卉、苗木、宠物用品、建筑材料、装饰材料、化妆品、珠宝首饰、通讯设备、卫生间用品、农药；生产手机（仅限在海淀区永捷北路2号三层生产及外埠生产）；出版物批发；出版物零售；销售食品。（销售第三类医疗器械以及销售食品以及依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动。）",
							"is_check": true,
							"is_page_show": false,
							"title": "企业信息"
						},
						{
							"is_check": true,
							"is_page_show": false,
							"title": "7天价格保护",
							"url": "https://s1.mi.com/m/faq/giving/index.html?project_id=44"
						}
					],
					"url": ""
				},
				"sku": 38977
			},
			{
				"action_button": {
					"bigtap_source": "bigtap",
					"button_title": "等待到货",
					"is_bigtap": false,
					"mode_change_time": 0,
					"mode_end_time": 0,
					"mode_start_time": 1668051600,
					"occ_timeout": 5000,
					"sale_mode": "webview",
					"sale_mode_line": "first",
					"sale_mode_title": "立即购买",
					"source": "common",
					"use_occ": false,
					"webview": {
						"is_hide_stock": true,
						"url": ""
					}
				},
				"address_type": "common",
				"class_parameters": {
					"list": [
						{
							"bottom_title": "骁龙8 Gen 1",
							"icon": "https://cdn.cnbj0.fds.api.mi-img.com/b2c-mimall-media/f0c04e138bfed2b1ebb589de615236d1.png",
							"is_page_show": true,
							"name": "CPU型号",
							"top_title": "CPU",
							"value": "骁龙8 Gen 1"
						},
						{
							"name": "CPU主频",
							"value": "最高3.0GHz"
						},
						{
							"bottom_title": "6400万像素+800万像素+200万像素",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/7692726e7a1dd34a3b1b4ede8aca020d.png",
							"is_page_show": true,
							"name": "后置摄像头",
							"top_title": "三摄像头",
							"value": "6400万像素+800万像素+200万像素"
						},
						{
							"name": "前置摄像头",
							"value": "2000万像素"
						},
						{
							"name": "屏幕",
							"value": "OLED"
						},
						{
							"bottom_title": "6.67英寸",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/86a3bd46cf4f7f19daa2c3250cf30604.png",
							"is_page_show": true,
							"name": "屏幕尺寸",
							"top_title": "超大屏",
							"value": "6.67英寸"
						},
						{
							"bottom_title": "2400×1080",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/a5ab24dcb527e49f970f13b11e000ab1.png",
							"is_page_show": true,
							"name": "屏幕分辨率",
							"top_title": "屏幕分辨率",
							"value": "2400×1080"
						},
						{
							"bottom_title": "最高12GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/c8ec0829241324e401744da627482560.png",
							"is_page_show": true,
							"name": "运行内存",
							"top_title": "极速畅玩",
							"value": "最高12GB"
						},
						{
							"bottom_title": "最高256GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/8941adac25333e785b9cc78ca11f4f27.png",
							"is_page_show": true,
							"name": "存储容量",
							"top_title": "存储容量",
							"value": "最高256GB"
						},
						{
							"name": "NFC",
							"value": "支持"
						},
						{
							"name": "红外遥控",
							"value": "支持"
						},
						{
							"name": "独立AI键",
							"value": "磁动力弹出式肩键 2.0"
						},
						{
							"name": "指纹识别",
							"value": "侧边指纹"
						},
						{
							"bottom_title": "8.5mm",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/52ad10a73685342e437e44ea3d29cbff.png",
							"is_page_show": true,
							"name": "机身厚度",
							"top_title": "普通厚度",
							"value": "8.5mm"
						},
						{
							"bottom_title": "4700mAh",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/0b4ea0fb21dde2f29df3c20de73539b9.png",
							"is_page_show": true,
							"name": "电池容量",
							"top_title": "超长待机",
							"value": "4700mAh"
						},
						{
							"name": "有线快充",
							"value": "120W"
						},
						{
							"bottom_title": "5G全网通7.0",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/d1b67a407fb2a1ed42c2c0ce15af3318.png",
							"is_page_show": true,
							"name": "网络类型",
							"top_title": "运营商网络",
							"value": "5G全网通7.0"
						},
						{
							"bottom_title": "双卡双待",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/bfd5ba9ae72c365dee42db14dfae4b0f.png",
							"is_page_show": true,
							"name": "网络模式",
							"top_title": "网络模式",
							"value": "双卡双待"
						},
						{
							"name": "数据接口",
							"value": "Type-C"
						}
					],
					"name": "关键参数"
				},
				"commodity_id": 1220600014,
				"customize_template_id": 0,
				"discount_price": "",
				"ext_buy_option": [],
				"gallery_v3": [
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/7ba11fdfecac708e7dcc3b98b537a83a.jpeg",
						"img_url_webp": "",
						"type": "video"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/5061b08df9de9ca1c23ba97b58a340c8.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/cdd534269f9326beaa4a493eab827070.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/22bbbda34d4535698318fc9cf40d0334.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bb7e0537300b002657566cd68920f176.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c090f46e0d07ed6db835fe8f96cf41c1.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0542966b26815adcb450e47b7bca70cc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/eab9a225caeae1c6c9f0673915ad295e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/a9ccfcac5415437e23127e1219966a44.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/1d1d0ab8b94208e69ec929f4d95b3c17.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8348f5f8d83643c9fcfc16dd240c4adc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bd48dcfd217a949798f2748a94bb0b41.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/93d64b29aa8aaff8fc9db0e9a665ad9e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3e3d8b8212a6cc53d5db756531e1ba1d.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0ce722c3e15ed76a9654c20fca0c7faf.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/e85d8a06e1b0ea43d3b8fa397aaf5ebc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/68bb35c26ff3f358362652df9b132d2a.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/577d8e3b1ba50a967e423c8562654b57.jpg",
						"img_url_webp": "",
						"type": "img"
					}
				],
				"goods_id": 2220600011,
				"image_share": "http://cdn.cnbj1.fds.api.mi-img.com/mi-mall/063c4055e2befc30c8042dd9cff559f6.png",
				"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/063c4055e2befc30c8042dd9cff559f6.png",
				"is_customize": false,
				"is_sale": true,
				"is_stock": false,
				"market_price": "3599",
				"multi_buy_limit": {
					"first": 10
				},
				"multi_instalment": {
					"first": {
						"default_instal": "antinstal_m",
						"list": [
							{
								"Bank": "mifinanceinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 79.9,
										"is_differential_rate": false,
										"rate": 2.22,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1226.3,
										"stage_interest": 26.63,
										"tab_detail": "手续费26.63元/期起，费率2.22%起，单利年化利率约为13.32%",
										"tab_title": "1226.3元x3期",
										"total_cost": 3678.9
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 140.36,
										"is_differential_rate": false,
										"rate": 3.9,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 623.22,
										"stage_interest": 23.39,
										"tab_detail": "手续费23.39元/期起，费率3.9%起，单利年化利率约为13.37%",
										"tab_title": "623.22元x6期",
										"total_cost": 3739.36
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 259.13,
										"is_differential_rate": false,
										"rate": 7.2,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 321.51,
										"stage_interest": 21.59,
										"tab_detail": "手续费21.59元/期起，费率7.2%起，单利年化利率约为13.29%",
										"tab_title": "321.51元x12期",
										"total_cost": 3858.13
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 647.82,
										"is_differential_rate": false,
										"rate": 18,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 176.95,
										"stage_interest": 26.99,
										"tab_detail": "手续费26.99元/期起，费率18%起，单利年化利率约为17.28%",
										"tab_title": "176.95元x24期",
										"total_cost": 4246.82
									}
								],
								"Installments": null,
								"Title": "小米分期"
							},
							{
								"Bank": "antinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 82.78,
										"is_differential_rate": true,
										"rate": 2.3,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1227.25,
										"stage_interest": 27.59,
										"tab_detail": "手续费27.59元/期起，费率2.3%起，单利年化利率约为13.70%",
										"tab_title": "1227.25元x3期",
										"total_cost": 3681.78
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 161.96,
										"is_differential_rate": true,
										"rate": 4.5,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 626.82,
										"stage_interest": 26.99,
										"tab_detail": "手续费26.99元/期起，费率4.5%起，单利年化利率约为15.30%",
										"tab_title": "626.82元x6期",
										"total_cost": 3760.96
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 269.92,
										"is_differential_rate": true,
										"rate": 7.5,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 322.4,
										"stage_interest": 22.49,
										"tab_detail": "手续费22.49元/期起，费率7.5%起，单利年化利率约为13.60%",
										"tab_title": "322.4元x12期",
										"total_cost": 3868.92
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 449.88,
										"is_differential_rate": true,
										"rate": 12.5,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 168.69,
										"stage_interest": 18.74,
										"tab_detail": "手续费18.74元/期起，费率12.5%起，单利年化利率约为13.80%",
										"tab_title": "168.69元x24期",
										"total_cost": 4048.88
									}
								],
								"Installments": null,
								"Title": "花呗分期"
							}
						]
					}
				},
				"multi_instalment_info": {
					"first": {
						"detail_info": "低至157.01元 x 24期"
					}
				},
				"multi_insurance": {
					"first": {
						"list": [
							{
								"goods_id": 2220600026,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548371.81671372.jpg",
								"insurance_type": "prolong",
								"market_price": 119,
								"name": "Redmi K50 电竞版 延长保修服务",
								"onsale_price": 59,
								"price": 59,
								"short_name": "Redmi K50 电竞版 延长保修服务",
								"sku": 40964,
								"type": "2"
							},
							{
								"goods_id": 2220600024,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548009.85713550.jpg",
								"insurance_type": "accident",
								"market_price": 249,
								"name": "Redmi K50 电竞版 碎屏保障服务",
								"onsale_price": 124,
								"price": 124,
								"short_name": "Redmi K50 电竞版 碎屏保障服务",
								"sku": 40963,
								"type": "2"
							},
							{
								"goods_id": 2220600025,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548202.10119327.jpg",
								"insurance_type": "accident",
								"market_price": 399,
								"name": "Redmi K50 电竞版 意外保障服务",
								"onsale_price": 0,
								"price": 399,
								"short_name": "Redmi K50 电竞版 意外保障服务",
								"sku": 40962,
								"type": "2"
							},
							{
								"goods_id": 2220700001,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644809331.80464581.jpg",
								"insurance_type": "micare",
								"market_price": 599,
								"name": "Redmi K50 电竞版 MiCare保障服务",
								"onsale_price": 0,
								"price": 599,
								"short_name": "Redmi K50 电竞版 MiCare保障服务",
								"sku": 41034,
								"type": "2"
							}
						]
					}
				},
				"multi_market_price": {
					"first": "3599",
					"second": "3599"
				},
				"multi_price": {
					"first": "2699",
					"second": "2699"
				},
				"multi_service_bargins": {
					"first": [
						{
							"can_fold": true,
							"mutex_type": [
								"service_insurance",
								"service_prolong"
							],
							"service_info": [
								{
									"act_diff": "碎屏延保换新",
									"goods_id": 2220600011,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D41034%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D41034%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "享碎屏、延保、换电池、保值换新4大权益",
									"service_goods_id": "2220700001",
									"service_name": "Redmi K50 电竞版 MiCare保障服务",
									"service_price": "599",
									"service_short_name": "MiCare保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
									"source": "common"
								}
							],
							"service_type_name": "service_micare",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
							"type_name": "尊享服务"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "意外损免费修",
									"goods_id": 2220600011,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40962%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40962%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次意外损坏 官方原厂 免费维修",
									"service_goods_id": "2220600025",
									"service_name": "Redmi K50 电竞版 意外保障服务",
									"service_price": "399",
									"service_short_name": "意外保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
									"source": "common"
								},
								{
									"act_diff": "已省125元",
									"act_price": "124",
									"goods_id": 2220600011,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40963%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40963%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次碎屏 官方原厂 免费维修",
									"service_goods_id": "2220600024",
									"service_name": "Redmi K50 电竞版 碎屏保障服务",
									"service_price": "124",
									"service_short_name": "碎屏保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40342",
									"source": "common"
								}
							],
							"service_type_name": "service_insurance",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
							"type_name": "意外保护"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "已省60元",
									"act_price": "59",
									"goods_id": 2220600011,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40964%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40964%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "性能故障 官方原厂 多次免费维修",
									"service_goods_id": "2220600026",
									"service_name": "Redmi K50 电竞版 延长保修服务",
									"service_price": "59",
									"service_short_name": "延长保修服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
									"source": "common"
								}
							],
							"service_type_name": "service_prolong",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
							"type_name": "延长保修"
						},
						{
							"can_fold": true,
							"service_info": [
								{
									"act_price": "208",
									"goods_id": 2220600011,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400049",
									"service_name": "云空间年卡 200G",
									"service_price": "208",
									"service_short_name": "云空间年卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "58",
									"goods_id": 2220600011,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400048",
									"service_name": "云空间年卡50G",
									"service_price": "58",
									"service_short_name": "云空间年卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "21",
									"goods_id": 2220600011,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2182300057",
									"service_name": "小米云服务空间月卡 200G",
									"service_price": "21",
									"service_short_name": "云空间月卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "6",
									"goods_id": 2220600011,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400054",
									"service_name": "小米云服务空间月卡 50G",
									"service_price": "6",
									"service_short_name": "云空间月卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								}
							],
							"service_type_name": "service_micloud",
							"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
							"type_name": "云空间服务"
						}
					]
				},
				"multi_service_refound_policy": {
					"first": {
						"desc": "支持7天无理由退货",
						"is_support": true
					}
				},
				"multi_stock_channel": {
					"first": "3"
				},
				"name": "Redmi K50 电竞版 12GB+128GB 暗影",
				"next_action_button": null,
				"next_onsale_action_button": null,
				"onsale_action_button": null,
				"page_id": 23164,
				"price": "2699",
				"product_desc": "",
				"product_desc_ext": "",
				"product_gallery_icon": {},
				"product_id": 15615,
				"prop_list": [
					{
						"prop_cfg_id": 8,
						"prop_value_id": 882
					},
					{
						"prop_cfg_id": 2,
						"prop_value_id": 3
					}
				],
				"reduce_price": "直降900元",
				"seckill_action_button": null,
				"sell_point_desc": null,
				"service_refound_policy_list": {
					"list": [
						{
							"goods_id": 2220600011,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/a32fabc4c2cecf39e88b9b018fb8d6ec.png",
							"is_support": true,
							"item_content": "7日内，无人为损坏，包装配件齐全，可以无理由退货。",
							"item_id": 1,
							"item_name": "7天无理由退货",
							"sku": 38975
						},
						{
							"goods_id": 2220600011,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/1769a228b31fb6b994a77c4c4ca16448.png",
							"is_support": true,
							"item_content": "15日内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费更换。",
							"item_id": 2,
							"item_name": "15天质量问题换货",
							"sku": 38975
						},
						{
							"goods_id": 2220600011,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/de2196c347411bab09e7e46e0df5c547.png",
							"is_support": true,
							"item_content": "1年内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费维修。",
							"item_id": 3,
							"item_name": "365天保修",
							"sku": 38975
						}
					],
					"list_new": [
						{
							"is_check": true,
							"is_page_show": true,
							"title": "小米自营"
						},
						{
							"desc": "由小米发货",
							"is_check": true,
							"is_page_show": true,
							"title": "小米发货"
						},
						{
							"is_check": true,
							"is_page_show": true,
							"title": "7天无理由退货"
						},
						{
							"desc": "由小米发货商品(不含有品),单笔满69元免运费;\n由小米有品发货的商品,免运费;\n由第三方商家发货的商品，运费以实际结算金额为准;\n特殊商品需要单独收取运费,具体以实际结算金额为准;优惠券等不能抵扣运费金额;如需无理由退货,用户将承担该商品的退货物流费用;\n使用门店闪送服务，需单独支付10元运费。",
							"is_check": true,
							"is_page_show": false,
							"title": "运费说明"
						},
						{
							"desc": "企业名称：小米通讯技术有限公司\n企业执照注册号：91110108558521630L\n企业地址：北京市海淀区西二旗中路33号院6 号楼9层019号\n企业电话：400-100-5678\n营业期限：2010年08月25日 至 2040年08月24日\n经营范围：开发手机技术、计算机软件及信息技术；技术检测、技术咨询、技术服务、技术转让；计算机技术培训；系统集成；货物进出口、技术进出口、代理进出口；家用电器、通信设备、广播电视设备（不含卫星电视广播、地面接收装置）、机械设备、电子产品、文化用品的批发零售；维修仪器仪表；销售医疗器械I类、II、III类、针纺织品（含家纺家饰）、服装鞋帽、日用杂货、工艺品、文化用品、体育用品、照相器材、卫生用品（含个人护理用品）、钟表眼镜、箱包、家具（不从事实体店铺经营）、小饰品、日用品、乐器、自行车、智能卡；计算机、通讯设备、家用电器、电子产品、机械设备的技术开发、技术服务；销售金银饰品（不含金银质地纪念币）；家用空调的委托生产；委托生产翻译机；销售翻译机、五金交电（不含电动自行车）、厨房用品、陶瓷制品、玻璃制品、玩具、汽车零配件、食用农产品、花卉、苗木、宠物用品、建筑材料、装饰材料、化妆品、珠宝首饰、通讯设备、卫生间用品、农药；生产手机（仅限在海淀区永捷北路2号三层生产及外埠生产）；出版物批发；出版物零售；销售食品。（销售第三类医疗器械以及销售食品以及依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动。）",
							"is_check": true,
							"is_page_show": false,
							"title": "企业信息"
						},
						{
							"is_check": true,
							"is_page_show": false,
							"title": "7天价格保护",
							"url": "https://s1.mi.com/m/faq/giving/index.html?project_id=44"
						}
					],
					"url": ""
				},
				"sku": 38975
			},
			{
				"action_button": {
					"bigtap_source": "bigtap",
					"button_title": "等待到货",
					"is_bigtap": false,
					"mode_change_time": 0,
					"mode_end_time": 0,
					"mode_start_time": 1668051600,
					"occ_timeout": 5000,
					"sale_mode": "webview",
					"sale_mode_line": "first",
					"sale_mode_title": "立即购买",
					"source": "common",
					"use_occ": false,
					"webview": {
						"is_hide_stock": true,
						"url": ""
					}
				},
				"address_type": "common",
				"class_parameters": {
					"list": [
						{
							"bottom_title": "骁龙8 Gen 1",
							"icon": "https://cdn.cnbj0.fds.api.mi-img.com/b2c-mimall-media/f0c04e138bfed2b1ebb589de615236d1.png",
							"is_page_show": true,
							"name": "CPU型号",
							"top_title": "CPU",
							"value": "骁龙8 Gen 1"
						},
						{
							"name": "CPU主频",
							"value": "最高3.0GHz"
						},
						{
							"bottom_title": "6400万像素+800万像素+200万像素",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/7692726e7a1dd34a3b1b4ede8aca020d.png",
							"is_page_show": true,
							"name": "后置摄像头",
							"top_title": "三摄像头",
							"value": "6400万像素+800万像素+200万像素"
						},
						{
							"name": "前置摄像头",
							"value": "2000万像素"
						},
						{
							"name": "屏幕",
							"value": "OLED"
						},
						{
							"bottom_title": "6.67英寸",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/86a3bd46cf4f7f19daa2c3250cf30604.png",
							"is_page_show": true,
							"name": "屏幕尺寸",
							"top_title": "超大屏",
							"value": "6.67英寸"
						},
						{
							"bottom_title": "2400×1080",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/a5ab24dcb527e49f970f13b11e000ab1.png",
							"is_page_show": true,
							"name": "屏幕分辨率",
							"top_title": "屏幕分辨率",
							"value": "2400×1080"
						},
						{
							"bottom_title": "最高12GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/c8ec0829241324e401744da627482560.png",
							"is_page_show": true,
							"name": "运行内存",
							"top_title": "极速畅玩",
							"value": "最高12GB"
						},
						{
							"bottom_title": "最高256GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/8941adac25333e785b9cc78ca11f4f27.png",
							"is_page_show": true,
							"name": "存储容量",
							"top_title": "存储容量",
							"value": "最高256GB"
						},
						{
							"name": "NFC",
							"value": "支持"
						},
						{
							"name": "红外遥控",
							"value": "支持"
						},
						{
							"name": "独立AI键",
							"value": "磁动力弹出式肩键 2.0"
						},
						{
							"name": "指纹识别",
							"value": "侧边指纹"
						},
						{
							"bottom_title": "8.5mm",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/52ad10a73685342e437e44ea3d29cbff.png",
							"is_page_show": true,
							"name": "机身厚度",
							"top_title": "普通厚度",
							"value": "8.5mm"
						},
						{
							"bottom_title": "4700mAh",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/0b4ea0fb21dde2f29df3c20de73539b9.png",
							"is_page_show": true,
							"name": "电池容量",
							"top_title": "超长待机",
							"value": "4700mAh"
						},
						{
							"name": "有线快充",
							"value": "120W"
						},
						{
							"bottom_title": "5G全网通7.0",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/d1b67a407fb2a1ed42c2c0ce15af3318.png",
							"is_page_show": true,
							"name": "网络类型",
							"top_title": "运营商网络",
							"value": "5G全网通7.0"
						},
						{
							"bottom_title": "双卡双待",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/bfd5ba9ae72c365dee42db14dfae4b0f.png",
							"is_page_show": true,
							"name": "网络模式",
							"top_title": "网络模式",
							"value": "双卡双待"
						},
						{
							"name": "数据接口",
							"value": "Type-C"
						}
					],
					"name": "关键参数"
				},
				"commodity_id": 1220600014,
				"customize_template_id": 0,
				"discount_price": "",
				"ext_buy_option": [],
				"gallery_v3": [
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/7ba11fdfecac708e7dcc3b98b537a83a.jpeg",
						"img_url_webp": "",
						"type": "video"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/5061b08df9de9ca1c23ba97b58a340c8.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/cdd534269f9326beaa4a493eab827070.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/22bbbda34d4535698318fc9cf40d0334.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bb7e0537300b002657566cd68920f176.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c090f46e0d07ed6db835fe8f96cf41c1.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0542966b26815adcb450e47b7bca70cc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/eab9a225caeae1c6c9f0673915ad295e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/a9ccfcac5415437e23127e1219966a44.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/1d1d0ab8b94208e69ec929f4d95b3c17.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8348f5f8d83643c9fcfc16dd240c4adc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bd48dcfd217a949798f2748a94bb0b41.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/93d64b29aa8aaff8fc9db0e9a665ad9e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3e3d8b8212a6cc53d5db756531e1ba1d.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0ce722c3e15ed76a9654c20fca0c7faf.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/e85d8a06e1b0ea43d3b8fa397aaf5ebc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/68bb35c26ff3f358362652df9b132d2a.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/577d8e3b1ba50a967e423c8562654b57.jpg",
						"img_url_webp": "",
						"type": "img"
					}
				],
				"goods_id": 2220600010,
				"image_share": "http://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f5d5806d6807671fd2a28d9a944b5358.png",
				"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/f5d5806d6807671fd2a28d9a944b5358.png",
				"is_customize": false,
				"is_sale": true,
				"is_stock": false,
				"market_price": "3599",
				"multi_buy_limit": {
					"first": 10
				},
				"multi_instalment": {
					"first": {
						"default_instal": "antinstal_m",
						"list": [
							{
								"Bank": "mifinanceinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 79.9,
										"is_differential_rate": false,
										"rate": 2.22,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1226.3,
										"stage_interest": 26.63,
										"tab_detail": "手续费26.63元/期起，费率2.22%起，单利年化利率约为13.32%",
										"tab_title": "1226.3元x3期",
										"total_cost": 3678.9
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 140.36,
										"is_differential_rate": false,
										"rate": 3.9,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 623.22,
										"stage_interest": 23.39,
										"tab_detail": "手续费23.39元/期起，费率3.9%起，单利年化利率约为13.37%",
										"tab_title": "623.22元x6期",
										"total_cost": 3739.36
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 259.13,
										"is_differential_rate": false,
										"rate": 7.2,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 321.51,
										"stage_interest": 21.59,
										"tab_detail": "手续费21.59元/期起，费率7.2%起，单利年化利率约为13.29%",
										"tab_title": "321.51元x12期",
										"total_cost": 3858.13
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 647.82,
										"is_differential_rate": false,
										"rate": 18,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 176.95,
										"stage_interest": 26.99,
										"tab_detail": "手续费26.99元/期起，费率18%起，单利年化利率约为17.28%",
										"tab_title": "176.95元x24期",
										"total_cost": 4246.82
									}
								],
								"Installments": null,
								"Title": "小米分期"
							},
							{
								"Bank": "antinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 82.78,
										"is_differential_rate": true,
										"rate": 2.3,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1227.25,
										"stage_interest": 27.59,
										"tab_detail": "手续费27.59元/期起，费率2.3%起，单利年化利率约为13.70%",
										"tab_title": "1227.25元x3期",
										"total_cost": 3681.78
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 161.96,
										"is_differential_rate": true,
										"rate": 4.5,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 626.82,
										"stage_interest": 26.99,
										"tab_detail": "手续费26.99元/期起，费率4.5%起，单利年化利率约为15.30%",
										"tab_title": "626.82元x6期",
										"total_cost": 3760.96
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 269.92,
										"is_differential_rate": true,
										"rate": 7.5,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 322.4,
										"stage_interest": 22.49,
										"tab_detail": "手续费22.49元/期起，费率7.5%起，单利年化利率约为13.60%",
										"tab_title": "322.4元x12期",
										"total_cost": 3868.92
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 449.88,
										"is_differential_rate": true,
										"rate": 12.5,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 168.69,
										"stage_interest": 18.74,
										"tab_detail": "手续费18.74元/期起，费率12.5%起，单利年化利率约为13.80%",
										"tab_title": "168.69元x24期",
										"total_cost": 4048.88
									}
								],
								"Installments": null,
								"Title": "花呗分期"
							}
						]
					}
				},
				"multi_instalment_info": {
					"first": {
						"detail_info": "低至157.01元 x 24期"
					}
				},
				"multi_insurance": {
					"first": {
						"list": [
							{
								"goods_id": 2220600024,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548009.85713550.jpg",
								"insurance_type": "accident",
								"market_price": 249,
								"name": "Redmi K50 电竞版 碎屏保障服务",
								"onsale_price": 124,
								"price": 124,
								"short_name": "Redmi K50 电竞版 碎屏保障服务",
								"sku": 40963,
								"type": "2"
							},
							{
								"goods_id": 2220600025,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548202.10119327.jpg",
								"insurance_type": "accident",
								"market_price": 399,
								"name": "Redmi K50 电竞版 意外保障服务",
								"onsale_price": 0,
								"price": 399,
								"short_name": "Redmi K50 电竞版 意外保障服务",
								"sku": 40962,
								"type": "2"
							},
							{
								"goods_id": 2220700001,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644809331.80464581.jpg",
								"insurance_type": "micare",
								"market_price": 599,
								"name": "Redmi K50 电竞版 MiCare保障服务",
								"onsale_price": 0,
								"price": 599,
								"short_name": "Redmi K50 电竞版 MiCare保障服务",
								"sku": 41034,
								"type": "2"
							},
							{
								"goods_id": 2220600026,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548371.81671372.jpg",
								"insurance_type": "prolong",
								"market_price": 119,
								"name": "Redmi K50 电竞版 延长保修服务",
								"onsale_price": 59,
								"price": 59,
								"short_name": "Redmi K50 电竞版 延长保修服务",
								"sku": 40964,
								"type": "2"
							}
						]
					}
				},
				"multi_market_price": {
					"first": "3599",
					"second": "3599"
				},
				"multi_price": {
					"first": "2699",
					"second": "2699"
				},
				"multi_service_bargins": {
					"first": [
						{
							"can_fold": true,
							"mutex_type": [
								"service_insurance",
								"service_prolong"
							],
							"service_info": [
								{
									"act_diff": "碎屏延保换新",
									"goods_id": 2220600010,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D41034%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D41034%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "享碎屏、延保、换电池、保值换新4大权益",
									"service_goods_id": "2220700001",
									"service_name": "Redmi K50 电竞版 MiCare保障服务",
									"service_price": "599",
									"service_short_name": "MiCare保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
									"source": "common"
								}
							],
							"service_type_name": "service_micare",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
							"type_name": "尊享服务"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "意外损免费修",
									"goods_id": 2220600010,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40962%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40962%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次意外损坏 官方原厂 免费维修",
									"service_goods_id": "2220600025",
									"service_name": "Redmi K50 电竞版 意外保障服务",
									"service_price": "399",
									"service_short_name": "意外保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
									"source": "common"
								},
								{
									"act_diff": "已省125元",
									"act_price": "124",
									"goods_id": 2220600010,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40963%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40963%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次碎屏 官方原厂 免费维修",
									"service_goods_id": "2220600024",
									"service_name": "Redmi K50 电竞版 碎屏保障服务",
									"service_price": "124",
									"service_short_name": "碎屏保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40342",
									"source": "common"
								}
							],
							"service_type_name": "service_insurance",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
							"type_name": "意外保护"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "已省60元",
									"act_price": "59",
									"goods_id": 2220600010,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40964%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40964%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "性能故障 官方原厂 多次免费维修",
									"service_goods_id": "2220600026",
									"service_name": "Redmi K50 电竞版 延长保修服务",
									"service_price": "59",
									"service_short_name": "延长保修服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
									"source": "common"
								}
							],
							"service_type_name": "service_prolong",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
							"type_name": "延长保修"
						},
						{
							"can_fold": true,
							"service_info": [
								{
									"act_price": "208",
									"goods_id": 2220600010,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400049",
									"service_name": "云空间年卡 200G",
									"service_price": "208",
									"service_short_name": "云空间年卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "58",
									"goods_id": 2220600010,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400048",
									"service_name": "云空间年卡50G",
									"service_price": "58",
									"service_short_name": "云空间年卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "21",
									"goods_id": 2220600010,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2182300057",
									"service_name": "小米云服务空间月卡 200G",
									"service_price": "21",
									"service_short_name": "云空间月卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "6",
									"goods_id": 2220600010,
									"goods_name": "Redmi K50 电竞版 12GB+128GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400054",
									"service_name": "小米云服务空间月卡 50G",
									"service_price": "6",
									"service_short_name": "云空间月卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								}
							],
							"service_type_name": "service_micloud",
							"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
							"type_name": "云空间服务"
						}
					]
				},
				"multi_service_refound_policy": {
					"first": {
						"desc": "支持7天无理由退货",
						"is_support": true
					}
				},
				"multi_stock_channel": {
					"first": "3"
				},
				"name": "Redmi K50 电竞版 12GB+128GB 冰斩",
				"next_action_button": null,
				"next_onsale_action_button": null,
				"onsale_action_button": null,
				"page_id": 23164,
				"price": "2699",
				"product_desc": "",
				"product_desc_ext": "",
				"product_gallery_icon": {},
				"product_id": 15615,
				"prop_list": [
					{
						"prop_cfg_id": 8,
						"prop_value_id": 882
					},
					{
						"prop_cfg_id": 2,
						"prop_value_id": 4
					}
				],
				"reduce_price": "直降900元",
				"seckill_action_button": null,
				"sell_point_desc": null,
				"service_refound_policy_list": {
					"list": [
						{
							"goods_id": 2220600010,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/a32fabc4c2cecf39e88b9b018fb8d6ec.png",
							"is_support": true,
							"item_content": "7日内，无人为损坏，包装配件齐全，可以无理由退货。",
							"item_id": 1,
							"item_name": "7天无理由退货",
							"sku": 38972
						},
						{
							"goods_id": 2220600010,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/1769a228b31fb6b994a77c4c4ca16448.png",
							"is_support": true,
							"item_content": "15日内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费更换。",
							"item_id": 2,
							"item_name": "15天质量问题换货",
							"sku": 38972
						},
						{
							"goods_id": 2220600010,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/de2196c347411bab09e7e46e0df5c547.png",
							"is_support": true,
							"item_content": "1年内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费维修。",
							"item_id": 3,
							"item_name": "365天保修",
							"sku": 38972
						}
					],
					"list_new": [
						{
							"is_check": true,
							"is_page_show": true,
							"title": "小米自营"
						},
						{
							"desc": "由小米发货",
							"is_check": true,
							"is_page_show": true,
							"title": "小米发货"
						},
						{
							"is_check": true,
							"is_page_show": true,
							"title": "7天无理由退货"
						},
						{
							"desc": "由小米发货商品(不含有品),单笔满69元免运费;\n由小米有品发货的商品,免运费;\n由第三方商家发货的商品，运费以实际结算金额为准;\n特殊商品需要单独收取运费,具体以实际结算金额为准;优惠券等不能抵扣运费金额;如需无理由退货,用户将承担该商品的退货物流费用;\n使用门店闪送服务，需单独支付10元运费。",
							"is_check": true,
							"is_page_show": false,
							"title": "运费说明"
						},
						{
							"desc": "企业名称：小米通讯技术有限公司\n企业执照注册号：91110108558521630L\n企业地址：北京市海淀区西二旗中路33号院6 号楼9层019号\n企业电话：400-100-5678\n营业期限：2010年08月25日 至 2040年08月24日\n经营范围：开发手机技术、计算机软件及信息技术；技术检测、技术咨询、技术服务、技术转让；计算机技术培训；系统集成；货物进出口、技术进出口、代理进出口；家用电器、通信设备、广播电视设备（不含卫星电视广播、地面接收装置）、机械设备、电子产品、文化用品的批发零售；维修仪器仪表；销售医疗器械I类、II、III类、针纺织品（含家纺家饰）、服装鞋帽、日用杂货、工艺品、文化用品、体育用品、照相器材、卫生用品（含个人护理用品）、钟表眼镜、箱包、家具（不从事实体店铺经营）、小饰品、日用品、乐器、自行车、智能卡；计算机、通讯设备、家用电器、电子产品、机械设备的技术开发、技术服务；销售金银饰品（不含金银质地纪念币）；家用空调的委托生产；委托生产翻译机；销售翻译机、五金交电（不含电动自行车）、厨房用品、陶瓷制品、玻璃制品、玩具、汽车零配件、食用农产品、花卉、苗木、宠物用品、建筑材料、装饰材料、化妆品、珠宝首饰、通讯设备、卫生间用品、农药；生产手机（仅限在海淀区永捷北路2号三层生产及外埠生产）；出版物批发；出版物零售；销售食品。（销售第三类医疗器械以及销售食品以及依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动。）",
							"is_check": true,
							"is_page_show": false,
							"title": "企业信息"
						},
						{
							"is_check": true,
							"is_page_show": false,
							"title": "7天价格保护",
							"url": "https://s1.mi.com/m/faq/giving/index.html?project_id=44"
						}
					],
					"url": ""
				},
				"sku": 38972
			},
			{
				"action_button": {
					"bigtap_source": "bigtap",
					"button_title": "等待到货",
					"is_bigtap": false,
					"mode_change_time": 0,
					"mode_end_time": 0,
					"mode_start_time": 1668051600,
					"occ_timeout": 5000,
					"sale_mode": "webview",
					"sale_mode_line": "first",
					"sale_mode_title": "立即购买",
					"source": "common",
					"use_occ": false,
					"webview": {
						"is_hide_stock": true,
						"url": ""
					}
				},
				"address_type": "common",
				"class_parameters": {
					"list": [
						{
							"bottom_title": "骁龙8 Gen 1",
							"icon": "https://cdn.cnbj0.fds.api.mi-img.com/b2c-mimall-media/f0c04e138bfed2b1ebb589de615236d1.png",
							"is_page_show": true,
							"name": "CPU型号",
							"top_title": "CPU",
							"value": "骁龙8 Gen 1"
						},
						{
							"name": "CPU主频",
							"value": "最高3.0GHz"
						},
						{
							"bottom_title": "6400万像素+800万像素+200万像素",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/7692726e7a1dd34a3b1b4ede8aca020d.png",
							"is_page_show": true,
							"name": "后置摄像头",
							"top_title": "三摄像头",
							"value": "6400万像素+800万像素+200万像素"
						},
						{
							"name": "前置摄像头",
							"value": "2000万像素"
						},
						{
							"name": "屏幕",
							"value": "OLED"
						},
						{
							"bottom_title": "6.67英寸",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/86a3bd46cf4f7f19daa2c3250cf30604.png",
							"is_page_show": true,
							"name": "屏幕尺寸",
							"top_title": "超大屏",
							"value": "6.67英寸"
						},
						{
							"bottom_title": "2400×1080",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/a5ab24dcb527e49f970f13b11e000ab1.png",
							"is_page_show": true,
							"name": "屏幕分辨率",
							"top_title": "屏幕分辨率",
							"value": "2400×1080"
						},
						{
							"bottom_title": "最高12GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/c8ec0829241324e401744da627482560.png",
							"is_page_show": true,
							"name": "运行内存",
							"top_title": "极速畅玩",
							"value": "最高12GB"
						},
						{
							"bottom_title": "最高256GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/8941adac25333e785b9cc78ca11f4f27.png",
							"is_page_show": true,
							"name": "存储容量",
							"top_title": "存储容量",
							"value": "最高256GB"
						},
						{
							"name": "NFC",
							"value": "支持"
						},
						{
							"name": "红外遥控",
							"value": "支持"
						},
						{
							"name": "独立AI键",
							"value": "磁动力弹出式肩键 2.0"
						},
						{
							"name": "指纹识别",
							"value": "侧边指纹"
						},
						{
							"bottom_title": "8.5mm",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/52ad10a73685342e437e44ea3d29cbff.png",
							"is_page_show": true,
							"name": "机身厚度",
							"top_title": "普通厚度",
							"value": "8.5mm"
						},
						{
							"bottom_title": "4700mAh",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/0b4ea0fb21dde2f29df3c20de73539b9.png",
							"is_page_show": true,
							"name": "电池容量",
							"top_title": "超长待机",
							"value": "4700mAh"
						},
						{
							"name": "有线快充",
							"value": "120W"
						},
						{
							"bottom_title": "5G全网通7.0",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/d1b67a407fb2a1ed42c2c0ce15af3318.png",
							"is_page_show": true,
							"name": "网络类型",
							"top_title": "运营商网络",
							"value": "5G全网通7.0"
						},
						{
							"bottom_title": "双卡双待",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/bfd5ba9ae72c365dee42db14dfae4b0f.png",
							"is_page_show": true,
							"name": "网络模式",
							"top_title": "网络模式",
							"value": "双卡双待"
						},
						{
							"name": "数据接口",
							"value": "Type-C"
						}
					],
					"name": "关键参数"
				},
				"commodity_id": 1220600015,
				"customize_template_id": 0,
				"discount_price": "",
				"ext_buy_option": [],
				"gallery_v3": [
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/7ba11fdfecac708e7dcc3b98b537a83a.jpeg",
						"img_url_webp": "",
						"type": "video"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/5061b08df9de9ca1c23ba97b58a340c8.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/cdd534269f9326beaa4a493eab827070.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/22bbbda34d4535698318fc9cf40d0334.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bb7e0537300b002657566cd68920f176.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c090f46e0d07ed6db835fe8f96cf41c1.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0542966b26815adcb450e47b7bca70cc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/eab9a225caeae1c6c9f0673915ad295e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/a9ccfcac5415437e23127e1219966a44.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/1d1d0ab8b94208e69ec929f4d95b3c17.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8348f5f8d83643c9fcfc16dd240c4adc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bd48dcfd217a949798f2748a94bb0b41.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/93d64b29aa8aaff8fc9db0e9a665ad9e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3e3d8b8212a6cc53d5db756531e1ba1d.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0ce722c3e15ed76a9654c20fca0c7faf.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/e85d8a06e1b0ea43d3b8fa397aaf5ebc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/68bb35c26ff3f358362652df9b132d2a.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/577d8e3b1ba50a967e423c8562654b57.jpg",
						"img_url_webp": "",
						"type": "img"
					}
				],
				"goods_id": 2220600015,
				"image_share": "http://cdn.cnbj1.fds.api.mi-img.com/mi-mall/8ec65803d049f8d68b6a092fb051590a.png",
				"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8ec65803d049f8d68b6a092fb051590a.png",
				"is_customize": false,
				"is_sale": true,
				"is_stock": false,
				"market_price": "3899",
				"multi_buy_limit": {
					"first": 10
				},
				"multi_instalment": {
					"first": {
						"default_instal": "antinstal_m",
						"list": [
							{
								"Bank": "mifinanceinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 86.56,
										"is_differential_rate": false,
										"rate": 2.22,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1328.52,
										"stage_interest": 28.85,
										"tab_detail": "手续费28.85元/期起，费率2.22%起，单利年化利率约为13.32%",
										"tab_title": "1328.52元x3期",
										"total_cost": 3985.56
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 152.06,
										"is_differential_rate": false,
										"rate": 3.9,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 675.17,
										"stage_interest": 25.34,
										"tab_detail": "手续费25.34元/期起，费率3.9%起，单利年化利率约为13.37%",
										"tab_title": "675.17元x6期",
										"total_cost": 4051.06
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 280.73,
										"is_differential_rate": false,
										"rate": 7.2,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 348.31,
										"stage_interest": 23.39,
										"tab_detail": "手续费23.39元/期起，费率7.2%起，单利年化利率约为13.29%",
										"tab_title": "348.31元x12期",
										"total_cost": 4179.73
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 701.82,
										"is_differential_rate": false,
										"rate": 18,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 191.7,
										"stage_interest": 29.24,
										"tab_detail": "手续费29.24元/期起，费率18%起，单利年化利率约为17.28%",
										"tab_title": "191.7元x24期",
										"total_cost": 4600.82
									}
								],
								"Installments": null,
								"Title": "小米分期"
							},
							{
								"Bank": "antinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 89.68,
										"is_differential_rate": true,
										"rate": 2.3,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1329.55,
										"stage_interest": 29.89,
										"tab_detail": "手续费29.89元/期起，费率2.3%起，单利年化利率约为13.70%",
										"tab_title": "1329.55元x3期",
										"total_cost": 3988.68
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 175.46,
										"is_differential_rate": true,
										"rate": 4.5,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 679.07,
										"stage_interest": 29.24,
										"tab_detail": "手续费29.24元/期起，费率4.5%起，单利年化利率约为15.30%",
										"tab_title": "679.07元x6期",
										"total_cost": 4074.46
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 292.42,
										"is_differential_rate": true,
										"rate": 7.5,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 349.27,
										"stage_interest": 24.36,
										"tab_detail": "手续费24.36元/期起，费率7.5%起，单利年化利率约为13.60%",
										"tab_title": "349.27元x12期",
										"total_cost": 4191.42
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 487.38,
										"is_differential_rate": true,
										"rate": 12.5,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 182.75,
										"stage_interest": 20.3,
										"tab_detail": "手续费20.3元/期起，费率12.5%起，单利年化利率约为13.80%",
										"tab_title": "182.75元x24期",
										"total_cost": 4386.38
									}
								],
								"Installments": null,
								"Title": "花呗分期"
							}
						]
					}
				},
				"multi_instalment_info": {
					"first": {
						"detail_info": "低至170.57元 x 24期"
					}
				},
				"multi_insurance": {
					"first": {
						"list": [
							{
								"goods_id": 2220700001,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644809331.80464581.jpg",
								"insurance_type": "micare",
								"market_price": 599,
								"name": "Redmi K50 电竞版 MiCare保障服务",
								"onsale_price": 0,
								"price": 599,
								"short_name": "Redmi K50 电竞版 MiCare保障服务",
								"sku": 41034,
								"type": "2"
							},
							{
								"goods_id": 2220600026,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548371.81671372.jpg",
								"insurance_type": "prolong",
								"market_price": 119,
								"name": "Redmi K50 电竞版 延长保修服务",
								"onsale_price": 59,
								"price": 59,
								"short_name": "Redmi K50 电竞版 延长保修服务",
								"sku": 40964,
								"type": "2"
							},
							{
								"goods_id": 2220600024,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548009.85713550.jpg",
								"insurance_type": "accident",
								"market_price": 249,
								"name": "Redmi K50 电竞版 碎屏保障服务",
								"onsale_price": 124,
								"price": 124,
								"short_name": "Redmi K50 电竞版 碎屏保障服务",
								"sku": 40963,
								"type": "2"
							},
							{
								"goods_id": 2220600025,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548202.10119327.jpg",
								"insurance_type": "accident",
								"market_price": 399,
								"name": "Redmi K50 电竞版 意外保障服务",
								"onsale_price": 0,
								"price": 399,
								"short_name": "Redmi K50 电竞版 意外保障服务",
								"sku": 40962,
								"type": "2"
							}
						]
					}
				},
				"multi_market_price": {
					"first": "3899",
					"second": "3899"
				},
				"multi_price": {
					"first": "3299",
					"second": "3299"
				},
				"multi_service_bargins": {
					"first": [
						{
							"can_fold": true,
							"mutex_type": [
								"service_insurance",
								"service_prolong"
							],
							"service_info": [
								{
									"act_diff": "碎屏延保换新",
									"goods_id": 2220600015,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D41034%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D41034%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "享碎屏、延保、换电池、保值换新4大权益",
									"service_goods_id": "2220700001",
									"service_name": "Redmi K50 电竞版 MiCare保障服务",
									"service_price": "599",
									"service_short_name": "MiCare保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
									"source": "common"
								}
							],
							"service_type_name": "service_micare",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
							"type_name": "尊享服务"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "意外损免费修",
									"goods_id": 2220600015,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40962%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40962%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次意外损坏 官方原厂 免费维修",
									"service_goods_id": "2220600025",
									"service_name": "Redmi K50 电竞版 意外保障服务",
									"service_price": "399",
									"service_short_name": "意外保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
									"source": "common"
								},
								{
									"act_diff": "已省125元",
									"act_price": "124",
									"goods_id": 2220600015,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40963%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40963%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次碎屏 官方原厂 免费维修",
									"service_goods_id": "2220600024",
									"service_name": "Redmi K50 电竞版 碎屏保障服务",
									"service_price": "124",
									"service_short_name": "碎屏保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40342",
									"source": "common"
								}
							],
							"service_type_name": "service_insurance",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
							"type_name": "意外保护"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "已省60元",
									"act_price": "59",
									"goods_id": 2220600015,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40964%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40964%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "性能故障 官方原厂 多次免费维修",
									"service_goods_id": "2220600026",
									"service_name": "Redmi K50 电竞版 延长保修服务",
									"service_price": "59",
									"service_short_name": "延长保修服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
									"source": "common"
								}
							],
							"service_type_name": "service_prolong",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
							"type_name": "延长保修"
						},
						{
							"can_fold": true,
							"service_info": [
								{
									"act_price": "208",
									"goods_id": 2220600015,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400049",
									"service_name": "云空间年卡 200G",
									"service_price": "208",
									"service_short_name": "云空间年卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "58",
									"goods_id": 2220600015,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400048",
									"service_name": "云空间年卡50G",
									"service_price": "58",
									"service_short_name": "云空间年卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "21",
									"goods_id": 2220600015,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2182300057",
									"service_name": "小米云服务空间月卡 200G",
									"service_price": "21",
									"service_short_name": "云空间月卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "6",
									"goods_id": 2220600015,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 银翼",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400054",
									"service_name": "小米云服务空间月卡 50G",
									"service_price": "6",
									"service_short_name": "云空间月卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								}
							],
							"service_type_name": "service_micloud",
							"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
							"type_name": "云空间服务"
						}
					]
				},
				"multi_service_refound_policy": {
					"first": {
						"desc": "支持7天无理由退货",
						"is_support": true
					}
				},
				"multi_stock_channel": {
					"first": "3"
				},
				"name": "Redmi K50 电竞版 12GB+256GB 银翼",
				"next_action_button": null,
				"next_onsale_action_button": null,
				"onsale_action_button": null,
				"page_id": 23164,
				"price": "3299",
				"product_desc": "",
				"product_desc_ext": "",
				"product_gallery_icon": {},
				"product_id": 15615,
				"prop_list": [
					{
						"prop_cfg_id": 8,
						"prop_value_id": 884
					},
					{
						"prop_cfg_id": 2,
						"prop_value_id": 10
					}
				],
				"reduce_price": "直降600元",
				"seckill_action_button": null,
				"sell_point_desc": null,
				"service_refound_policy_list": {
					"list": [
						{
							"goods_id": 2220600015,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/a32fabc4c2cecf39e88b9b018fb8d6ec.png",
							"is_support": true,
							"item_content": "7日内，无人为损坏，包装配件齐全，可以无理由退货。",
							"item_id": 1,
							"item_name": "7天无理由退货",
							"sku": 37741
						},
						{
							"goods_id": 2220600015,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/1769a228b31fb6b994a77c4c4ca16448.png",
							"is_support": true,
							"item_content": "15日内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费更换。",
							"item_id": 2,
							"item_name": "15天质量问题换货",
							"sku": 37741
						},
						{
							"goods_id": 2220600015,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/de2196c347411bab09e7e46e0df5c547.png",
							"is_support": true,
							"item_content": "1年内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费维修。",
							"item_id": 3,
							"item_name": "365天保修",
							"sku": 37741
						}
					],
					"list_new": [
						{
							"is_check": true,
							"is_page_show": true,
							"title": "小米自营"
						},
						{
							"desc": "由小米发货",
							"is_check": true,
							"is_page_show": true,
							"title": "小米发货"
						},
						{
							"is_check": true,
							"is_page_show": true,
							"title": "7天无理由退货"
						},
						{
							"desc": "由小米发货商品(不含有品),单笔满69元免运费;\n由小米有品发货的商品,免运费;\n由第三方商家发货的商品，运费以实际结算金额为准;\n特殊商品需要单独收取运费,具体以实际结算金额为准;优惠券等不能抵扣运费金额;如需无理由退货,用户将承担该商品的退货物流费用;\n使用门店闪送服务，需单独支付10元运费。",
							"is_check": true,
							"is_page_show": false,
							"title": "运费说明"
						},
						{
							"desc": "企业名称：小米通讯技术有限公司\n企业执照注册号：91110108558521630L\n企业地址：北京市海淀区西二旗中路33号院6 号楼9层019号\n企业电话：400-100-5678\n营业期限：2010年08月25日 至 2040年08月24日\n经营范围：开发手机技术、计算机软件及信息技术；技术检测、技术咨询、技术服务、技术转让；计算机技术培训；系统集成；货物进出口、技术进出口、代理进出口；家用电器、通信设备、广播电视设备（不含卫星电视广播、地面接收装置）、机械设备、电子产品、文化用品的批发零售；维修仪器仪表；销售医疗器械I类、II、III类、针纺织品（含家纺家饰）、服装鞋帽、日用杂货、工艺品、文化用品、体育用品、照相器材、卫生用品（含个人护理用品）、钟表眼镜、箱包、家具（不从事实体店铺经营）、小饰品、日用品、乐器、自行车、智能卡；计算机、通讯设备、家用电器、电子产品、机械设备的技术开发、技术服务；销售金银饰品（不含金银质地纪念币）；家用空调的委托生产；委托生产翻译机；销售翻译机、五金交电（不含电动自行车）、厨房用品、陶瓷制品、玻璃制品、玩具、汽车零配件、食用农产品、花卉、苗木、宠物用品、建筑材料、装饰材料、化妆品、珠宝首饰、通讯设备、卫生间用品、农药；生产手机（仅限在海淀区永捷北路2号三层生产及外埠生产）；出版物批发；出版物零售；销售食品。（销售第三类医疗器械以及销售食品以及依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动。）",
							"is_check": true,
							"is_page_show": false,
							"title": "企业信息"
						},
						{
							"is_check": true,
							"is_page_show": false,
							"title": "7天价格保护",
							"url": "https://s1.mi.com/m/faq/giving/index.html?project_id=44"
						}
					],
					"url": ""
				},
				"sku": 37741
			},
			{
				"action_button": {
					"bigtap_source": "bigtap",
					"button_title": "等待到货",
					"is_bigtap": false,
					"mode_change_time": 0,
					"mode_end_time": 0,
					"mode_start_time": 1668051600,
					"occ_timeout": 5000,
					"sale_mode": "webview",
					"sale_mode_line": "first",
					"sale_mode_title": "立即购买",
					"source": "common",
					"use_occ": false,
					"webview": {
						"is_hide_stock": true,
						"url": ""
					}
				},
				"address_type": "common",
				"class_parameters": {
					"list": [
						{
							"bottom_title": "骁龙8 Gen 1",
							"icon": "https://cdn.cnbj0.fds.api.mi-img.com/b2c-mimall-media/f0c04e138bfed2b1ebb589de615236d1.png",
							"is_page_show": true,
							"name": "CPU型号",
							"top_title": "CPU",
							"value": "骁龙8 Gen 1"
						},
						{
							"name": "CPU主频",
							"value": "最高3.0GHz"
						},
						{
							"bottom_title": "6400万像素+800万像素+200万像素",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/7692726e7a1dd34a3b1b4ede8aca020d.png",
							"is_page_show": true,
							"name": "后置摄像头",
							"top_title": "三摄像头",
							"value": "6400万像素+800万像素+200万像素"
						},
						{
							"name": "前置摄像头",
							"value": "2000万像素"
						},
						{
							"name": "屏幕",
							"value": "OLED"
						},
						{
							"bottom_title": "6.67英寸",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/86a3bd46cf4f7f19daa2c3250cf30604.png",
							"is_page_show": true,
							"name": "屏幕尺寸",
							"top_title": "超大屏",
							"value": "6.67英寸"
						},
						{
							"bottom_title": "2400×1080",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/a5ab24dcb527e49f970f13b11e000ab1.png",
							"is_page_show": true,
							"name": "屏幕分辨率",
							"top_title": "屏幕分辨率",
							"value": "2400×1080"
						},
						{
							"bottom_title": "最高12GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/c8ec0829241324e401744da627482560.png",
							"is_page_show": true,
							"name": "运行内存",
							"top_title": "极速畅玩",
							"value": "最高12GB"
						},
						{
							"bottom_title": "最高256GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/8941adac25333e785b9cc78ca11f4f27.png",
							"is_page_show": true,
							"name": "存储容量",
							"top_title": "存储容量",
							"value": "最高256GB"
						},
						{
							"name": "NFC",
							"value": "支持"
						},
						{
							"name": "红外遥控",
							"value": "支持"
						},
						{
							"name": "独立AI键",
							"value": "磁动力弹出式肩键 2.0"
						},
						{
							"name": "指纹识别",
							"value": "侧边指纹"
						},
						{
							"bottom_title": "8.5mm",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/52ad10a73685342e437e44ea3d29cbff.png",
							"is_page_show": true,
							"name": "机身厚度",
							"top_title": "普通厚度",
							"value": "8.5mm"
						},
						{
							"bottom_title": "4700mAh",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/0b4ea0fb21dde2f29df3c20de73539b9.png",
							"is_page_show": true,
							"name": "电池容量",
							"top_title": "超长待机",
							"value": "4700mAh"
						},
						{
							"name": "有线快充",
							"value": "120W"
						},
						{
							"bottom_title": "5G全网通7.0",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/d1b67a407fb2a1ed42c2c0ce15af3318.png",
							"is_page_show": true,
							"name": "网络类型",
							"top_title": "运营商网络",
							"value": "5G全网通7.0"
						},
						{
							"bottom_title": "双卡双待",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/bfd5ba9ae72c365dee42db14dfae4b0f.png",
							"is_page_show": true,
							"name": "网络模式",
							"top_title": "网络模式",
							"value": "双卡双待"
						},
						{
							"name": "数据接口",
							"value": "Type-C"
						}
					],
					"name": "关键参数"
				},
				"commodity_id": 1220600015,
				"customize_template_id": 0,
				"discount_price": "",
				"ext_buy_option": [],
				"gallery_v3": [
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/7ba11fdfecac708e7dcc3b98b537a83a.jpeg",
						"img_url_webp": "",
						"type": "video"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/5061b08df9de9ca1c23ba97b58a340c8.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/cdd534269f9326beaa4a493eab827070.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/22bbbda34d4535698318fc9cf40d0334.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bb7e0537300b002657566cd68920f176.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c090f46e0d07ed6db835fe8f96cf41c1.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0542966b26815adcb450e47b7bca70cc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/eab9a225caeae1c6c9f0673915ad295e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/a9ccfcac5415437e23127e1219966a44.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/1d1d0ab8b94208e69ec929f4d95b3c17.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8348f5f8d83643c9fcfc16dd240c4adc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bd48dcfd217a949798f2748a94bb0b41.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/93d64b29aa8aaff8fc9db0e9a665ad9e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3e3d8b8212a6cc53d5db756531e1ba1d.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0ce722c3e15ed76a9654c20fca0c7faf.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/e85d8a06e1b0ea43d3b8fa397aaf5ebc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/68bb35c26ff3f358362652df9b132d2a.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/577d8e3b1ba50a967e423c8562654b57.jpg",
						"img_url_webp": "",
						"type": "img"
					}
				],
				"goods_id": 2220600014,
				"image_share": "http://cdn.cnbj1.fds.api.mi-img.com/mi-mall/063c4055e2befc30c8042dd9cff559f6.png",
				"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/063c4055e2befc30c8042dd9cff559f6.png",
				"is_customize": false,
				"is_sale": true,
				"is_stock": false,
				"market_price": "3899",
				"multi_buy_limit": {
					"first": 10
				},
				"multi_instalment": {
					"first": {
						"default_instal": "antinstal_m",
						"list": [
							{
								"Bank": "mifinanceinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 86.56,
										"is_differential_rate": false,
										"rate": 2.22,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1328.52,
										"stage_interest": 28.85,
										"tab_detail": "手续费28.85元/期起，费率2.22%起，单利年化利率约为13.32%",
										"tab_title": "1328.52元x3期",
										"total_cost": 3985.56
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 152.06,
										"is_differential_rate": false,
										"rate": 3.9,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 675.17,
										"stage_interest": 25.34,
										"tab_detail": "手续费25.34元/期起，费率3.9%起，单利年化利率约为13.37%",
										"tab_title": "675.17元x6期",
										"total_cost": 4051.06
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 280.73,
										"is_differential_rate": false,
										"rate": 7.2,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 348.31,
										"stage_interest": 23.39,
										"tab_detail": "手续费23.39元/期起，费率7.2%起，单利年化利率约为13.29%",
										"tab_title": "348.31元x12期",
										"total_cost": 4179.73
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 701.82,
										"is_differential_rate": false,
										"rate": 18,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 191.7,
										"stage_interest": 29.24,
										"tab_detail": "手续费29.24元/期起，费率18%起，单利年化利率约为17.28%",
										"tab_title": "191.7元x24期",
										"total_cost": 4600.82
									}
								],
								"Installments": null,
								"Title": "小米分期"
							},
							{
								"Bank": "antinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 89.68,
										"is_differential_rate": true,
										"rate": 2.3,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1329.55,
										"stage_interest": 29.89,
										"tab_detail": "手续费29.89元/期起，费率2.3%起，单利年化利率约为13.70%",
										"tab_title": "1329.55元x3期",
										"total_cost": 3988.68
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 175.46,
										"is_differential_rate": true,
										"rate": 4.5,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 679.07,
										"stage_interest": 29.24,
										"tab_detail": "手续费29.24元/期起，费率4.5%起，单利年化利率约为15.30%",
										"tab_title": "679.07元x6期",
										"total_cost": 4074.46
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 292.42,
										"is_differential_rate": true,
										"rate": 7.5,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 349.27,
										"stage_interest": 24.36,
										"tab_detail": "手续费24.36元/期起，费率7.5%起，单利年化利率约为13.60%",
										"tab_title": "349.27元x12期",
										"total_cost": 4191.42
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 487.38,
										"is_differential_rate": true,
										"rate": 12.5,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 182.75,
										"stage_interest": 20.3,
										"tab_detail": "手续费20.3元/期起，费率12.5%起，单利年化利率约为13.80%",
										"tab_title": "182.75元x24期",
										"total_cost": 4386.38
									}
								],
								"Installments": null,
								"Title": "花呗分期"
							}
						]
					}
				},
				"multi_instalment_info": {
					"first": {
						"detail_info": "低至170.57元 x 24期"
					}
				},
				"multi_insurance": {
					"first": {
						"list": [
							{
								"goods_id": 2220700001,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644809331.80464581.jpg",
								"insurance_type": "micare",
								"market_price": 599,
								"name": "Redmi K50 电竞版 MiCare保障服务",
								"onsale_price": 0,
								"price": 599,
								"short_name": "Redmi K50 电竞版 MiCare保障服务",
								"sku": 41034,
								"type": "2"
							},
							{
								"goods_id": 2220600026,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548371.81671372.jpg",
								"insurance_type": "prolong",
								"market_price": 119,
								"name": "Redmi K50 电竞版 延长保修服务",
								"onsale_price": 59,
								"price": 59,
								"short_name": "Redmi K50 电竞版 延长保修服务",
								"sku": 40964,
								"type": "2"
							},
							{
								"goods_id": 2220600024,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548009.85713550.jpg",
								"insurance_type": "accident",
								"market_price": 249,
								"name": "Redmi K50 电竞版 碎屏保障服务",
								"onsale_price": 124,
								"price": 124,
								"short_name": "Redmi K50 电竞版 碎屏保障服务",
								"sku": 40963,
								"type": "2"
							},
							{
								"goods_id": 2220600025,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548202.10119327.jpg",
								"insurance_type": "accident",
								"market_price": 399,
								"name": "Redmi K50 电竞版 意外保障服务",
								"onsale_price": 0,
								"price": 399,
								"short_name": "Redmi K50 电竞版 意外保障服务",
								"sku": 40962,
								"type": "2"
							}
						]
					}
				},
				"multi_market_price": {
					"first": "3899",
					"second": "3899"
				},
				"multi_price": {
					"first": "3299",
					"second": "3299"
				},
				"multi_service_bargins": {
					"first": [
						{
							"can_fold": true,
							"mutex_type": [
								"service_insurance",
								"service_prolong"
							],
							"service_info": [
								{
									"act_diff": "碎屏延保换新",
									"goods_id": 2220600014,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D41034%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D41034%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "享碎屏、延保、换电池、保值换新4大权益",
									"service_goods_id": "2220700001",
									"service_name": "Redmi K50 电竞版 MiCare保障服务",
									"service_price": "599",
									"service_short_name": "MiCare保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
									"source": "common"
								}
							],
							"service_type_name": "service_micare",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
							"type_name": "尊享服务"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "意外损免费修",
									"goods_id": 2220600014,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40962%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40962%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次意外损坏 官方原厂 免费维修",
									"service_goods_id": "2220600025",
									"service_name": "Redmi K50 电竞版 意外保障服务",
									"service_price": "399",
									"service_short_name": "意外保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
									"source": "common"
								},
								{
									"act_diff": "已省125元",
									"act_price": "124",
									"goods_id": 2220600014,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40963%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40963%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次碎屏 官方原厂 免费维修",
									"service_goods_id": "2220600024",
									"service_name": "Redmi K50 电竞版 碎屏保障服务",
									"service_price": "124",
									"service_short_name": "碎屏保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40342",
									"source": "common"
								}
							],
							"service_type_name": "service_insurance",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
							"type_name": "意外保护"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "已省60元",
									"act_price": "59",
									"goods_id": 2220600014,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40964%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40964%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "性能故障 官方原厂 多次免费维修",
									"service_goods_id": "2220600026",
									"service_name": "Redmi K50 电竞版 延长保修服务",
									"service_price": "59",
									"service_short_name": "延长保修服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
									"source": "common"
								}
							],
							"service_type_name": "service_prolong",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
							"type_name": "延长保修"
						},
						{
							"can_fold": true,
							"service_info": [
								{
									"act_price": "208",
									"goods_id": 2220600014,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400049",
									"service_name": "云空间年卡 200G",
									"service_price": "208",
									"service_short_name": "云空间年卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "58",
									"goods_id": 2220600014,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400048",
									"service_name": "云空间年卡50G",
									"service_price": "58",
									"service_short_name": "云空间年卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "21",
									"goods_id": 2220600014,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2182300057",
									"service_name": "小米云服务空间月卡 200G",
									"service_price": "21",
									"service_short_name": "云空间月卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "6",
									"goods_id": 2220600014,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 暗影",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400054",
									"service_name": "小米云服务空间月卡 50G",
									"service_price": "6",
									"service_short_name": "云空间月卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								}
							],
							"service_type_name": "service_micloud",
							"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
							"type_name": "云空间服务"
						}
					]
				},
				"multi_service_refound_policy": {
					"first": {
						"desc": "支持7天无理由退货",
						"is_support": true
					}
				},
				"multi_stock_channel": {
					"first": "3"
				},
				"name": "Redmi K50 电竞版 12GB+256GB 暗影",
				"next_action_button": null,
				"next_onsale_action_button": null,
				"onsale_action_button": null,
				"page_id": 23164,
				"price": "3299",
				"product_desc": "",
				"product_desc_ext": "",
				"product_gallery_icon": {},
				"product_id": 15615,
				"prop_list": [
					{
						"prop_cfg_id": 8,
						"prop_value_id": 884
					},
					{
						"prop_cfg_id": 2,
						"prop_value_id": 3
					}
				],
				"reduce_price": "直降600元",
				"seckill_action_button": null,
				"sell_point_desc": null,
				"service_refound_policy_list": {
					"list": [
						{
							"goods_id": 2220600014,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/a32fabc4c2cecf39e88b9b018fb8d6ec.png",
							"is_support": true,
							"item_content": "7日内，无人为损坏，包装配件齐全，可以无理由退货。",
							"item_id": 1,
							"item_name": "7天无理由退货",
							"sku": 37737
						},
						{
							"goods_id": 2220600014,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/1769a228b31fb6b994a77c4c4ca16448.png",
							"is_support": true,
							"item_content": "15日内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费更换。",
							"item_id": 2,
							"item_name": "15天质量问题换货",
							"sku": 37737
						},
						{
							"goods_id": 2220600014,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/de2196c347411bab09e7e46e0df5c547.png",
							"is_support": true,
							"item_content": "1年内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费维修。",
							"item_id": 3,
							"item_name": "365天保修",
							"sku": 37737
						}
					],
					"list_new": [
						{
							"is_check": true,
							"is_page_show": true,
							"title": "小米自营"
						},
						{
							"desc": "由小米发货",
							"is_check": true,
							"is_page_show": true,
							"title": "小米发货"
						},
						{
							"is_check": true,
							"is_page_show": true,
							"title": "7天无理由退货"
						},
						{
							"desc": "由小米发货商品(不含有品),单笔满69元免运费;\n由小米有品发货的商品,免运费;\n由第三方商家发货的商品，运费以实际结算金额为准;\n特殊商品需要单独收取运费,具体以实际结算金额为准;优惠券等不能抵扣运费金额;如需无理由退货,用户将承担该商品的退货物流费用;\n使用门店闪送服务，需单独支付10元运费。",
							"is_check": true,
							"is_page_show": false,
							"title": "运费说明"
						},
						{
							"desc": "企业名称：小米通讯技术有限公司\n企业执照注册号：91110108558521630L\n企业地址：北京市海淀区西二旗中路33号院6 号楼9层019号\n企业电话：400-100-5678\n营业期限：2010年08月25日 至 2040年08月24日\n经营范围：开发手机技术、计算机软件及信息技术；技术检测、技术咨询、技术服务、技术转让；计算机技术培训；系统集成；货物进出口、技术进出口、代理进出口；家用电器、通信设备、广播电视设备（不含卫星电视广播、地面接收装置）、机械设备、电子产品、文化用品的批发零售；维修仪器仪表；销售医疗器械I类、II、III类、针纺织品（含家纺家饰）、服装鞋帽、日用杂货、工艺品、文化用品、体育用品、照相器材、卫生用品（含个人护理用品）、钟表眼镜、箱包、家具（不从事实体店铺经营）、小饰品、日用品、乐器、自行车、智能卡；计算机、通讯设备、家用电器、电子产品、机械设备的技术开发、技术服务；销售金银饰品（不含金银质地纪念币）；家用空调的委托生产；委托生产翻译机；销售翻译机、五金交电（不含电动自行车）、厨房用品、陶瓷制品、玻璃制品、玩具、汽车零配件、食用农产品、花卉、苗木、宠物用品、建筑材料、装饰材料、化妆品、珠宝首饰、通讯设备、卫生间用品、农药；生产手机（仅限在海淀区永捷北路2号三层生产及外埠生产）；出版物批发；出版物零售；销售食品。（销售第三类医疗器械以及销售食品以及依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动。）",
							"is_check": true,
							"is_page_show": false,
							"title": "企业信息"
						},
						{
							"is_check": true,
							"is_page_show": false,
							"title": "7天价格保护",
							"url": "https://s1.mi.com/m/faq/giving/index.html?project_id=44"
						}
					],
					"url": ""
				},
				"sku": 37737
			},
			{
				"action_button": {
					"bigtap_source": "bigtap",
					"button_title": "等待到货",
					"is_bigtap": false,
					"mode_change_time": 0,
					"mode_end_time": 0,
					"mode_start_time": 1668051600,
					"occ_timeout": 5000,
					"sale_mode": "webview",
					"sale_mode_line": "first",
					"sale_mode_title": "立即购买",
					"source": "common",
					"use_occ": false,
					"webview": {
						"is_hide_stock": true,
						"url": ""
					}
				},
				"address_type": "common",
				"class_parameters": {
					"list": [
						{
							"bottom_title": "骁龙8 Gen 1",
							"icon": "https://cdn.cnbj0.fds.api.mi-img.com/b2c-mimall-media/f0c04e138bfed2b1ebb589de615236d1.png",
							"is_page_show": true,
							"name": "CPU型号",
							"top_title": "CPU",
							"value": "骁龙8 Gen 1"
						},
						{
							"name": "CPU主频",
							"value": "最高3.0GHz"
						},
						{
							"bottom_title": "6400万像素+800万像素+200万像素",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/7692726e7a1dd34a3b1b4ede8aca020d.png",
							"is_page_show": true,
							"name": "后置摄像头",
							"top_title": "三摄像头",
							"value": "6400万像素+800万像素+200万像素"
						},
						{
							"name": "前置摄像头",
							"value": "2000万像素"
						},
						{
							"name": "屏幕",
							"value": "OLED"
						},
						{
							"bottom_title": "6.67英寸",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/86a3bd46cf4f7f19daa2c3250cf30604.png",
							"is_page_show": true,
							"name": "屏幕尺寸",
							"top_title": "超大屏",
							"value": "6.67英寸"
						},
						{
							"bottom_title": "2400×1080",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/a5ab24dcb527e49f970f13b11e000ab1.png",
							"is_page_show": true,
							"name": "屏幕分辨率",
							"top_title": "屏幕分辨率",
							"value": "2400×1080"
						},
						{
							"bottom_title": "最高12GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/c8ec0829241324e401744da627482560.png",
							"is_page_show": true,
							"name": "运行内存",
							"top_title": "极速畅玩",
							"value": "最高12GB"
						},
						{
							"bottom_title": "最高256GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/8941adac25333e785b9cc78ca11f4f27.png",
							"is_page_show": true,
							"name": "存储容量",
							"top_title": "存储容量",
							"value": "最高256GB"
						},
						{
							"name": "NFC",
							"value": "支持"
						},
						{
							"name": "红外遥控",
							"value": "支持"
						},
						{
							"name": "独立AI键",
							"value": "磁动力弹出式肩键 2.0"
						},
						{
							"name": "指纹识别",
							"value": "侧边指纹"
						},
						{
							"bottom_title": "8.5mm",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/52ad10a73685342e437e44ea3d29cbff.png",
							"is_page_show": true,
							"name": "机身厚度",
							"top_title": "普通厚度",
							"value": "8.5mm"
						},
						{
							"bottom_title": "4700mAh",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/0b4ea0fb21dde2f29df3c20de73539b9.png",
							"is_page_show": true,
							"name": "电池容量",
							"top_title": "超长待机",
							"value": "4700mAh"
						},
						{
							"name": "有线快充",
							"value": "120W"
						},
						{
							"bottom_title": "5G全网通7.0",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/d1b67a407fb2a1ed42c2c0ce15af3318.png",
							"is_page_show": true,
							"name": "网络类型",
							"top_title": "运营商网络",
							"value": "5G全网通7.0"
						},
						{
							"bottom_title": "双卡双待",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/bfd5ba9ae72c365dee42db14dfae4b0f.png",
							"is_page_show": true,
							"name": "网络模式",
							"top_title": "网络模式",
							"value": "双卡双待"
						},
						{
							"name": "数据接口",
							"value": "Type-C"
						}
					],
					"name": "关键参数"
				},
				"commodity_id": 1220600015,
				"customize_template_id": 0,
				"discount_price": "",
				"ext_buy_option": [],
				"gallery_v3": [
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/7ba11fdfecac708e7dcc3b98b537a83a.jpeg",
						"img_url_webp": "",
						"type": "video"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/5061b08df9de9ca1c23ba97b58a340c8.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/cdd534269f9326beaa4a493eab827070.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/22bbbda34d4535698318fc9cf40d0334.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bb7e0537300b002657566cd68920f176.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c090f46e0d07ed6db835fe8f96cf41c1.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0542966b26815adcb450e47b7bca70cc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/eab9a225caeae1c6c9f0673915ad295e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/a9ccfcac5415437e23127e1219966a44.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/1d1d0ab8b94208e69ec929f4d95b3c17.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8348f5f8d83643c9fcfc16dd240c4adc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bd48dcfd217a949798f2748a94bb0b41.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/93d64b29aa8aaff8fc9db0e9a665ad9e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3e3d8b8212a6cc53d5db756531e1ba1d.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0ce722c3e15ed76a9654c20fca0c7faf.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/e85d8a06e1b0ea43d3b8fa397aaf5ebc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/68bb35c26ff3f358362652df9b132d2a.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/577d8e3b1ba50a967e423c8562654b57.jpg",
						"img_url_webp": "",
						"type": "img"
					}
				],
				"goods_id": 2220600013,
				"image_share": "http://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f5d5806d6807671fd2a28d9a944b5358.png",
				"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/f5d5806d6807671fd2a28d9a944b5358.png",
				"is_customize": false,
				"is_sale": true,
				"is_stock": false,
				"market_price": "3899",
				"multi_buy_limit": {
					"first": 10
				},
				"multi_instalment": {
					"first": {
						"default_instal": "antinstal_m",
						"list": [
							{
								"Bank": "mifinanceinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 86.56,
										"is_differential_rate": false,
										"rate": 2.22,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1328.52,
										"stage_interest": 28.85,
										"tab_detail": "手续费28.85元/期起，费率2.22%起，单利年化利率约为13.32%",
										"tab_title": "1328.52元x3期",
										"total_cost": 3985.56
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 152.06,
										"is_differential_rate": false,
										"rate": 3.9,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 675.17,
										"stage_interest": 25.34,
										"tab_detail": "手续费25.34元/期起，费率3.9%起，单利年化利率约为13.37%",
										"tab_title": "675.17元x6期",
										"total_cost": 4051.06
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 280.73,
										"is_differential_rate": false,
										"rate": 7.2,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 348.31,
										"stage_interest": 23.39,
										"tab_detail": "手续费23.39元/期起，费率7.2%起，单利年化利率约为13.29%",
										"tab_title": "348.31元x12期",
										"total_cost": 4179.73
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 701.82,
										"is_differential_rate": false,
										"rate": 18,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 191.7,
										"stage_interest": 29.24,
										"tab_detail": "手续费29.24元/期起，费率18%起，单利年化利率约为17.28%",
										"tab_title": "191.7元x24期",
										"total_cost": 4600.82
									}
								],
								"Installments": null,
								"Title": "小米分期"
							},
							{
								"Bank": "antinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 89.68,
										"is_differential_rate": true,
										"rate": 2.3,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1329.55,
										"stage_interest": 29.89,
										"tab_detail": "手续费29.89元/期起，费率2.3%起，单利年化利率约为13.70%",
										"tab_title": "1329.55元x3期",
										"total_cost": 3988.68
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 175.46,
										"is_differential_rate": true,
										"rate": 4.5,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 679.07,
										"stage_interest": 29.24,
										"tab_detail": "手续费29.24元/期起，费率4.5%起，单利年化利率约为15.30%",
										"tab_title": "679.07元x6期",
										"total_cost": 4074.46
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 292.42,
										"is_differential_rate": true,
										"rate": 7.5,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 349.27,
										"stage_interest": 24.36,
										"tab_detail": "手续费24.36元/期起，费率7.5%起，单利年化利率约为13.60%",
										"tab_title": "349.27元x12期",
										"total_cost": 4191.42
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 487.38,
										"is_differential_rate": true,
										"rate": 12.5,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 182.75,
										"stage_interest": 20.3,
										"tab_detail": "手续费20.3元/期起，费率12.5%起，单利年化利率约为13.80%",
										"tab_title": "182.75元x24期",
										"total_cost": 4386.38
									}
								],
								"Installments": null,
								"Title": "花呗分期"
							}
						]
					}
				},
				"multi_instalment_info": {
					"first": {
						"detail_info": "低至170.57元 x 24期"
					}
				},
				"multi_insurance": {
					"first": {
						"list": [
							{
								"goods_id": 2220700001,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644809331.80464581.jpg",
								"insurance_type": "micare",
								"market_price": 599,
								"name": "Redmi K50 电竞版 MiCare保障服务",
								"onsale_price": 0,
								"price": 599,
								"short_name": "Redmi K50 电竞版 MiCare保障服务",
								"sku": 41034,
								"type": "2"
							},
							{
								"goods_id": 2220600026,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548371.81671372.jpg",
								"insurance_type": "prolong",
								"market_price": 119,
								"name": "Redmi K50 电竞版 延长保修服务",
								"onsale_price": 59,
								"price": 59,
								"short_name": "Redmi K50 电竞版 延长保修服务",
								"sku": 40964,
								"type": "2"
							},
							{
								"goods_id": 2220600024,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548009.85713550.jpg",
								"insurance_type": "accident",
								"market_price": 249,
								"name": "Redmi K50 电竞版 碎屏保障服务",
								"onsale_price": 124,
								"price": 124,
								"short_name": "Redmi K50 电竞版 碎屏保障服务",
								"sku": 40963,
								"type": "2"
							},
							{
								"goods_id": 2220600025,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548202.10119327.jpg",
								"insurance_type": "accident",
								"market_price": 399,
								"name": "Redmi K50 电竞版 意外保障服务",
								"onsale_price": 0,
								"price": 399,
								"short_name": "Redmi K50 电竞版 意外保障服务",
								"sku": 40962,
								"type": "2"
							}
						]
					}
				},
				"multi_market_price": {
					"first": "3899",
					"second": "3899"
				},
				"multi_price": {
					"first": "3299",
					"second": "3299"
				},
				"multi_service_bargins": {
					"first": [
						{
							"can_fold": true,
							"mutex_type": [
								"service_insurance",
								"service_prolong"
							],
							"service_info": [
								{
									"act_diff": "碎屏延保换新",
									"goods_id": 2220600013,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D41034%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D41034%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "享碎屏、延保、换电池、保值换新4大权益",
									"service_goods_id": "2220700001",
									"service_name": "Redmi K50 电竞版 MiCare保障服务",
									"service_price": "599",
									"service_short_name": "MiCare保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
									"source": "common"
								}
							],
							"service_type_name": "service_micare",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
							"type_name": "尊享服务"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "意外损免费修",
									"goods_id": 2220600013,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40962%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40962%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次意外损坏 官方原厂 免费维修",
									"service_goods_id": "2220600025",
									"service_name": "Redmi K50 电竞版 意外保障服务",
									"service_price": "399",
									"service_short_name": "意外保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
									"source": "common"
								},
								{
									"act_diff": "已省125元",
									"act_price": "124",
									"goods_id": 2220600013,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40963%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40963%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次碎屏 官方原厂 免费维修",
									"service_goods_id": "2220600024",
									"service_name": "Redmi K50 电竞版 碎屏保障服务",
									"service_price": "124",
									"service_short_name": "碎屏保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40342",
									"source": "common"
								}
							],
							"service_type_name": "service_insurance",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
							"type_name": "意外保护"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "已省60元",
									"act_price": "59",
									"goods_id": 2220600013,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40964%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40964%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "性能故障 官方原厂 多次免费维修",
									"service_goods_id": "2220600026",
									"service_name": "Redmi K50 电竞版 延长保修服务",
									"service_price": "59",
									"service_short_name": "延长保修服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
									"source": "common"
								}
							],
							"service_type_name": "service_prolong",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
							"type_name": "延长保修"
						},
						{
							"can_fold": true,
							"service_info": [
								{
									"act_price": "208",
									"goods_id": 2220600013,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400049",
									"service_name": "云空间年卡 200G",
									"service_price": "208",
									"service_short_name": "云空间年卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "58",
									"goods_id": 2220600013,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400048",
									"service_name": "云空间年卡50G",
									"service_price": "58",
									"service_short_name": "云空间年卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "21",
									"goods_id": 2220600013,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2182300057",
									"service_name": "小米云服务空间月卡 200G",
									"service_price": "21",
									"service_short_name": "云空间月卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "6",
									"goods_id": 2220600013,
									"goods_name": "Redmi K50 电竞版 12GB+256GB 冰斩",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400054",
									"service_name": "小米云服务空间月卡 50G",
									"service_price": "6",
									"service_short_name": "云空间月卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								}
							],
							"service_type_name": "service_micloud",
							"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
							"type_name": "云空间服务"
						}
					]
				},
				"multi_service_refound_policy": {
					"first": {
						"desc": "支持7天无理由退货",
						"is_support": true
					}
				},
				"multi_stock_channel": {
					"first": "3"
				},
				"name": "Redmi K50 电竞版 12GB+256GB 冰斩",
				"next_action_button": null,
				"next_onsale_action_button": null,
				"onsale_action_button": null,
				"page_id": 23164,
				"price": "3299",
				"product_desc": "",
				"product_desc_ext": "",
				"product_gallery_icon": {},
				"product_id": 15615,
				"prop_list": [
					{
						"prop_cfg_id": 8,
						"prop_value_id": 884
					},
					{
						"prop_cfg_id": 2,
						"prop_value_id": 4
					}
				],
				"reduce_price": "直降600元",
				"seckill_action_button": null,
				"sell_point_desc": null,
				"service_refound_policy_list": {
					"list": [
						{
							"goods_id": 2220600013,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/a32fabc4c2cecf39e88b9b018fb8d6ec.png",
							"is_support": true,
							"item_content": "7日内，无人为损坏，包装配件齐全，可以无理由退货。",
							"item_id": 1,
							"item_name": "7天无理由退货",
							"sku": 37739
						},
						{
							"goods_id": 2220600013,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/1769a228b31fb6b994a77c4c4ca16448.png",
							"is_support": true,
							"item_content": "15日内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费更换。",
							"item_id": 2,
							"item_name": "15天质量问题换货",
							"sku": 37739
						},
						{
							"goods_id": 2220600013,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/de2196c347411bab09e7e46e0df5c547.png",
							"is_support": true,
							"item_content": "1年内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费维修。",
							"item_id": 3,
							"item_name": "365天保修",
							"sku": 37739
						}
					],
					"list_new": [
						{
							"is_check": true,
							"is_page_show": true,
							"title": "小米自营"
						},
						{
							"desc": "由小米发货",
							"is_check": true,
							"is_page_show": true,
							"title": "小米发货"
						},
						{
							"is_check": true,
							"is_page_show": true,
							"title": "7天无理由退货"
						},
						{
							"desc": "由小米发货商品(不含有品),单笔满69元免运费;\n由小米有品发货的商品,免运费;\n由第三方商家发货的商品，运费以实际结算金额为准;\n特殊商品需要单独收取运费,具体以实际结算金额为准;优惠券等不能抵扣运费金额;如需无理由退货,用户将承担该商品的退货物流费用;\n使用门店闪送服务，需单独支付10元运费。",
							"is_check": true,
							"is_page_show": false,
							"title": "运费说明"
						},
						{
							"desc": "企业名称：小米通讯技术有限公司\n企业执照注册号：91110108558521630L\n企业地址：北京市海淀区西二旗中路33号院6 号楼9层019号\n企业电话：400-100-5678\n营业期限：2010年08月25日 至 2040年08月24日\n经营范围：开发手机技术、计算机软件及信息技术；技术检测、技术咨询、技术服务、技术转让；计算机技术培训；系统集成；货物进出口、技术进出口、代理进出口；家用电器、通信设备、广播电视设备（不含卫星电视广播、地面接收装置）、机械设备、电子产品、文化用品的批发零售；维修仪器仪表；销售医疗器械I类、II、III类、针纺织品（含家纺家饰）、服装鞋帽、日用杂货、工艺品、文化用品、体育用品、照相器材、卫生用品（含个人护理用品）、钟表眼镜、箱包、家具（不从事实体店铺经营）、小饰品、日用品、乐器、自行车、智能卡；计算机、通讯设备、家用电器、电子产品、机械设备的技术开发、技术服务；销售金银饰品（不含金银质地纪念币）；家用空调的委托生产；委托生产翻译机；销售翻译机、五金交电（不含电动自行车）、厨房用品、陶瓷制品、玻璃制品、玩具、汽车零配件、食用农产品、花卉、苗木、宠物用品、建筑材料、装饰材料、化妆品、珠宝首饰、通讯设备、卫生间用品、农药；生产手机（仅限在海淀区永捷北路2号三层生产及外埠生产）；出版物批发；出版物零售；销售食品。（销售第三类医疗器械以及销售食品以及依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动。）",
							"is_check": true,
							"is_page_show": false,
							"title": "企业信息"
						},
						{
							"is_check": true,
							"is_page_show": false,
							"title": "7天价格保护",
							"url": "https://s1.mi.com/m/faq/giving/index.html?project_id=44"
						}
					],
					"url": ""
				},
				"sku": 37739
			},
			{
				"action_button": {
					"bigtap_source": "bigtap",
					"button_title": "加入购物车",
					"is_bigtap": false,
					"mode_change_time": 0,
					"mode_end_time": 0,
					"mode_start_time": 1661159400,
					"occ_timeout": 5000,
					"sale_mode": "standard",
					"sale_mode_line": "first",
					"sale_mode_title": "立即购买",
					"source": "common",
					"standard": [],
					"use_occ": false
				},
				"address_type": "common",
				"class_parameters": {
					"list": [
						{
							"bottom_title": "骁龙8 Gen 1",
							"icon": "https://cdn.cnbj0.fds.api.mi-img.com/b2c-mimall-media/f0c04e138bfed2b1ebb589de615236d1.png",
							"is_page_show": true,
							"name": "CPU型号",
							"top_title": "CPU",
							"value": "骁龙8 Gen 1"
						},
						{
							"name": "CPU主频",
							"value": "最高3.0GHz"
						},
						{
							"bottom_title": "6400万像素+800万像素+200万像素",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/7692726e7a1dd34a3b1b4ede8aca020d.png",
							"is_page_show": true,
							"name": "后置摄像头",
							"top_title": "三摄像头",
							"value": "6400万像素+800万像素+200万像素"
						},
						{
							"name": "前置摄像头",
							"value": "2000万像素"
						},
						{
							"name": "屏幕",
							"value": "OLED"
						},
						{
							"bottom_title": "6.67英寸",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/86a3bd46cf4f7f19daa2c3250cf30604.png",
							"is_page_show": true,
							"name": "屏幕尺寸",
							"top_title": "超大屏",
							"value": "6.67英寸"
						},
						{
							"bottom_title": "2400×1080",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/a5ab24dcb527e49f970f13b11e000ab1.png",
							"is_page_show": true,
							"name": "屏幕分辨率",
							"top_title": "屏幕分辨率",
							"value": "2400×1080"
						},
						{
							"bottom_title": "最高12GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/c8ec0829241324e401744da627482560.png",
							"is_page_show": true,
							"name": "运行内存",
							"top_title": "极速畅玩",
							"value": "最高12GB"
						},
						{
							"bottom_title": "最高256GB",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/8941adac25333e785b9cc78ca11f4f27.png",
							"is_page_show": true,
							"name": "存储容量",
							"top_title": "存储容量",
							"value": "最高256GB"
						},
						{
							"name": "NFC",
							"value": "支持"
						},
						{
							"name": "红外遥控",
							"value": "支持"
						},
						{
							"name": "独立AI键",
							"value": "磁动力弹出式肩键 2.0"
						},
						{
							"name": "指纹识别",
							"value": "侧边指纹"
						},
						{
							"bottom_title": "8.5mm",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/52ad10a73685342e437e44ea3d29cbff.png",
							"is_page_show": true,
							"name": "机身厚度",
							"top_title": "普通厚度",
							"value": "8.5mm"
						},
						{
							"bottom_title": "4700mAh",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/0b4ea0fb21dde2f29df3c20de73539b9.png",
							"is_page_show": true,
							"name": "电池容量",
							"top_title": "超长待机",
							"value": "4700mAh"
						},
						{
							"name": "有线快充",
							"value": "120W"
						},
						{
							"bottom_title": "5G全网通7.0",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/d1b67a407fb2a1ed42c2c0ce15af3318.png",
							"is_page_show": true,
							"name": "网络类型",
							"top_title": "运营商网络",
							"value": "5G全网通7.0"
						},
						{
							"bottom_title": "双卡双待",
							"icon": "https://i8.mifile.cn/b2c-mimall-media/bfd5ba9ae72c365dee42db14dfae4b0f.png",
							"is_page_show": true,
							"name": "网络模式",
							"top_title": "网络模式",
							"value": "双卡双待"
						},
						{
							"name": "数据接口",
							"value": "Type-C"
						}
					],
					"name": "关键参数"
				},
				"commodity_id": 1220600015,
				"customize_template_id": 0,
				"discount_price": "",
				"ext_buy_option": [],
				"gallery_v3": [
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/7ba11fdfecac708e7dcc3b98b537a83a.jpeg",
						"img_url_webp": "",
						"type": "video"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/5061b08df9de9ca1c23ba97b58a340c8.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/cdd534269f9326beaa4a493eab827070.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/22bbbda34d4535698318fc9cf40d0334.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bb7e0537300b002657566cd68920f176.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c090f46e0d07ed6db835fe8f96cf41c1.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0542966b26815adcb450e47b7bca70cc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/eab9a225caeae1c6c9f0673915ad295e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/a9ccfcac5415437e23127e1219966a44.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/1d1d0ab8b94208e69ec929f4d95b3c17.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8348f5f8d83643c9fcfc16dd240c4adc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bd48dcfd217a949798f2748a94bb0b41.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/93d64b29aa8aaff8fc9db0e9a665ad9e.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3e3d8b8212a6cc53d5db756531e1ba1d.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0ce722c3e15ed76a9654c20fca0c7faf.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/e85d8a06e1b0ea43d3b8fa397aaf5ebc.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/68bb35c26ff3f358362652df9b132d2a.jpg",
						"img_url_webp": "",
						"type": "img"
					},
					{
						"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/577d8e3b1ba50a967e423c8562654b57.jpg",
						"img_url_webp": "",
						"type": "img"
					}
				],
				"goods_id": 2220600020,
				"image_share": "http://cdn.cnbj1.fds.api.mi-img.com/mi-mall/3c863e2b22faf7af3b6b922adbe45e4b.png",
				"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3c863e2b22faf7af3b6b922adbe45e4b.png",
				"is_customize": false,
				"is_sale": true,
				"is_stock": true,
				"market_price": "4199",
				"multi_buy_limit": {
					"first": 2
				},
				"multi_instalment": {
					"first": {
						"default_instal": "antinstal_m",
						"list": [
							{
								"Bank": "mifinanceinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 93.22,
										"is_differential_rate": false,
										"rate": 2.22,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1430.74,
										"stage_interest": 31.07,
										"tab_detail": "手续费31.07元/期起，费率2.22%起，单利年化利率约为13.32%",
										"tab_title": "1430.74元x3期",
										"total_cost": 4292.22
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 163.76,
										"is_differential_rate": false,
										"rate": 3.9,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 727.12,
										"stage_interest": 27.29,
										"tab_detail": "手续费27.29元/期起，费率3.9%起，单利年化利率约为13.37%",
										"tab_title": "727.12元x6期",
										"total_cost": 4362.76
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 302.33,
										"is_differential_rate": false,
										"rate": 7.2,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 375.11,
										"stage_interest": 25.19,
										"tab_detail": "手续费25.19元/期起，费率7.2%起，单利年化利率约为13.29%",
										"tab_title": "375.11元x12期",
										"total_cost": 4501.33
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 755.82,
										"is_differential_rate": false,
										"rate": 18,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 206.45,
										"stage_interest": 31.49,
										"tab_detail": "手续费31.49元/期起，费率18%起，单利年化利率约为17.28%",
										"tab_title": "206.45元x24期",
										"total_cost": 4954.82
									}
								],
								"Installments": null,
								"Title": "小米分期"
							},
							{
								"Bank": "antinstal_m",
								"Desc": "",
								"Detail": [
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 96.58,
										"is_differential_rate": true,
										"rate": 2.3,
										"rate_discount": 10,
										"stage": 3,
										"stage_cost": 1431.85,
										"stage_interest": 32.19,
										"tab_detail": "手续费32.19元/期起，费率2.3%起，单利年化利率约为13.70%",
										"tab_title": "1431.85元x3期",
										"total_cost": 4295.58
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 188.96,
										"is_differential_rate": true,
										"rate": 4.5,
										"rate_discount": 10,
										"stage": 6,
										"stage_cost": 731.32,
										"stage_interest": 31.49,
										"tab_detail": "手续费31.49元/期起，费率4.5%起，单利年化利率约为15.30%",
										"tab_title": "731.32元x6期",
										"total_cost": 4387.96
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 314.92,
										"is_differential_rate": true,
										"rate": 7.5,
										"rate_discount": 10,
										"stage": 12,
										"stage_cost": 376.15,
										"stage_interest": 26.24,
										"tab_detail": "手续费26.24元/期起，费率7.5%起，单利年化利率约为13.60%",
										"tab_title": "376.15元x12期",
										"total_cost": 4513.92
									},
									{
										"checked": false,
										"first_stage_interest": 0,
										"interest": 524.88,
										"is_differential_rate": true,
										"rate": 12.5,
										"rate_discount": 10,
										"stage": 24,
										"stage_cost": 196.82,
										"stage_interest": 21.87,
										"tab_detail": "手续费21.87元/期起，费率12.5%起，单利年化利率约为13.80%",
										"tab_title": "196.82元x24期",
										"total_cost": 4723.88
									}
								],
								"Installments": null,
								"Title": "花呗分期"
							}
						]
					}
				},
				"multi_instalment_info": {
					"first": {
						"detail_info": "低至181.16元 x 24期"
					}
				},
				"multi_insurance": {
					"first": {
						"list": [
							{
								"goods_id": 2220700001,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644809331.80464581.jpg",
								"insurance_type": "micare",
								"market_price": 599,
								"name": "Redmi K50 电竞版 MiCare保障服务",
								"onsale_price": 0,
								"price": 599,
								"short_name": "Redmi K50 电竞版 MiCare保障服务",
								"sku": 41034,
								"type": "2"
							},
							{
								"goods_id": 2220600026,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548371.81671372.jpg",
								"insurance_type": "prolong",
								"market_price": 119,
								"name": "Redmi K50 电竞版 延长保修服务",
								"onsale_price": 59,
								"price": 59,
								"short_name": "Redmi K50 电竞版 延长保修服务",
								"sku": 40964,
								"type": "2"
							},
							{
								"goods_id": 2220600024,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548009.85713550.jpg",
								"insurance_type": "accident",
								"market_price": 249,
								"name": "Redmi K50 电竞版 碎屏保障服务",
								"onsale_price": 124,
								"price": 124,
								"short_name": "Redmi K50 电竞版 碎屏保障服务",
								"sku": 40963,
								"type": "2"
							},
							{
								"goods_id": 2220600025,
								"image_url": "//cdn.cnbj0.fds.api.mi-img.com/b2c-shopapi-pms/pms_1644548202.10119327.jpg",
								"insurance_type": "accident",
								"market_price": 399,
								"name": "Redmi K50 电竞版 意外保障服务",
								"onsale_price": 0,
								"price": 399,
								"short_name": "Redmi K50 电竞版 意外保障服务",
								"sku": 40962,
								"type": "2"
							}
						]
					}
				},
				"multi_market_price": {
					"first": "4199",
					"second": "4199"
				},
				"multi_price": {
					"first": "4199",
					"second": "4199"
				},
				"multi_service_bargins": {
					"first": [
						{
							"can_fold": true,
							"mutex_type": [
								"service_insurance",
								"service_prolong"
							],
							"service_info": [
								{
									"act_diff": "碎屏延保换新",
									"goods_id": 2220600020,
									"goods_name": "Redmi K50电竞版 梅赛德斯-AMG F1车队冠军版",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D41034%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D41034%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "享碎屏、延保、换电池、保值换新4大权益",
									"service_goods_id": "2220700001",
									"service_name": "Redmi K50 电竞版 MiCare保障服务",
									"service_price": "599",
									"service_short_name": "MiCare保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
									"source": "common"
								}
							],
							"service_type_name": "service_micare",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40423",
							"type_name": "尊享服务"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "意外损免费修",
									"goods_id": 2220600020,
									"goods_name": "Redmi K50电竞版 梅赛德斯-AMG F1车队冠军版",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40962%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40962%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次意外损坏 官方原厂 免费维修",
									"service_goods_id": "2220600025",
									"service_name": "Redmi K50 电竞版 意外保障服务",
									"service_price": "399",
									"service_short_name": "意外保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
									"source": "common"
								},
								{
									"act_diff": "已省125元",
									"act_price": "124",
									"goods_id": 2220600020,
									"goods_name": "Redmi K50电竞版 梅赛德斯-AMG F1车队冠军版",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40963%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40963%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "1年1次碎屏 官方原厂 免费维修",
									"service_goods_id": "2220600024",
									"service_name": "Redmi K50 电竞版 碎屏保障服务",
									"service_price": "124",
									"service_short_name": "碎屏保障服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40342",
									"source": "common"
								}
							],
							"service_type_name": "service_insurance",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40344",
							"type_name": "意外保护"
						},
						{
							"can_fold": true,
							"mutex_type": [
								"service_micare"
							],
							"service_info": [
								{
									"act_diff": "已省60元",
									"act_price": "59",
									"goods_id": 2220600020,
									"goods_name": "Redmi K50电竞版 梅赛德斯-AMG F1车队冠军版",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_bxtk%26insuranceSku%3D40964%26couponFrom%3Drule"
										},
										{
											"desc": "常见问题",
											"url": "https://order.mi.com/static/jrUrl?url=https%3A%2F%2Fapi.jr.mi.com%2Finsurance%2Fdocument%2Fphone_accidentIns.html%3Ffrom%3Dins_phonedetail_cjwt%26insuranceSku%3D40964%26couponFrom%3Dquestion"
										}
									],
									"service_desc": "性能故障 官方原厂 多次免费维修",
									"service_goods_id": "2220600026",
									"service_name": "Redmi K50 电竞版 延长保修服务",
									"service_price": "59",
									"service_short_name": "延长保修服务",
									"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
									"source": "common"
								}
							],
							"service_type_name": "service_prolong",
							"service_url": "https://api.jr.mi.com/activity/accidentIns/?from=mishop&insuranceSku=40349",
							"type_name": "延长保修"
						},
						{
							"can_fold": true,
							"service_info": [
								{
									"act_price": "208",
									"goods_id": 2220600020,
									"goods_name": "Redmi K50电竞版 梅赛德斯-AMG F1车队冠军版",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400049",
									"service_name": "云空间年卡 200G",
									"service_price": "208",
									"service_short_name": "云空间年卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "58",
									"goods_id": 2220600020,
									"goods_name": "Redmi K50电竞版 梅赛德斯-AMG F1车队冠军版",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400048",
									"service_name": "云空间年卡50G",
									"service_price": "58",
									"service_short_name": "云空间年卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "21",
									"goods_id": 2220600020,
									"goods_name": "Redmi K50电竞版 梅赛德斯-AMG F1车队冠军版",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2182300057",
									"service_name": "小米云服务空间月卡 200G",
									"service_price": "21",
									"service_short_name": "云空间月卡200G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								},
								{
									"act_price": "6",
									"goods_id": 2220600020,
									"goods_name": "Redmi K50电竞版 梅赛德斯-AMG F1车队冠军版",
									"is_default_selected": false,
									"phone_accidentIns": [
										{
											"desc": "服务条款",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572506400847/index.html"
										},
										{
											"desc": "常见问题",
											"url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572507054693/index.html"
										}
									],
									"service_desc": "主商品签收后，自动激活至下单帐号",
									"service_goods_id": "2194400054",
									"service_name": "小米云服务空间月卡 50G",
									"service_price": "6",
									"service_short_name": "云空间月卡 50G",
									"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
									"source": "relatedbuy"
								}
							],
							"service_type_name": "service_micloud",
							"service_url": "https://cnbj1.fds.api.xiaomi.com/mih5/page/d-1572343721754/index.html",
							"type_name": "云空间服务"
						}
					]
				},
				"multi_service_refound_policy": {
					"first": {
						"desc": "支持7天无理由退货",
						"is_support": true
					}
				},
				"multi_stock_channel": {
					"first": "3"
				},
				"name": "Redmi K50电竞版 梅赛德斯-AMG F1车队冠军版",
				"next_action_button": null,
				"next_onsale_action_button": null,
				"onsale_action_button": null,
				"page_id": 23164,
				"price": "4199",
				"product_desc": "",
				"product_desc_ext": "",
				"product_gallery_icon": {},
				"product_id": 15615,
				"prop_list": [
					{
						"prop_cfg_id": 8,
						"prop_value_id": 884
					},
					{
						"prop_cfg_id": 2,
						"prop_value_id": 1
					}
				],
				"reduce_price": "",
				"seckill_action_button": null,
				"sell_point_desc": null,
				"service_refound_policy_list": {
					"list": [
						{
							"goods_id": 2220600020,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/a32fabc4c2cecf39e88b9b018fb8d6ec.png",
							"is_support": true,
							"item_content": "7日内，无人为损坏，包装配件齐全，可以无理由退货。",
							"item_id": 1,
							"item_name": "7天无理由退货",
							"sku": 38974
						},
						{
							"goods_id": 2220600020,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/1769a228b31fb6b994a77c4c4ca16448.png",
							"is_support": true,
							"item_content": "15日内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费更换。",
							"item_id": 2,
							"item_name": "15天质量问题换货",
							"sku": 38974
						},
						{
							"goods_id": 2220600020,
							"img_url": "http://i8.mifile.cn/b2c-mimall-media/de2196c347411bab09e7e46e0df5c547.png",
							"is_support": true,
							"item_content": "1年内，如出现功能性故障或商品质量问题，经小米公司授权服务商检测，确认属于非人为商品质量问题，我们将会为您免费维修。",
							"item_id": 3,
							"item_name": "365天保修",
							"sku": 38974
						}
					],
					"list_new": [
						{
							"is_check": true,
							"is_page_show": true,
							"title": "小米自营"
						},
						{
							"desc": "由小米发货",
							"is_check": true,
							"is_page_show": true,
							"title": "小米发货"
						},
						{
							"is_check": true,
							"is_page_show": true,
							"title": "7天无理由退货"
						},
						{
							"desc": "由小米发货商品(不含有品),单笔满69元免运费;\n由小米有品发货的商品,免运费;\n由第三方商家发货的商品，运费以实际结算金额为准;\n特殊商品需要单独收取运费,具体以实际结算金额为准;优惠券等不能抵扣运费金额;如需无理由退货,用户将承担该商品的退货物流费用;\n使用门店闪送服务，需单独支付10元运费。",
							"is_check": true,
							"is_page_show": false,
							"title": "运费说明"
						},
						{
							"desc": "企业名称：小米通讯技术有限公司\n企业执照注册号：91110108558521630L\n企业地址：北京市海淀区西二旗中路33号院6 号楼9层019号\n企业电话：400-100-5678\n营业期限：2010年08月25日 至 2040年08月24日\n经营范围：开发手机技术、计算机软件及信息技术；技术检测、技术咨询、技术服务、技术转让；计算机技术培训；系统集成；货物进出口、技术进出口、代理进出口；家用电器、通信设备、广播电视设备（不含卫星电视广播、地面接收装置）、机械设备、电子产品、文化用品的批发零售；维修仪器仪表；销售医疗器械I类、II、III类、针纺织品（含家纺家饰）、服装鞋帽、日用杂货、工艺品、文化用品、体育用品、照相器材、卫生用品（含个人护理用品）、钟表眼镜、箱包、家具（不从事实体店铺经营）、小饰品、日用品、乐器、自行车、智能卡；计算机、通讯设备、家用电器、电子产品、机械设备的技术开发、技术服务；销售金银饰品（不含金银质地纪念币）；家用空调的委托生产；委托生产翻译机；销售翻译机、五金交电（不含电动自行车）、厨房用品、陶瓷制品、玻璃制品、玩具、汽车零配件、食用农产品、花卉、苗木、宠物用品、建筑材料、装饰材料、化妆品、珠宝首饰、通讯设备、卫生间用品、农药；生产手机（仅限在海淀区永捷北路2号三层生产及外埠生产）；出版物批发；出版物零售；销售食品。（销售第三类医疗器械以及销售食品以及依法须经批准的项目，经相关部门批准后依批准的内容开展经营活动。）",
							"is_check": true,
							"is_page_show": false,
							"title": "企业信息"
						},
						{
							"is_check": true,
							"is_page_show": false,
							"title": "7天价格保护",
							"url": "https://s1.mi.com/m/faq/giving/index.html?project_id=44"
						}
					],
					"url": ""
				},
				"sku": 38974
			}
		],
		"goods_share_datas": {
			"comments": {
				"detail": {
					"comment_tags": [
						{
							"ext": "",
							"num_desc": "3085",
							"profile_id": 102604,
							"profile_num": 3085,
							"tag_id": 102604,
							"tag_name": "价格实惠"
						},
						{
							"ext": "",
							"num_desc": "2726",
							"profile_id": 102255,
							"profile_num": 2726,
							"tag_id": 102255,
							"tag_name": "运行稳定"
						},
						{
							"ext": "",
							"num_desc": "2175",
							"profile_id": 103078,
							"profile_num": 2175,
							"tag_id": 103078,
							"tag_name": "颜值够高"
						},
						{
							"ext": "",
							"num_desc": "1998",
							"profile_id": 102257,
							"profile_num": 1998,
							"tag_id": 102257,
							"tag_name": "拍照一流"
						},
						{
							"ext": "",
							"num_desc": "1128",
							"profile_id": 102066,
							"profile_num": 1128,
							"tag_id": 102066,
							"tag_name": "系统稳定"
						},
						{
							"ext": "",
							"num_desc": "786",
							"profile_id": 103074,
							"profile_num": 786,
							"tag_id": 103074,
							"tag_name": "充电快速"
						}
					],
					"comments_bad": 547,
					"comments_general": 141,
					"comments_good": 90512,
					"comments_total": 91200,
					"default_good": 78069,
					"five_star": 11928,
					"four_star": 259,
					"one_star": 547,
					"satisfy_per": "好评率99.2%",
					"three_star": 256,
					"trace_kv": "-requestId_9a6b4f5e320143a590a9eaae0dc32de1-pids_15615-tag_ids___all__",
					"two_star": 141
				},
				"list": [
					{
						"add_time": "2022-11-03",
						"average_grade": "5",
						"comment_content": "首先，设计语言很帅气，再来与F1 AMG的联名也是难以抗拒，最关键屏幕，配置，声音表现，最重要是振动真的是无敌，虽说上市半年多了，但依然能抗能打",
						"comment_full_images": [
							"https://i1.mifile.cn/a2/1667488246_5549243_s1536_2287wh!540x5400.jpg",
							"https://i1.mifile.cn/a2/1667488246_3177178_s1080_2400wh!540x5400.jpg",
							"https://i1.mifile.cn/a2/1667488246_5549243_s1536_2287wh!540x5400.jpg",
							"https://i1.mifile.cn/a2/1667488246_3177178_s1080_2400wh!540x5400.jpg"
						],
						"comment_grade": 1,
						"comment_id": "219444643",
						"comment_images": [
							"https://i1.mifile.cn/a2/1667488246_5549243_s1536_2287wh!540x5400.jpg",
							"https://i1.mifile.cn/a2/1667488246_3177178_s1080_2400wh!540x5400.jpg"
						],
						"content_list_type": 0,
						"down": false,
						"goods_info": null,
						"has_up_customer": false,
						"image_infos": [
							{
								"img_url": "https://i1.mifile.cn/a2/1667488246_5549243_s1536_2287wh.jpg",
								"origin_size": 0,
								"origin_url": ""
							},
							{
								"img_url": "https://i1.mifile.cn/a2/1667488246_3177178_s1080_2400wh.jpg",
								"origin_size": 0,
								"origin_url": ""
							}
						],
						"is_anonymous": false,
						"mizone_content_type": "",
						"reply_content": "您的好评就像是一盏明灯，时刻指引着我们向更高的标准前进，我们一定不会辜负您的期望，用最好的产品回报您的喜爱~",
						"reply_time": "2022-11-03",
						"reply_up_num": "0",
						"site_name": "小米官网",
						"total_grade": "5",
						"up": false,
						"up_customer_num": 0,
						"up_num": 5,
						"up_rate": 100,
						"user_avatar": "https://cdn.cnbj1.fds.api.mi-img.com/user-avatar/61df069b-4217-42ae-9b7f-98e68ec7c4d7.jpg",
						"user_id": "180****92",
						"user_name": "Edison Chai",
						"user_reply_num": "1",
						"user_replys": []
					},
					{
						"add_time": "2022-10-21",
						"average_grade": "5",
						"comment_content": "性价比：非常不错<br />运行速度：快<br />拍照效果：还可以<br />电池续航：够用<br />外观设计：时尚<br />MIUI系统：简洁<br />一直在用黑鲨，这个跟黑鲨在设计上也差不多。比黑鲨性价比高一些",
						"comment_full_images": [
							"https://i1.mifile.cn/a2/1666282642_9540844_s1080_2400wh!540x5400.jpg",
							"https://i1.mifile.cn/a2/1666282642_9540844_s1080_2400wh!540x5400.jpg"
						],
						"comment_grade": 1,
						"comment_id": "219214960",
						"comment_images": [
							"https://i1.mifile.cn/a2/1666282642_9540844_s1080_2400wh!540x5400.jpg?w=540&h=1200"
						],
						"content_list_type": 0,
						"down": false,
						"goods_info": null,
						"has_up_customer": false,
						"image_infos": [
							{
								"img_url": "https://i1.mifile.cn/a2/1666282642_9540844_s1080_2400wh.jpg",
								"origin_size": 0,
								"origin_url": ""
							}
						],
						"is_anonymous": false,
						"mizone_content_type": "",
						"reply_content": "您满意的评语，像那冬日暖阳，夏日清风，给予了我们希望与动力,我们一定不会辜负您的期望，用最好的产品回报您的喜爱。",
						"reply_time": "2022-10-21",
						"reply_up_num": "0",
						"site_name": "小米官网",
						"total_grade": "5",
						"up": false,
						"up_customer_num": 0,
						"up_num": 2,
						"up_rate": 100,
						"user_avatar": "https://cdn.cnbj0.fds.api.mi-img.com/b2c-data-mishop/52cc4721e1eaed5af47af693906205c8.jpg",
						"user_id": "249****00",
						"user_name": "djg365",
						"user_reply_num": "1",
						"user_replys": []
					},
					{
						"add_time": "2022-10-27",
						"average_grade": "5",
						"comment_content": "性价比：高<br />运行速度：快<br />拍照效果：可以满足<br />电池续航：充电快<br />外观设计：个性<br />MIUI系统：好",
						"comment_full_images": [
							"https://i1.mifile.cn/a2/1666834484_8872254_s900_2000wh!540x5400.jpg",
							"https://i1.mifile.cn/a2/1666834484_5727877_s900_2000wh!540x5400.jpg",
							"https://i1.mifile.cn/a2/1666834484_8872254_s900_2000wh!540x5400.jpg",
							"https://i1.mifile.cn/a2/1666834484_5727877_s900_2000wh!540x5400.jpg"
						],
						"comment_grade": 1,
						"comment_id": "219293440",
						"comment_images": [
							"https://i1.mifile.cn/a2/1666834484_8872254_s900_2000wh!540x5400.jpg",
							"https://i1.mifile.cn/a2/1666834484_5727877_s900_2000wh!540x5400.jpg"
						],
						"content_list_type": 0,
						"down": false,
						"goods_info": null,
						"has_up_customer": false,
						"image_infos": [
							{
								"img_url": "https://i1.mifile.cn/a2/1666834484_8872254_s900_2000wh.jpg",
								"origin_size": 0,
								"origin_url": ""
							},
							{
								"img_url": "https://i1.mifile.cn/a2/1666834484_5727877_s900_2000wh.jpg",
								"origin_size": 0,
								"origin_url": ""
							}
						],
						"is_anonymous": false,
						"mizone_content_type": "",
						"site_name": "小米官网",
						"tags": "{\"1\":{\"marks\":\"16,18\",\"tags\":\"1\"}}",
						"total_grade": "5",
						"up": false,
						"up_customer_num": 0,
						"up_num": 2,
						"up_rate": 100,
						"user_avatar": "https://cdn.cnbj1.fds.api.mi-img.com/user-avatar/f6b82120-c37e-4e29-a14f-28167f8aec1f.jpg",
						"user_id": "240****096",
						"user_name": "JLL",
						"user_reply_num": "",
						"user_replys": []
					},
					{
						"add_time": "2022-10-26",
						"average_grade": "5",
						"comment_content": "非常好看，很满意下次再买小米产品",
						"comment_full_images": [
							"https://i1.mifile.cn/a2/1666751132_4085419_s2304_1728wh!540x5400.jpg",
							"https://i1.mifile.cn/a2/1666751132_4085419_s2304_1728wh!540x5400.jpg"
						],
						"comment_grade": 1,
						"comment_id": "219282372",
						"comment_images": [
							"https://i1.mifile.cn/a2/1666751132_4085419_s2304_1728wh!540x5400.jpg?w=540&h=405"
						],
						"content_list_type": 0,
						"down": false,
						"goods_info": null,
						"has_up_customer": false,
						"image_infos": [
							{
								"img_url": "https://i1.mifile.cn/a2/1666751132_4085419_s2304_1728wh.jpg",
								"origin_size": 0,
								"origin_url": ""
							}
						],
						"is_anonymous": false,
						"mizone_content_type": "",
						"reply_content": "您的好评就像是一盏明灯，时刻指引着我们向更高的标准前进，我们一定不会辜负您的期望，用最好的产品回报您的喜爱~",
						"reply_time": "2022-10-26",
						"reply_up_num": "0",
						"site_name": "小米官网",
						"total_grade": "5",
						"up": false,
						"up_customer_num": 0,
						"up_num": 2,
						"up_rate": 100,
						"user_avatar": "https://cdn.cnbj1.fds.api.mi-img.com/user-avatar/p01Jmdje2qm0/EM0PZfETYPmMOj.jpg",
						"user_id": "195****16",
						"user_name": "灿烂༒人生",
						"user_reply_num": "1",
						"user_replys": []
					},
					{
						"add_time": "2022-10-10",
						"average_grade": "5",
						"comment_content": "可以的，刚发布的时候，价格有些小贵。性价比不那么高，后面降价以后，就很香了。非常好！",
						"comment_full_images": [
							"https://i1.mifile.cn/a2/1665379057_9246314_s1536_2048wh!540x5400.jpg",
							"https://i1.mifile.cn/a2/1665379057_9246314_s1536_2048wh!540x5400.jpg"
						],
						"comment_grade": 1,
						"comment_id": "219073517",
						"comment_images": [
							"https://i1.mifile.cn/a2/1665379057_9246314_s1536_2048wh!540x5400.jpg?w=540&h=720"
						],
						"comment_videos": [
							"https://i1.mifile.cn/a2/comment_video_1665379056_9764189.mp4"
						],
						"content_list_type": 0,
						"cover_videos": [
							{
								"cover_url": "https://i1.mifile.cn/a2/1665379056_2798312_s480_720wh.jpg",
								"video_url": "https://i1.mifile.cn/a2/comment_video_1665379056_9764189.mp4"
							}
						],
						"down": false,
						"goods_info": null,
						"has_up_customer": false,
						"image_infos": [
							{
								"img_url": "https://i1.mifile.cn/a2/1665379057_9246314_s1536_2048wh.jpg",
								"origin_size": 0,
								"origin_url": ""
							}
						],
						"is_anonymous": true,
						"mizone_content_type": "",
						"site_name": "小米官网",
						"total_grade": "5",
						"up": false,
						"up_customer_num": 0,
						"up_num": 6,
						"up_rate": 100,
						"user_avatar": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/0599f465efe793fee4af47e6de005efd.png",
						"user_id": "268****266",
						"user_name": "陈***",
						"user_reply_num": "",
						"user_replys": []
					}
				],
				"total": 11005,
				"trace_kv": "-requestId_732860579317491e93108d7adc80a3d5-pids_15615-tag_ids___img__"
			},
			"gallery_view": [
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/7ba11fdfecac708e7dcc3b98b537a83a.jpeg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/7ba11fdfecac708e7dcc3b98b537a83a.jpeg?f=webp",
					"type": "video",
					"video_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/1b74138de2d62caac782c1d7f1f16129.mp4"
				},
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/5061b08df9de9ca1c23ba97b58a340c8.jpg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/5061b08df9de9ca1c23ba97b58a340c8.jpg?f=webp",
					"type": "img"
				},
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/cdd534269f9326beaa4a493eab827070.jpg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/cdd534269f9326beaa4a493eab827070.jpg?f=webp",
					"type": "img"
				},
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/22bbbda34d4535698318fc9cf40d0334.jpg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/22bbbda34d4535698318fc9cf40d0334.jpg?f=webp",
					"type": "img"
				},
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bb7e0537300b002657566cd68920f176.jpg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bb7e0537300b002657566cd68920f176.jpg?f=webp",
					"type": "img"
				},
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c090f46e0d07ed6db835fe8f96cf41c1.jpg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c090f46e0d07ed6db835fe8f96cf41c1.jpg?f=webp",
					"type": "img"
				},
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0542966b26815adcb450e47b7bca70cc.jpg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0542966b26815adcb450e47b7bca70cc.jpg?f=webp",
					"type": "img"
				},
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/eab9a225caeae1c6c9f0673915ad295e.jpg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/eab9a225caeae1c6c9f0673915ad295e.jpg?f=webp",
					"type": "img"
				},
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/a9ccfcac5415437e23127e1219966a44.jpg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/a9ccfcac5415437e23127e1219966a44.jpg?f=webp",
					"type": "img"
				},
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/1d1d0ab8b94208e69ec929f4d95b3c17.jpg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/1d1d0ab8b94208e69ec929f4d95b3c17.jpg?f=webp",
					"type": "img"
				},
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8348f5f8d83643c9fcfc16dd240c4adc.jpg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8348f5f8d83643c9fcfc16dd240c4adc.jpg?f=webp",
					"type": "img"
				},
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bd48dcfd217a949798f2748a94bb0b41.jpg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/bd48dcfd217a949798f2748a94bb0b41.jpg?f=webp",
					"type": "img"
				},
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/93d64b29aa8aaff8fc9db0e9a665ad9e.jpg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/93d64b29aa8aaff8fc9db0e9a665ad9e.jpg?f=webp",
					"type": "img"
				},
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3e3d8b8212a6cc53d5db756531e1ba1d.jpg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3e3d8b8212a6cc53d5db756531e1ba1d.jpg?f=webp",
					"type": "img"
				},
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0ce722c3e15ed76a9654c20fca0c7faf.jpg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/0ce722c3e15ed76a9654c20fca0c7faf.jpg?f=webp",
					"type": "img"
				},
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/e85d8a06e1b0ea43d3b8fa397aaf5ebc.jpg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/e85d8a06e1b0ea43d3b8fa397aaf5ebc.jpg?f=webp",
					"type": "img"
				},
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/68bb35c26ff3f358362652df9b132d2a.jpg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/68bb35c26ff3f358362652df9b132d2a.jpg?f=webp",
					"type": "img"
				},
				{
					"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/577d8e3b1ba50a967e423c8562654b57.jpg?f=webp",
					"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/577d8e3b1ba50a967e423c8562654b57.jpg?f=webp",
					"type": "img"
				}
			],
			"relate_parts": {
				"parts": null,
				"title": ""
			},
			"related_data_view": {
				"data": null,
				"title": ""
			}
		},
		"goods_tpl_datas": {
			"23164": {
				"header": {
					"spm_b": "cms_23164"
				},
				"sections": [
					{
						"block_id": "3970187",
						"body": {
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "\\u3010\\u5355\\u54c1\\u9875\\u3011\\u4ea7\\u54c1\\u8f6e\\u64ad",
						"spm_c": "3970187",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3970187",
							"spm_params": "{\"component\":\"product_info_product_gallery\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u4ea7\\u54c1\\u8f6e\\u64ad\"}"
						},
						"stat": "product_info_product_gallery",
						"view_type": "product_info_product_gallery"
					},
					{
						"block_id": "3970191",
						"body": {
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "\\u3010\\u5355\\u54c1\\u9875\\u3011\\u4ea7\\u54c1\\u4ef7\\u683c",
						"spm_c": "3970191",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3970191",
							"spm_params": "{\"component\":\"product_info_product_price\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u4ea7\\u54c1\\u4ef7\\u683c\"}"
						},
						"stat": "product_info_product_price",
						"view_type": "product_info_product_price"
					},
					{
						"body": {
							"bg_color": "#FFF2EF",
							"height": "0",
							"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/e6420b919318477089f1d5cd6d7da260.png?w=1080&h=1",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "【单品页】优惠活动",
						"spm_stat": {},
						"stat": "product_info_activity",
						"view_type": "product_info_activity"
					},
					{
						"body": {
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "【单品页】新人红包",
						"spm_stat": {},
						"stat": "product_info_new_people",
						"view_type": "product_info_new_people"
					},
					{
						"body": {
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "【单品页】预约组件",
						"spm_stat": {},
						"stat": "product_info_subscribe_process",
						"view_type": "product_info_subscribe_process"
					},
					{
						"body": {
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"spm_stat": {},
						"stat": "product_info_popnew_product_tag",
						"view_type": "product_info_popnew_product_tag"
					},
					{
						"block_id": "3970189",
						"body": {
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "\\u3010\\u5355\\u54c1\\u9875\\u3011\\u4ea7\\u54c1\\u540d\\u79f0",
						"spm_c": "3970189",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3970189",
							"spm_params": "{\"component\":\"product_info_product_name\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u4ea7\\u54c1\\u540d\\u79f0\"}"
						},
						"stat": "product_info_product_name",
						"view_type": "product_info_product_name"
					},
					{
						"block_id": "3970190",
						"body": {
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "\\u3010\\u5355\\u54c1\\u9875\\u3011\\u4ea7\\u54c1\\u63cf\\u8ff0",
						"spm_c": "3970190",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3970190",
							"spm_params": "{\"component\":\"product_info_product_desc\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u4ea7\\u54c1\\u63cf\\u8ff0\"}"
						},
						"stat": "product_info_product_desc",
						"view_type": "product_info_product_desc"
					},
					{
						"body": {
							"action": {
								"path": "15962",
								"productId": "15962",
								"type": "product"
							},
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"text": "Redmi Note 11T Pro丨点击查看",
							"width": "0"
						},
						"components_name": "产品流量分发组件",
						"spm_stat": {},
						"stat": "product_info_flow",
						"view_type": "product_info_flow"
					},
					{
						"body": {
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "【单品页】入口",
						"spm_stat": {},
						"stat": "product_info_entrance",
						"view_type": "product_info_entrance"
					},
					{
						"block_id": "3970192",
						"body": {
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "\\u3010\\u5355\\u54c1\\u9875\\u3011\\u5173\\u952e\\u53c2\\u6570",
						"spm_c": "3970192",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3970192",
							"spm_params": "{\"component\":\"product_info_class_parameters\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u5173\\u952e\\u53c2\\u6570\"}"
						},
						"stat": "product_info_class_parameters",
						"view_type": "product_info_class_parameters"
					},
					{
						"block_id": "3682954",
						"body": {
							"action": {
								"log_code": "bpm=6.64.3682954.1&next=88.267.41.49",
								"spm_stat": {
									"spm_code": "cms_23164.3682954.2",
									"spm_params": "{\"component\":\"rank_list\"}"
								}
							},
							"home_action": {
								"log_code": "bpm=6.64.3682954.2&next=88.258",
								"spm_stat": {
									"spm_code": "cms_23164.3682954.3",
									"spm_params": "{\"component\":\"rank_list\"}"
								}
							},
							"icon": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/049ec9bcc093365914620c46965bf9ab.png",
							"id": "49",
							"margin_left": 0,
							"margin_right": 0,
							"name": "【手机直降热销榜】第1名"
						},
						"spm_c": "3682954",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3682954",
							"spm_params": "[]"
						},
						"stat": "rank_list",
						"view_type": "rank_list"
					},
					{
						"body": {
							"bg_color": "#F6F6F6",
							"height": "0",
							"line_height": "30",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"spm_stat": {},
						"stat": "blank_line",
						"view_type": "blank_line"
					},
					{
						"block_id": "3971115",
						"body": {
							"component_blank_line_bottom": 30,
							"height": "0",
							"items": [
								{
									"ad_level_id": 0,
									"desc": "至低享{recycle_diff_price}元购机",
									"margin_left": 0,
									"margin_right": 0,
									"name": "信用换新至高补贴480元",
									"type": "2"
								},
								{
									"ad_level_id": 0,
									"desc": "极速换新 至低享{recycle_diff_price}元购机",
									"margin_left": 0,
									"margin_right": 0,
									"name": "换新至低0元购新机",
									"type": "3"
								},
								{
									"ad_level_id": 0,
									"desc": "本机至低享{recycle_diff_price}元换机",
									"margin_left": 0,
									"margin_right": 0,
									"name": "一站换新至高补贴480元",
									"type": "1"
								}
							],
							"margin_left": 0,
							"margin_right": 0,
							"title": "以旧换新 - 单品页入口配置",
							"width": "0"
						},
						"components_name": "\\u3010\\u5355\\u54c1\\u9875\\u3011\\u4ee5\\u65e7\\u6362\\u65b0\\u5165\\u53e3\\u914d\\u7f6e",
						"spm_c": "3971115",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3971115",
							"spm_params": "{\"component\":\"recycle_page\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u4ee5\\u65e7\\u6362\\u65b0\\u5165\\u53e3\\u914d\\u7f6e\"}"
						},
						"stat": "recycle_page",
						"view_type": "recycle_page"
					},
					{
						"block_id": "3970193",
						"body": {
							"component_blank_line_bottom": 30,
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "\\u3010\\u5355\\u54c1\\u9875\\u3011\\u5229\\u76ca\\u70b9",
						"spm_c": "3970193",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3970193",
							"spm_params": "{\"component\":\"product_info_benefits\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u5229\\u76ca\\u70b9\"}"
						},
						"stat": "product_info_benefits",
						"view_type": "product_info_benefits"
					},
					{
						"block_id": "3970194",
						"body": {
							"component_blank_line_bottom": 30,
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "\\u3010\\u5355\\u54c1\\u9875\\u3011\\u9009\\u62e9\\u7248\\u672c",
						"spm_c": "3970194",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3970194",
							"spm_params": "{\"component\":\"product_info_choose_version\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u9009\\u62e9\\u7248\\u672c\"}"
						},
						"stat": "product_info_choose_version",
						"view_type": "product_info_choose_version"
					},
					{
						"body": {
							"component_blank_line_bottom": 30,
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "【单品页】推荐套装",
						"spm_stat": {},
						"stat": "product_info_recommend_batch",
						"view_type": "product_info_recommend_batch"
					},
					{
						"body": {
							"component_blank_line_bottom": 30,
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "【单品页】推荐搭配",
						"spm_stat": {},
						"stat": "product_info_recommend_match",
						"view_type": "product_info_recommend_match"
					},
					{
						"block_id": "3639257",
						"body": {
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"spm_c": "3639257",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3639257",
							"spm_params": "[]"
						},
						"stat": "mi_circle_package",
						"view_type": "product_info_mi_circle"
					},
					{
						"block_id": "3639257",
						"body": {
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"name": "【单品页】买家秀",
						"spm_c": "3639257",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3639257",
							"spm_params": "[]"
						},
						"stat": "mi_circle_package",
						"view_type": "product_info_comment_v2"
					},
					{
						"block_id": "3639257",
						"body": {
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"name": "【单品页】问大家",
						"spm_c": "3639257",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3639257",
							"spm_params": "[]"
						},
						"stat": "mi_circle_package",
						"view_type": "product_info_ask_everyone"
					},
					{
						"block_id": "3639257",
						"body": {
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"name": "【单品页】米粉点评团",
						"spm_c": "3639257",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3639257",
							"spm_params": "[]"
						},
						"stat": "mi_circle_package",
						"view_type": "product_info_content_v2"
					},
					{
						"block_id": "3970195",
						"body": {
							"component_blank_line_bottom": 30,
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "\\u3010\\u5355\\u54c1\\u9875\\u3011\\u8bc4\\u4ef7",
						"spm_c": "3970195",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3970195",
							"spm_params": "{\"component\":\"product_info_comment\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u8bc4\\u4ef7\"}"
						},
						"stat": "product_content_package",
						"view_type": "product_info_comment"
					},
					{
						"block_id": "3639257",
						"body": {
							"action": {
								"log_code": "bpm=6.64.3639257.364&uni=2.15615&next=18.70.39.60677",
								"spm_stat": {
									"spm_code": "cms_23164.3639257.2",
									"spm_params": "{\"component\":\"product_info_content\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u8fbe\\u4eba\\u63a8\\u8350\"}"
								}
							},
							"content_id": "60677",
							"content_type": 4,
							"items": [
								{
									"action": {
										"log_code": "bpm=6.64.3639257.365&uni=2.15615&next=18.62.34.1346209",
										"spm_stat": {
											"scm": "server.0.0.0.tiezi.1346209.0.0",
											"spm_code": "cms_23164.3639257.10001",
											"spm_params": "{\"component\":\"product_info_content\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u8fbe\\u4eba\\u63a8\\u8350\"}"
										}
									},
									"ad_level_id": 0,
									"comment_num": "2",
									"icon": "https://cdn.cnbj1.fds.api.mi-img.com/user-avatar/b8e89c9b-e666-4994-9c09-b49f74518e79.jpg?thumb=150x150",
									"id": "1346209",
									"img_url_list": [
										"https://cdn.fds-ssl.api.xiaomi.com/b2c-data-mishop/de864671b616d1f0e68c595901c7e9b0.jpg?w=1500&h=2000"
									],
									"margin_left": 0,
									"margin_right": 0,
									"mid": "crypt-5edc15fa04f3e6b0c6142666419594a2efc74d8bb8b3f5",
									"name": "晚安，深圳！📷 Shot On Redmi K50 Pro",
									"nickname": "揭阳大雨",
									"praise_num": 54,
									"type": "1",
									"view_num": "1821"
								},
								{
									"action": {
										"log_code": "bpm=6.64.3639257.366&uni=2.15615&next=18.61.40.1292042",
										"spm_stat": {
											"scm": "server.0.0.0.article.1292042.0.0",
											"spm_code": "cms_23164.3639257.10002",
											"spm_params": "{\"component\":\"product_info_content\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u8fbe\\u4eba\\u63a8\\u8350\"}"
										}
									},
									"ad_level_id": 0,
									"comment_num": "179",
									"icon": "http://i8.mifile.cn/b2c-mimall-media/fa83661ee38a1495b26a59e73ae15eb3.png",
									"id": "1292042",
									"img_url_list": [
										"https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4bca43e4d5ddeb6cd63a00312d8eaa79.jpg?w=160&h=90"
									],
									"margin_left": 0,
									"margin_right": 0,
									"mid": "crypt-2ccaa5b267d66f9cac39748fcf1a4bcde8b36d2e28442360",
									"name": "如何用K50G的肩键实现王者荣耀1秒换装",
									"nickname": "巨蟹座的Hans♋️",
									"praise_num": 1427,
									"type": "3",
									"view_num": "2.3万"
								},
								{
									"action": {
										"log_code": "bpm=6.64.3639257.367&uni=2.15615&next=18.61.40.1311559",
										"spm_stat": {
											"scm": "server.0.0.0.article.1311559.0.0",
											"spm_code": "cms_23164.3639257.10003",
											"spm_params": "{\"component\":\"product_info_content\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u8fbe\\u4eba\\u63a8\\u8350\"}"
										}
									},
									"ad_level_id": 0,
									"comment_num": "2",
									"icon": "https://cdn.cnbj1.fds.api.mi-img.com/user-avatar/b8e89c9b-e666-4994-9c09-b49f74518e79.jpg?thumb=150x150",
									"id": "1311559",
									"img_url_list": [
										"https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/fcae3986eaa907898f313468a0c8eeb1.jpg?w=160&h=90"
									],
									"margin_left": 0,
									"margin_right": 0,
									"mid": "crypt-a1fe68095ae90bbf7d6688bdcd6417792e22db1f54ae53",
									"name": "K50电竞版拍照怎样？20张照片告诉你！",
									"nickname": "揭阳大雨",
									"praise_num": 86,
									"type": "3",
									"view_num": "9618"
								},
								{
									"action": {
										"log_code": "bpm=6.64.3639257.368&uni=2.15615&next=18.61.40.1311380",
										"spm_stat": {
											"scm": "server.0.0.0.article.1311380.0.0",
											"spm_code": "cms_23164.3639257.10004",
											"spm_params": "{\"component\":\"product_info_content\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u8fbe\\u4eba\\u63a8\\u8350\"}"
										}
									},
									"ad_level_id": 0,
									"comment_num": "0",
									"icon": "https://cdn.cnbj1.fds.api.mi-img.com/user-avatar/b8e89c9b-e666-4994-9c09-b49f74518e79.jpg?thumb=150x150",
									"id": "1311380",
									"img_url_list": [
										"https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/e805d27941760f57af51986299d6c263.jpg?w=160&h=90"
									],
									"margin_left": 0,
									"margin_right": 0,
									"mid": "crypt-163e230b99cdab8d63f15f0779e67daa633b4fe482959b",
									"name": "Redmi K50电竞版：春天，你好！",
									"nickname": "揭阳大雨",
									"praise_num": 31,
									"type": "3",
									"view_num": "357"
								},
								{
									"action": {
										"log_code": "bpm=6.64.3639257.369&uni=2.15615&next=18.61.40.1306876",
										"spm_stat": {
											"scm": "server.0.0.0.article.1306876.0.0",
											"spm_code": "cms_23164.3639257.10005",
											"spm_params": "{\"component\":\"product_info_content\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u8fbe\\u4eba\\u63a8\\u8350\"}"
										}
									},
									"ad_level_id": 0,
									"comment_num": "154",
									"icon": "https://cdn.cnbj1.fds.api.mi-img.com/user-avatar/2b4d4ff9-6a22-4bbf-a3ae-066e575cdf87.jpg?thumb=150x150",
									"id": "1306876",
									"img_url_list": [
										"https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/289987e889e55842c00464bb22b70bf9.png?w=1223&h=688"
									],
									"margin_left": 0,
									"margin_right": 0,
									"mid": "crypt-da6195c7bea9349b9bdf2713a05b35c63962a6009be40132d5",
									"name": "Redmi K50系列、K40s超全对比",
									"nickname": "笔点酷玩",
									"praise_num": 1208,
									"type": "3",
									"view_num": "8.7万"
								},
								{
									"action": {
										"log_code": "bpm=6.64.3639257.370&uni=2.15615&next=18.61.40.1296790",
										"spm_stat": {
											"scm": "server.0.0.0.article.1296790.0.0",
											"spm_code": "cms_23164.3639257.10006",
											"spm_params": "{\"component\":\"product_info_content\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u8fbe\\u4eba\\u63a8\\u8350\"}"
										}
									},
									"ad_level_id": 0,
									"comment_num": "0",
									"icon": "https://cdn.cnbj1.fds.api.mi-img.com/user-avatar/p01wFp0kNIsF/N6oGyHUUmceKba.jpg?thumb=150x150",
									"id": "1296790",
									"img_url_list": [
										"https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/c87e137ea5ba8b4aced2411dfc1b6648.png?w=160&h=90"
									],
									"margin_left": 0,
									"margin_right": 0,
									"mid": "crypt-ca5f1c774610ccfb253f85aeb4171d22c1693dc2b1b1b1fc15",
									"name": "它到底有什么魅力，让消费者如此看重",
									"nickname": "吖有",
									"praise_num": 4,
									"type": "3",
									"view_num": "711"
								},
								{
									"action": {
										"log_code": "bpm=6.64.3639257.371&uni=2.15615&next=18.61.40.1295692",
										"spm_stat": {
											"scm": "server.0.0.0.article.1295692.0.0",
											"spm_code": "cms_23164.3639257.10007",
											"spm_params": "{\"component\":\"product_info_content\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u8fbe\\u4eba\\u63a8\\u8350\"}"
										}
									},
									"ad_level_id": 0,
									"comment_num": "2",
									"icon": "https://cdn.cnbj1.fds.api.mi-img.com/user-avatar/b8e89c9b-e666-4994-9c09-b49f74518e79.jpg?thumb=150x150",
									"id": "1295692",
									"img_url_list": [
										"https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/9fab5d704d63541145253003df809639.jpg"
									],
									"margin_left": 0,
									"margin_right": 0,
									"mid": "crypt-747f4599c253415977c51a7ba7034f726459d824ee77c0",
									"name": "K50电竞版：冰斩色开箱，震撼来袭！",
									"nickname": "揭阳大雨",
									"praise_num": 10,
									"type": "3",
									"view_num": "571"
								}
							],
							"margin_left": 0,
							"margin_right": 0,
							"show_title": true,
							"title": "米粉点评团"
						},
						"components_name": "\\u3010\\u5355\\u54c1\\u9875\\u3011\\u8fbe\\u4eba\\u63a8\\u8350",
						"spm_c": "3639257",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3639257",
							"spm_params": "{\"component\":\"product_info_content\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u8fbe\\u4eba\\u63a8\\u8350\"}"
						},
						"stat": "product_content_package",
						"view_type": "product_info_content"
					},
					{
						"body": {
							"component_blank_line_bottom": 30,
							"h": 200,
							"height": "0",
							"items": [
								{
									"action": {
										"log_code": "31wapotherblank_line001115#t=ad&act=webview&page=other&page_id=23165&bid=3970294.1&adp=5778&adm=34559&bpm=6.64.3970294.1&cdm=&uni=2&next=",
										"path": "https://m.mi.com/w/mishop_activity?_rt=weex&pageid=1057&sign=ebe97b04c8c24591f435462717eb56e5&pdl=jianyu",
										"type": "url"
									},
									"ad_level_id": 0,
									"ad_position_id": 5778,
									"h": 200,
									"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c43bcab0907e449909a25d06ce7c56db.jpg?f=webp&w=1080&h=300&bg=FFFFFF",
									"img_url_color": "#FFFFFF",
									"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c43bcab0907e449909a25d06ce7c56db.jpg?f=webp&w=1080&h=300&bg=FFFFFF",
									"margin_left": 0,
									"margin_right": 0,
									"material_id": 34559,
									"type": "image",
									"w": 720
								},
								{
									"action": {
										"log_code": "31wapotherblank_line002115#t=ad&act=product&page=other&pid=16644&page_id=23165&bid=3970294.2&adp=5763&adm=34504&bpm=6.64.3970294.2&cdm=&uni=2&next=",
										"path": "16644",
										"productId": "16644",
										"type": "product"
									},
									"ad_level_id": 0,
									"ad_position_id": 5763,
									"h": 200,
									"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/852c45d2691c156f60c2645d8581a591.png?f=webp&w=1080&h=300&bg=0",
									"img_url_color": "#0",
									"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/852c45d2691c156f60c2645d8581a591.png?f=webp&w=1080&h=300&bg=0",
									"margin_left": 0,
									"margin_right": 0,
									"material_id": 34504,
									"type": "image",
									"w": 720
								},
								{
									"action": {
										"log_code": "31wapotherblank_line003115#t=ad&act=webview&page=other&page_id=23165&bid=3970294.3&adp=5684&adm=34347&bpm=6.64.3970294.3&cdm=&uni=2&next=",
										"path": "https://m.mi.com/w/mishop_activity?_rt=weex&pageid=311&sign=5b5a7208ff06df839f2345f7b0144e43&pdl=jianyu",
										"type": "url"
									},
									"ad_level_id": 0,
									"ad_position_id": 5684,
									"h": 200,
									"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/4c5113489686db8c01b7a37834df0638.jpg?f=webp&w=1080&h=300&bg=F6E9D9",
									"img_url_color": "#F6E9D9",
									"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/4c5113489686db8c01b7a37834df0638.jpg?f=webp&w=1080&h=300&bg=F6E9D9",
									"margin_left": 0,
									"margin_right": 0,
									"material_id": 34347,
									"type": "image",
									"w": 720
								},
								{
									"action": {
										"log_code": "31wapotherblank_line004115#t=ad&act=product&page=other&pid=16326&page_id=23165&bid=3970294.4&adp=5787&adm=34600&bpm=6.64.3970294.4&cdm=&uni=2&next=",
										"path": "16326",
										"productId": "16326",
										"type": "product"
									},
									"ad_level_id": 0,
									"ad_position_id": 5787,
									"h": 200,
									"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c715923fb10270dd710ae08ac7ce4e16.jpg?f=webp&w=1080&h=300&bg=FFFFFF",
									"img_url_color": "#FFFFFF",
									"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/c715923fb10270dd710ae08ac7ce4e16.jpg?f=webp&w=1080&h=300&bg=FFFFFF",
									"margin_left": 0,
									"margin_right": 0,
									"material_id": 34600,
									"type": "image",
									"w": 720
								},
								{
									"action": {
										"log_code": "31wapotherblank_line005115#t=ad&act=product&page=other&pid=15694&page_id=23165&bid=3970294.5&adp=5786&adm=34599&bpm=6.64.3970294.5&cdm=&uni=2&next=",
										"path": "15694",
										"productId": "15694",
										"type": "product"
									},
									"ad_level_id": 0,
									"ad_position_id": 5786,
									"h": 200,
									"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3abba427bb7ee5c6f9182036064aed9c.jpg?f=webp&w=1080&h=300&bg=FFFFFF",
									"img_url_color": "#FFFFFF",
									"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/3abba427bb7ee5c6f9182036064aed9c.jpg?f=webp&w=1080&h=300&bg=FFFFFF",
									"margin_left": 0,
									"margin_right": 0,
									"material_id": 34599,
									"type": "image",
									"w": 720
								},
								{
									"action": {
										"log_code": "31wapotherblank_line006115#t=ad&act=webview&page=other&page_id=23165&bid=3970294.6&adp=5788&adm=34601&bpm=6.64.3970294.6&cdm=&uni=2&next=",
										"path": "https://m.mi.com/w/mishop_activity?_rt=weex&pageid=1052&sign=1052e200247a59904577ea00ca3d0995&pdl=jianyu",
										"type": "url"
									},
									"ad_level_id": 0,
									"ad_position_id": 5788,
									"h": 200,
									"img_url": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8e1742d49be3f6dc3b2a9ffdde9a64ce.jpg?f=webp&w=1080&h=300&bg=E3E9FF",
									"img_url_color": "#E3E9FF",
									"img_url_webp": "//cdn.cnbj1.fds.api.mi-img.com/mi-mall/8e1742d49be3f6dc3b2a9ffdde9a64ce.jpg?f=webp&w=1080&h=300&bg=E3E9FF",
									"margin_left": 0,
									"margin_right": 0,
									"material_id": 34601,
									"type": "image",
									"w": 720
								}
							],
							"margin_left": 32,
							"margin_right": 32,
							"title": "手机产品站轮播（全品）",
							"w": 720,
							"width": "0"
						},
						"spm_stat": {},
						"stat": "product_info_wheel_ad",
						"view_type": "product_info_wheel_ad"
					},
					{
						"body": {
							"component_blank_line_bottom": 30,
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "【单品页】推荐及排行榜",
						"spm_stat": {},
						"stat": "product_info_rank",
						"view_type": "product_info_rank"
					},
					{
						"block_id": "3970196",
						"body": {
							"height": "0",
							"items": [
								{
									"ad_level_id": 0,
									"margin_left": 0,
									"margin_right": 0,
									"page_info": [
										{
											"block_id": "3989771",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/1cca05ed9afe8c80e9c18bdb30ce01dc.jpg?w=1080&h=1269&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/1cca05ed9afe8c80e9c18bdb30ce01dc.jpg?f=webp&w=1080&h=1269&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970199",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/189fa6415097090a56bac59ee91e6a71.jpg?w=1080&h=718&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/189fa6415097090a56bac59ee91e6a71.jpg?f=webp&w=1080&h=718&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970384",
											"body": {
												"h": 1080,
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a8825db0358b8cd29dcfd1a3a1ecac8c.jpg?w=1920&h=1080&bg=60604",
												"img_url_color": "#60604",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a8825db0358b8cd29dcfd1a3a1ecac8c.jpg?f=webp&w=1920&h=1080&bg=60604",
												"margin_left": 0,
												"margin_right": 0,
												"video_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/7de071d88ae1d0b5a74ca726a19e633e.mp4",
												"w": 1920
											},
											"components_name": "【单品页】单个视频-高度不限",
											"view_type": "video_view"
										},
										{
											"block_id": "3970200",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/42cc59cfb1b779bd636a8422e43d47a9.jpg?w=1080&h=386&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/42cc59cfb1b779bd636a8422e43d47a9.jpg?f=webp&w=1080&h=386&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970201",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4397a61e0f27e72e6cbbc0f3e71a6b5c.jpg?w=1080&h=867&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4397a61e0f27e72e6cbbc0f3e71a6b5c.jpg?f=webp&w=1080&h=867&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970202",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/734c99a1386e7ca56e6a917ab033a5de.jpg?w=1080&h=818&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/734c99a1386e7ca56e6a917ab033a5de.jpg?f=webp&w=1080&h=818&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970203",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/1922aa1c3c6a65958ef98e8fe48b274f.jpg?w=1080&h=850&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/1922aa1c3c6a65958ef98e8fe48b274f.jpg?f=webp&w=1080&h=850&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970204",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/bc591acccf03159c6779abf28b0da3c9.jpg?w=1080&h=1113&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/bc591acccf03159c6779abf28b0da3c9.jpg?f=webp&w=1080&h=1113&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970205",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/9b6b2fbebe57461c4360c07c89cbcbe7.jpg?w=1080&h=578&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/9b6b2fbebe57461c4360c07c89cbcbe7.jpg?f=webp&w=1080&h=578&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970206",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/98d5d1bc497fc4f76612526c98876a6f.jpg?w=1080&h=672&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/98d5d1bc497fc4f76612526c98876a6f.jpg?f=webp&w=1080&h=672&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970207",
											"body": {
												"bg_color": "#000000",
												"h": 1708,
												"items": [
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4e4ad15477d20fc0c4d74a5a8afdb3c1.jpg?w=1080&h=1708&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4e4ad15477d20fc0c4d74a5a8afdb3c1.jpg?f=webp&w=1080&h=1708&bg=0",
														"margin_left": 0,
														"margin_right": 0
													},
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/08d2da1a6ad98684f4e06d65d4507544.jpg?w=1080&h=1708&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/08d2da1a6ad98684f4e06d65d4507544.jpg?f=webp&w=1080&h=1708&bg=0",
														"margin_left": 0,
														"margin_right": 0
													},
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/e845378c00098022c13e4a51d0b89ed2.jpg?w=1080&h=1708&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/e845378c00098022c13e4a51d0b89ed2.jpg?f=webp&w=1080&h=1708&bg=0",
														"margin_left": 0,
														"margin_right": 0
													}
												],
												"margin_left": 0,
												"margin_right": 0,
												"w": 1080
											},
											"components_name": "【单品页】图片轮播",
											"view_type": "gallery_w_1080"
										},
										{
											"block_id": "3970208",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4964ba513fcf4f03d1a435b3dcc10264.jpg?w=1080&h=787&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4964ba513fcf4f03d1a435b3dcc10264.jpg?f=webp&w=1080&h=787&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970209",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/e47c721cefcf1f3b585a1e14e90196ea.jpg?w=1080&h=1339&bg=10101",
												"img_url_color": "#10101",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/e47c721cefcf1f3b585a1e14e90196ea.jpg?f=webp&w=1080&h=1339&bg=10101",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970210",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/fe93eb10e13f0c5bbd4e0f3b0ed2bc3b.jpg?w=1080&h=769&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/fe93eb10e13f0c5bbd4e0f3b0ed2bc3b.jpg?f=webp&w=1080&h=769&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970211",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/0879821a9fddac67d276cff9e7fee582.jpg?w=1080&h=515&bg=80808",
												"img_url_color": "#80808",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/0879821a9fddac67d276cff9e7fee582.jpg?f=webp&w=1080&h=515&bg=80808",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970212",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/23de3a9184fc21a26e6187adf923b9a6.jpg?w=1080&h=617&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/23de3a9184fc21a26e6187adf923b9a6.jpg?f=webp&w=1080&h=617&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970213",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/b2568d94a006b6f698771d3972119afd.jpg?w=1080&h=907&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/b2568d94a006b6f698771d3972119afd.jpg?f=webp&w=1080&h=907&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970214",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/b996994f03a2438440de3bc6a2766e13.jpg?w=1080&h=393&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/b996994f03a2438440de3bc6a2766e13.jpg?f=webp&w=1080&h=393&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970215",
											"body": {
												"bg_color": "#000000",
												"h": 1320,
												"items": [
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/deed4404e7116bd8372dc75891a991c4.jpg?w=1080&h=1320&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/deed4404e7116bd8372dc75891a991c4.jpg?f=webp&w=1080&h=1320&bg=0",
														"margin_left": 0,
														"margin_right": 0
													},
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/56626b17a3e24dd1fc8fc043547dc69b.jpg?w=1080&h=1320&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/56626b17a3e24dd1fc8fc043547dc69b.jpg?f=webp&w=1080&h=1320&bg=0",
														"margin_left": 0,
														"margin_right": 0
													},
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/cab48290935ab8a8810d89fd2acaacd2.jpg?w=1080&h=1320&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/cab48290935ab8a8810d89fd2acaacd2.jpg?f=webp&w=1080&h=1320&bg=0",
														"margin_left": 0,
														"margin_right": 0
													},
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f25093e915b3a166aee4132ab8783399.jpg?w=1080&h=1320&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f25093e915b3a166aee4132ab8783399.jpg?f=webp&w=1080&h=1320&bg=0",
														"margin_left": 0,
														"margin_right": 0
													},
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/fdcbe344b4b82d134f86ad59632796db.jpg?w=1080&h=1320&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/fdcbe344b4b82d134f86ad59632796db.jpg?f=webp&w=1080&h=1320&bg=0",
														"margin_left": 0,
														"margin_right": 0
													}
												],
												"margin_left": 0,
												"margin_right": 0,
												"w": 1080
											},
											"components_name": "【单品页】图片轮播",
											"view_type": "gallery_w_1080"
										},
										{
											"block_id": "3970216",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/9d3c9ba35438277a96d0156d040f16d3.jpg?w=1080&h=861&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/9d3c9ba35438277a96d0156d040f16d3.jpg?f=webp&w=1080&h=861&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970217",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4089596f69eb77073412c4755a398157.jpg?w=1080&h=607&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4089596f69eb77073412c4755a398157.jpg?f=webp&w=1080&h=607&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970218",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4af6c32120c06dce1ba4c180dc551a4d.jpg?w=1080&h=693&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4af6c32120c06dce1ba4c180dc551a4d.jpg?f=webp&w=1080&h=693&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970219",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/b92cb05f91fbd85a96afadf1d2aa3c48.jpg?w=1080&h=895&bg=10302",
												"img_url_color": "#10302",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/b92cb05f91fbd85a96afadf1d2aa3c48.jpg?f=webp&w=1080&h=895&bg=10302",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970220",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d2fd672c0a128adc837983e283ca9815.jpg?w=1080&h=962&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d2fd672c0a128adc837983e283ca9815.jpg?f=webp&w=1080&h=962&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970221",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/7753b6659dce64e8c9b3580293f1315f.jpg?w=1080&h=729&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/7753b6659dce64e8c9b3580293f1315f.jpg?f=webp&w=1080&h=729&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970222",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/eb7dcd42bc6d5436bb3c8ba4e9ee4df9.jpg?w=1080&h=731&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/eb7dcd42bc6d5436bb3c8ba4e9ee4df9.jpg?f=webp&w=1080&h=731&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970223",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/7ae6ef7920b2c9de2c79028cee94216c.jpg?w=1080&h=1310&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/7ae6ef7920b2c9de2c79028cee94216c.jpg?f=webp&w=1080&h=1310&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970224",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/02f445b4f4c4e63f6705177fd86b8432.jpg?w=1080&h=687&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/02f445b4f4c4e63f6705177fd86b8432.jpg?f=webp&w=1080&h=687&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970225",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/07eab33d0865a0bdc4b2dfbf434c5a59.jpg?w=1080&h=967&bg=10101",
												"img_url_color": "#10101",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/07eab33d0865a0bdc4b2dfbf434c5a59.jpg?f=webp&w=1080&h=967&bg=10101",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970226",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a17d7bb270aed5fbc9554bcab6ed4848.jpg?w=1080&h=917&bg=110705",
												"img_url_color": "#110705",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a17d7bb270aed5fbc9554bcab6ed4848.jpg?f=webp&w=1080&h=917&bg=110705",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970227",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/5076a37d5f83fc8d655c2a355d58d2b8.jpg?w=1080&h=808&bg=11DDFE",
												"img_url_color": "#11DDFE",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/5076a37d5f83fc8d655c2a355d58d2b8.jpg?f=webp&w=1080&h=808&bg=11DDFE",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970228",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/3dbf2de3d48b7604545e57279abdcbf9.jpg?w=1080&h=926&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/3dbf2de3d48b7604545e57279abdcbf9.jpg?f=webp&w=1080&h=926&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970229",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f638dee70421f7216a3c8a394775f7d5.jpg?w=1080&h=596&bg=533E39",
												"img_url_color": "#533E39",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f638dee70421f7216a3c8a394775f7d5.jpg?f=webp&w=1080&h=596&bg=533E39",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970230",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a79428cfb5eeeee0cdd4f91f30df1405.jpg?w=1080&h=978&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a79428cfb5eeeee0cdd4f91f30df1405.jpg?f=webp&w=1080&h=978&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970232",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/cc618869da009420c87b26fe3bb38dd7.jpg?w=1080&h=857&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/cc618869da009420c87b26fe3bb38dd7.jpg?f=webp&w=1080&h=857&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970233",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/781430dbe0b003f6a919fd0cdeb5071a.jpg?w=1080&h=667&bg=90909",
												"img_url_color": "#90909",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/781430dbe0b003f6a919fd0cdeb5071a.jpg?f=webp&w=1080&h=667&bg=90909",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970234",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/77d425f5f5074e2549d59ae8eed9ce3e.jpg?w=1080&h=571&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/77d425f5f5074e2549d59ae8eed9ce3e.jpg?f=webp&w=1080&h=571&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970235",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/ddc8c502aabcaba2478bc41f5fd49d77.jpg?w=1080&h=699&bg=20202",
												"img_url_color": "#20202",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/ddc8c502aabcaba2478bc41f5fd49d77.jpg?f=webp&w=1080&h=699&bg=20202",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970236",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/49d015d016218aa955eec191bda2cb55.jpg?w=1080&h=504&bg=70604",
												"img_url_color": "#70604",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/49d015d016218aa955eec191bda2cb55.jpg?f=webp&w=1080&h=504&bg=70604",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970237",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/07f0a00d84af25699292a880dc5550bc.jpg?w=1080&h=751&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/07f0a00d84af25699292a880dc5550bc.jpg?f=webp&w=1080&h=751&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970238",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/cb991a82d36290cf338657e936ca50ec.jpg?w=1080&h=1231&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/cb991a82d36290cf338657e936ca50ec.jpg?f=webp&w=1080&h=1231&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970239",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/74e6e7cc9e5bf2885c2dbf1cf50fc7a8.jpg?w=1080&h=1047&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/74e6e7cc9e5bf2885c2dbf1cf50fc7a8.jpg?f=webp&w=1080&h=1047&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970240",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/ca344e2d9fe9f24b5a6823a6f3ffbd8d.jpg?w=1080&h=837&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/ca344e2d9fe9f24b5a6823a6f3ffbd8d.jpg?f=webp&w=1080&h=837&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970241",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/89319daa4bbd534393f83299185f9f3a.jpg?w=1080&h=665&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/89319daa4bbd534393f83299185f9f3a.jpg?f=webp&w=1080&h=665&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970242",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/8ab48d36def4bbc0947094665edccecb.jpg?w=1080&h=790&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/8ab48d36def4bbc0947094665edccecb.jpg?f=webp&w=1080&h=790&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970243",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f7a4bbec363786a9d39c8cea441949ad.jpg?w=1080&h=586&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f7a4bbec363786a9d39c8cea441949ad.jpg?f=webp&w=1080&h=586&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970244",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/3e9bee9ca9ac39e5ca5b3284d87d6465.jpg?w=1080&h=662&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/3e9bee9ca9ac39e5ca5b3284d87d6465.jpg?f=webp&w=1080&h=662&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970245",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f9775918cc977967aa5d3fda94369f2d.jpg?w=1080&h=841&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f9775918cc977967aa5d3fda94369f2d.jpg?f=webp&w=1080&h=841&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970246",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a4e2ed7076714e2c482f48b2eae6b530.jpg?w=1080&h=426&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a4e2ed7076714e2c482f48b2eae6b530.jpg?f=webp&w=1080&h=426&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970247",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/011143be144721eadfbbccca944f8654.jpg?w=1080&h=1040&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/011143be144721eadfbbccca944f8654.jpg?f=webp&w=1080&h=1040&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970248",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a559eec2ec623c47a2b1ec0b0eaf6c76.jpg?w=1080&h=725&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a559eec2ec623c47a2b1ec0b0eaf6c76.jpg?f=webp&w=1080&h=725&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970249",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/bbc3a94768bea8ee42a69ef86908b33c.jpg?w=1080&h=698&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/bbc3a94768bea8ee42a69ef86908b33c.jpg?f=webp&w=1080&h=698&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970250",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d5f0714fc0e2e3acfeed39915fe902a5.jpg?w=1080&h=587&bg=20202",
												"img_url_color": "#20202",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d5f0714fc0e2e3acfeed39915fe902a5.jpg?f=webp&w=1080&h=587&bg=20202",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970251",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/cd1f92f654af2b89d93e18d20e408e34.jpg?w=1080&h=633&bg=10101",
												"img_url_color": "#10101",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/cd1f92f654af2b89d93e18d20e408e34.jpg?f=webp&w=1080&h=633&bg=10101",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970252",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/9907397206709bd7a2d3bfa3e0402069.jpg?w=1080&h=735&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/9907397206709bd7a2d3bfa3e0402069.jpg?f=webp&w=1080&h=735&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970253",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4efbc49bde8890c5b61e9a0148c9a349.jpg?w=1080&h=1029&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4efbc49bde8890c5b61e9a0148c9a349.jpg?f=webp&w=1080&h=1029&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970254",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/e3354de95f018db7451ab2b7c835bbcf.jpg?w=1080&h=477&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/e3354de95f018db7451ab2b7c835bbcf.jpg?f=webp&w=1080&h=477&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970255",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/3e4397ba2de408658246f09b7bdbf023.jpg?w=1080&h=1023&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/3e4397ba2de408658246f09b7bdbf023.jpg?f=webp&w=1080&h=1023&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970256",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/e8dacc6dfc291d919ae6603e876e1368.jpg?w=1080&h=798&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/e8dacc6dfc291d919ae6603e876e1368.jpg?f=webp&w=1080&h=798&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970257",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/57637c36077fb35146103621e0c20946.jpg?w=1080&h=781&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/57637c36077fb35146103621e0c20946.jpg?f=webp&w=1080&h=781&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970258",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/7f49394bc61d42fdb6b17228b929c511.jpg?w=1080&h=671&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/7f49394bc61d42fdb6b17228b929c511.jpg?f=webp&w=1080&h=671&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970259",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/792aba3cb2accb9abab9704d4e59f7b1.jpg?w=1080&h=650&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/792aba3cb2accb9abab9704d4e59f7b1.jpg?f=webp&w=1080&h=650&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970260",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a2b1b817caf8b654e49bd4a293b1a536.jpg?w=1080&h=667&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a2b1b817caf8b654e49bd4a293b1a536.jpg?f=webp&w=1080&h=667&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970261",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/ff3ea7318b2a0c26cd12ae64b54eae61.jpg?w=1080&h=551&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/ff3ea7318b2a0c26cd12ae64b54eae61.jpg?f=webp&w=1080&h=551&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970262",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/899efdb9c980960920baccf0bc44015d.jpg?w=1080&h=780&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/899efdb9c980960920baccf0bc44015d.jpg?f=webp&w=1080&h=780&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970263",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/c81c25d1ee5ddb2ab232ca334ea820a4.jpg?w=1080&h=1015&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/c81c25d1ee5ddb2ab232ca334ea820a4.jpg?f=webp&w=1080&h=1015&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970264",
											"body": {
												"bg_color": "#000000",
												"h": 1282,
												"items": [
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4cf245881db60e6aec3ac3cdd7592354.jpg?w=1080&h=1282&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4cf245881db60e6aec3ac3cdd7592354.jpg?f=webp&w=1080&h=1282&bg=0",
														"margin_left": 0,
														"margin_right": 0
													},
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/9bc6c26377f6a63bf1446d3c373eb1da.jpg?w=1080&h=1282&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/9bc6c26377f6a63bf1446d3c373eb1da.jpg?f=webp&w=1080&h=1282&bg=0",
														"margin_left": 0,
														"margin_right": 0
													},
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/5a5d178090c9cc683863fe77340da0fd.jpg?w=1080&h=1282&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/5a5d178090c9cc683863fe77340da0fd.jpg?f=webp&w=1080&h=1282&bg=0",
														"margin_left": 0,
														"margin_right": 0
													}
												],
												"margin_left": 0,
												"margin_right": 0,
												"w": 1080
											},
											"components_name": "【单品页】图片轮播",
											"view_type": "gallery_w_1080"
										},
										{
											"block_id": "3970265",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/c4077a04615554a502cef04730cd1ad2.jpg?w=1080&h=786&bg=10101",
												"img_url_color": "#10101",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/c4077a04615554a502cef04730cd1ad2.jpg?f=webp&w=1080&h=786&bg=10101",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970266",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/3af28a3b1931cc03bf9a85286f1b12fd.jpg?w=1080&h=842&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/3af28a3b1931cc03bf9a85286f1b12fd.jpg?f=webp&w=1080&h=842&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970267",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/2d82c170b6816e5f4fea5f52490d1683.jpg?w=1080&h=1120&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/2d82c170b6816e5f4fea5f52490d1683.jpg?f=webp&w=1080&h=1120&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970268",
											"body": {
												"bg_color": "#000000",
												"h": 958,
												"items": [
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/741b0a3f6792c55fa9cdb7f07a453259.jpg?w=1080&h=958&bg=10101",
														"img_url_color": "#10101",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/741b0a3f6792c55fa9cdb7f07a453259.jpg?f=webp&w=1080&h=958&bg=10101",
														"margin_left": 0,
														"margin_right": 0
													},
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/e37987c95eadcfd45c859e18aaac527a.jpg?w=1080&h=958&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/e37987c95eadcfd45c859e18aaac527a.jpg?f=webp&w=1080&h=958&bg=0",
														"margin_left": 0,
														"margin_right": 0
													},
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/25868297e5167b881d03340f22c7f2e9.jpg?w=1080&h=958&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/25868297e5167b881d03340f22c7f2e9.jpg?f=webp&w=1080&h=958&bg=0",
														"margin_left": 0,
														"margin_right": 0
													},
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/5c545db7fb846a7690d63b24d79614a2.jpg?w=1080&h=958&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/5c545db7fb846a7690d63b24d79614a2.jpg?f=webp&w=1080&h=958&bg=0",
														"margin_left": 0,
														"margin_right": 0
													}
												],
												"margin_left": 0,
												"margin_right": 0,
												"w": 1080
											},
											"components_name": "【单品页】图片轮播",
											"view_type": "gallery_w_1080"
										},
										{
											"block_id": "3970269",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/2d96c372c5aba54d1bf7f2bc1830a278.jpg?w=1080&h=571&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/2d96c372c5aba54d1bf7f2bc1830a278.jpg?f=webp&w=1080&h=571&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970270",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/85828ed0f032ba8283c20c0e8350c547.jpg?w=1080&h=786&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/85828ed0f032ba8283c20c0e8350c547.jpg?f=webp&w=1080&h=786&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970271",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/322cc43565f281de902867e183ad83ae.jpg?w=1080&h=468&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/322cc43565f281de902867e183ad83ae.jpg?f=webp&w=1080&h=468&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970272",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/e3af2519215e1e6e81139be1d1a6489f.jpg?w=1080&h=672&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/e3af2519215e1e6e81139be1d1a6489f.jpg?f=webp&w=1080&h=672&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970273",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a75c19c41db958b5109b80cc7a027abf.jpg?w=1080&h=1139&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a75c19c41db958b5109b80cc7a027abf.jpg?f=webp&w=1080&h=1139&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970274",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a155292c20472215173fd60afd52d570.jpg?w=1080&h=753&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a155292c20472215173fd60afd52d570.jpg?f=webp&w=1080&h=753&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970275",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/6cb2cb5a4d6d563dfea766f5c3bc4fcd.jpg?w=1080&h=465&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/6cb2cb5a4d6d563dfea766f5c3bc4fcd.jpg?f=webp&w=1080&h=465&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970276",
											"body": {
												"bg_color": "#000000",
												"h": 1464,
												"items": [
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a0553657145a258adc81b92f5b0163b0.jpg?w=1080&h=1464&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a0553657145a258adc81b92f5b0163b0.jpg?f=webp&w=1080&h=1464&bg=0",
														"margin_left": 0,
														"margin_right": 0
													},
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/0fdda7b33b90d4e20b947fc18df14219.jpg?w=1080&h=1464&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/0fdda7b33b90d4e20b947fc18df14219.jpg?f=webp&w=1080&h=1464&bg=0",
														"margin_left": 0,
														"margin_right": 0
													},
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/eb1f944f3662e5572e561b77669357df.jpg?w=1080&h=1464&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/eb1f944f3662e5572e561b77669357df.jpg?f=webp&w=1080&h=1464&bg=0",
														"margin_left": 0,
														"margin_right": 0
													},
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/962582258cfb503322b16f58e8fa8b47.jpg?w=1080&h=1464&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/962582258cfb503322b16f58e8fa8b47.jpg?f=webp&w=1080&h=1464&bg=0",
														"margin_left": 0,
														"margin_right": 0
													},
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/24e0b97b79e7333ccc25473cb2b49249.jpg?w=1080&h=1464&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/24e0b97b79e7333ccc25473cb2b49249.jpg?f=webp&w=1080&h=1464&bg=0",
														"margin_left": 0,
														"margin_right": 0
													}
												],
												"margin_left": 0,
												"margin_right": 0,
												"w": 1080
											},
											"components_name": "【单品页】图片轮播",
											"view_type": "gallery_w_1080"
										},
										{
											"block_id": "3970277",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/7008f205d68748d0e0ecbd2ad4628a1c.jpg?w=1080&h=149&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/7008f205d68748d0e0ecbd2ad4628a1c.jpg?f=webp&w=1080&h=149&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970278",
											"body": {
												"bg_color": "#000000",
												"h": 1399,
												"items": [
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/39989790b7fa069734c706601040d046.jpg?w=1080&h=1399&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/39989790b7fa069734c706601040d046.jpg?f=webp&w=1080&h=1399&bg=0",
														"margin_left": 0,
														"margin_right": 0
													},
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/179406a8f10869aaa2d3646a73a3aae4.jpg?w=1080&h=1399&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/179406a8f10869aaa2d3646a73a3aae4.jpg?f=webp&w=1080&h=1399&bg=0",
														"margin_left": 0,
														"margin_right": 0
													},
													{
														"ad_level_id": 0,
														"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/768911420826d91d6926c55e775d0fc0.jpg?w=1080&h=1399&bg=0",
														"img_url_color": "#0",
														"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/768911420826d91d6926c55e775d0fc0.jpg?f=webp&w=1080&h=1399&bg=0",
														"margin_left": 0,
														"margin_right": 0
													}
												],
												"margin_left": 0,
												"margin_right": 0,
												"w": 1080
											},
											"components_name": "【单品页】图片轮播",
											"view_type": "gallery_w_1080"
										},
										{
											"block_id": "3970279",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/305972198b7d3d38495061e6ec7f657b.jpg?w=1080&h=588&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/305972198b7d3d38495061e6ec7f657b.jpg?f=webp&w=1080&h=588&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970280",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/9d3643216616785250fe0ab5b36e1027.jpg?w=1080&h=831&bg=40404",
												"img_url_color": "#40404",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/9d3643216616785250fe0ab5b36e1027.jpg?f=webp&w=1080&h=831&bg=40404",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970281",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/0cbe060eb8fc3de503e91a859df18669.jpg?w=1080&h=1039&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/0cbe060eb8fc3de503e91a859df18669.jpg?f=webp&w=1080&h=1039&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970282",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/cfd4ca9ae269ad5b657486ed7e6d513b.jpg?w=1080&h=1485&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/cfd4ca9ae269ad5b657486ed7e6d513b.jpg?f=webp&w=1080&h=1485&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970283",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/b815685ac6a65a3e821754bf52fb14aa.jpg?w=1080&h=634&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/b815685ac6a65a3e821754bf52fb14aa.jpg?f=webp&w=1080&h=634&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970284",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/7b014d10051c57bbfc40fd4535f60e11.jpg?w=1080&h=560&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/7b014d10051c57bbfc40fd4535f60e11.jpg?f=webp&w=1080&h=560&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970285",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/2e218ae7de3fc4582f88ee474cf3cfaa.jpg?w=1080&h=803&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/2e218ae7de3fc4582f88ee474cf3cfaa.jpg?f=webp&w=1080&h=803&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970286",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/1e0b680394c2533519c1ab9e76aeb40a.jpg?w=1080&h=1487&bg=0",
												"img_url_color": "#0",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/1e0b680394c2533519c1ab9e76aeb40a.jpg?f=webp&w=1080&h=1487&bg=0",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970289",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/8b93ba8343eff94714584d509429e640.jpg?w=1080&h=521&bg=30000",
												"img_url_color": "#30000",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/8b93ba8343eff94714584d509429e640.jpg?f=webp&w=1080&h=521&bg=30000",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970290",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/51e531d854be9eb5954ce238fc232954.jpg?w=1080&h=1428&bg=D2348",
												"img_url_color": "#D2348",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/51e531d854be9eb5954ce238fc232954.jpg?f=webp&w=1080&h=1428&bg=D2348",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970291",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/7c4e8282f98b70cadb4bdd37d22efc62.jpg?w=1080&h=580&bg=A2248",
												"img_url_color": "#A2248",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/7c4e8282f98b70cadb4bdd37d22efc62.jpg?f=webp&w=1080&h=580&bg=A2248",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/8bb1ee8ab2445ce187e993e1c1ff64dc.png?w=1080&h=894",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/8bb1ee8ab2445ce187e993e1c1ff64dc.png?w=1080&h=894",
												"is_show_animation": 2,
												"margin_left": 0,
												"margin_right": 0
											},
											"stat": "product_ino_price_desc",
											"view_type": "image_w_1080"
										}
									],
									"tab_name": "商品介绍"
								},
								{
									"ad_level_id": 0,
									"margin_left": 0,
									"margin_right": 0,
									"page_info": [
										{
											"block_id": "3970295",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/aed48d3ea3078d4da3c7bdd48a5f859f.jpeg?w=1080&h=1310&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/aed48d3ea3078d4da3c7bdd48a5f859f.jpeg?f=webp&w=1080&h=1310&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970296",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/bf71505a6ea6feff15caea9f3373e5a3.jpg?w=1080&h=1042&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/bf71505a6ea6feff15caea9f3373e5a3.jpg?f=webp&w=1080&h=1042&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970297",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/62ed1e4e2165996434325c20f5b7f2ae.jpg?w=1080&h=1352&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/62ed1e4e2165996434325c20f5b7f2ae.jpg?f=webp&w=1080&h=1352&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970298",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/453c24f225c1a3d23094fec64ed696fc.jpeg?w=1080&h=2097&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/453c24f225c1a3d23094fec64ed696fc.jpeg?f=webp&w=1080&h=2097&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970299",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/29fb97e784607bf4727ceca56d339c33.jpg?w=1080&h=1119&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/29fb97e784607bf4727ceca56d339c33.jpg?f=webp&w=1080&h=1119&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970300",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/72348d992610e02397a482a5c7cab503.jpg?w=1080&h=1060&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/72348d992610e02397a482a5c7cab503.jpg?f=webp&w=1080&h=1060&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970301",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/6a6b145d9373983342f91ae8759016ce.jpg?w=1080&h=1147&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/6a6b145d9373983342f91ae8759016ce.jpg?f=webp&w=1080&h=1147&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970302",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a7d6dca6842dd8d48d5489245ead6e64.jpg?w=1080&h=1424&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a7d6dca6842dd8d48d5489245ead6e64.jpg?f=webp&w=1080&h=1424&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970303",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/8ed98c0d09f7ec2fa369761e181fe0f3.jpg?w=1080&h=918&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/8ed98c0d09f7ec2fa369761e181fe0f3.jpg?f=webp&w=1080&h=918&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970304",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/6d0abf8f1228cdb0d7f07f95670ce808.jpg?w=1080&h=948&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/6d0abf8f1228cdb0d7f07f95670ce808.jpg?f=webp&w=1080&h=948&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970305",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/8589978fd1f9c75dc0a528233ce898f9.jpg?w=1080&h=1019&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/8589978fd1f9c75dc0a528233ce898f9.jpg?f=webp&w=1080&h=1019&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970306",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f3c89282e87c1e10493f535353aa9996.jpg?w=1080&h=1089&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f3c89282e87c1e10493f535353aa9996.jpg?f=webp&w=1080&h=1089&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970307",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/25026efff20026258abffbd52454e054.jpeg?w=1080&h=1276&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/25026efff20026258abffbd52454e054.jpeg?f=webp&w=1080&h=1276&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970308",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/da7342f5863730b4b11a1fd9bb4420a1.jpg?w=1080&h=1808&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/da7342f5863730b4b11a1fd9bb4420a1.jpg?f=webp&w=1080&h=1808&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970309",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/42e7fc0fd7ea882e60462ad1c310f446.jpg?w=1080&h=1419&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/42e7fc0fd7ea882e60462ad1c310f446.jpg?f=webp&w=1080&h=1419&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970310",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/93c82fe5536229ad00fab1dc7f6e0ab8.jpg?w=1080&h=807&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/93c82fe5536229ad00fab1dc7f6e0ab8.jpg?f=webp&w=1080&h=807&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970311",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/1e5bc031744d8d9fc098a88b53f78a3d.jpg?w=1080&h=1081&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/1e5bc031744d8d9fc098a88b53f78a3d.jpg?f=webp&w=1080&h=1081&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										},
										{
											"block_id": "3970312",
											"body": {
												"img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/dd926283d718e7799019bdbbc8269702.jpg?w=1080&h=725&bg=FFFFFF",
												"img_url_color": "#FFFFFF",
												"img_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/dd926283d718e7799019bdbbc8269702.jpg?f=webp&w=1080&h=725&bg=FFFFFF",
												"margin_left": 0,
												"margin_right": 0
											},
											"components_name": "【单品页】图片",
											"view_type": "image_w_1080"
										}
									],
									"tab_name": "规格参数"
								}
							],
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "\\u3010\\u5355\\u54c1\\u9875\\u3011\\u591aTab",
						"spm_c": "3970196",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3970196",
							"spm_params": "{\"component\":\"product_info_tab\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u591aTab\"}"
						},
						"stat": "product_info_tab",
						"view_type": "product_info_tab"
					},
					{
						"block_id": "3970293",
						"body": {
							"height": "0",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "\\u3010\\u5355\\u54c1\\u9875\\u3011\\u63a8\\u8350",
						"spm_c": "3970293",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3970293",
							"spm_params": "{\"component\":\"product_info_recommend\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u63a8\\u8350\"}"
						},
						"stat": "product_info_recommend",
						"view_type": "product_info_recommend"
					},
					{
						"block_id": "3970294",
						"body": {
							"height": "0",
							"line_height": "168",
							"margin_left": 0,
							"margin_right": 0,
							"width": "0"
						},
						"components_name": "\\u3010\\u5355\\u54c1\\u9875\\u3011\\u7a7a\\u767d\\u884c",
						"spm_c": "3970294",
						"spm_stat": {
							"scm": "cms.0.0.0.0.0.0.0",
							"spm_code": "cms_23164.3970294",
							"spm_params": "{\"component\":\"blank_line\",\"component_name\":\"\\u3010\\u5355\\u54c1\\u9875\\u3011\\u7a7a\\u767d\\u884c\"}"
						},
						"stat": "blank_line",
						"view_type": "blank_line"
					}
				]
			}
		},
		"hot_parts": {
			"parts": [
				{
					"image_url": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202209221557_a006e9e2738b11144e3e1ebe09e80384.png",
					"image_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202209221557_a006e9e2738b11144e3e1ebe09e80384.png",
					"is_multi_price": true,
					"market_price": "1199",
					"name": "Redmi Note 11 5G",
					"price": "1099",
					"product_id": 15135
				},
				{
					"image_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/80a8d807bb2ccb1ab6398b2b71b20754.jpg?f=webp",
					"image_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/80a8d807bb2ccb1ab6398b2b71b20754.jpg?f=webp",
					"is_multi_price": true,
					"market_price": "2299",
					"name": "Xiaomi Civi",
					"price": "2099",
					"product_id": 14999
				},
				{
					"image_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/51f05d5691cac518f2a95866859e12e6.jpg?f=webp",
					"image_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/51f05d5691cac518f2a95866859e12e6.jpg?f=webp",
					"is_multi_price": true,
					"market_price": "3699",
					"name": "Xiaomi 12",
					"price": "3699",
					"product_id": 15491
				},
				{
					"image_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/fc8cdc4af718eb844f956194049a4a3e.jpg?f=webp",
					"image_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/fc8cdc4af718eb844f956194049a4a3e.jpg?f=webp",
					"is_multi_price": true,
					"market_price": "2999",
					"name": "Xiaomi 12X",
					"price": "2699",
					"product_id": 15476
				},
				{
					"image_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/6d0a3e7acc3e91e2cfd83dbbe1d1c029.jpg?f=webp",
					"image_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/6d0a3e7acc3e91e2cfd83dbbe1d1c029.jpg?f=webp",
					"is_multi_price": true,
					"market_price": "1799",
					"name": "Redmi Note 11 Pro",
					"price": "1599",
					"product_id": 15133
				},
				{
					"image_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/6d0a3e7acc3e91e2cfd83dbbe1d1c029.jpg?f=webp",
					"image_url_webp": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/6d0a3e7acc3e91e2cfd83dbbe1d1c029.jpg?f=webp",
					"is_multi_price": true,
					"market_price": "1999",
					"name": "Redmi Note 11 Pro+",
					"price": "1799",
					"product_id": 15136
				}
			],
			"title": "爆款推荐"
		},
		"product_info": {
			"activity_tips": {
				"action": {},
				"img_url": ""
			},
			"category_id": 2042,
			"float_action": {
				"action": null,
				"img_url": ""
			},
			"is_batched": false,
			"is_choose": true,
			"is_customer_service": true,
			"is_enable": true,
			"name": "K50 电竞版",
			"page_id": 23164,
			"price_tips": {},
			"product_desc": "骁龙8 年度旗舰｜「双VC」强劲散热｜以电竞标准打造｜120W神仙秒充｜OLED 柔性直屏｜JBL 宽频四单元扬声器｜新一代超宽频X轴马达｜磁动力弹出式肩键2.0｜前后双索尼相机｜红外Pro｜多功能NFC",
			"product_desc_ext": "",
			"product_gallery_icon": {},
			"product_id": 15615,
			"sell_point_desc": [
				"【冷血旗舰】骁龙8 年度性能旗舰",
				"【120W秒充】4700mAh大电量，17分钟充满",
				"【双VC强劲散热】双热源覆盖，驯服骁龙8 汹涌性能"
			],
			"share_content": ""
		},
		"real_product_info": {
			"15615": {
				"popnew_product_tag": {},
				"product_gallery_icon": {}
			}
		},
		"related_rank": {
			"data": null,
			"title": ""
		},
		"related_recommend": {
			"data": null,
			"title": ""
		},
		"seo": {
			"description": "骁龙8 年度旗舰｜「双VC」强劲散热｜以电竞标准打造｜120W神仙秒充｜OLED 柔性直屏｜JBL 宽频四单元扬声器｜新一代超宽频X轴马达｜磁动力弹出式肩键2.0｜前后双索尼相机｜红外Pro｜多功能NFC",
			"keywords": "Redmi,K50 电竞版,K50,电竞,电竞版,游戏手机,小米",
			"title": "Redmi K50 电竞版"
		},
		"server_time": 1668067645
	},
	"result": "ok"
};

module.exports = {
  getMiproduct:data
}