const HTTP = require("./request");
module.exports = {
  "getHomeData":function(){
      return HTTP({
        url:"/home/page",
        method:"get",
        data:{
          "page_type":"home",
          "page_id":0
        }
      })
  },
  
  "recommendBlank":function(){
      return HTTP({
        url:"/home/recommendBlank",
        method:"get"
      })
  },

  "homeHisearch":function(data){
    return HTTP({
      url:"/hisearch/query_v3",
      method:"get",
      data
    })
  }


}