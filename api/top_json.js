module.exports ={

  data:{
  "code": 0,
  "data": {
      "page_title": "办公配件",
      "product": [
          {
              "cat_id": 0,
              "comments_total": "67135",
              "is_multi_price": false,
              "is_stock": false,
              "log_code": "bpm=83.250.3881506.8\u0026cdm=0.1.MjAwMS1NaXNob3BSYW5rLXhpYW9taV9jYXRlZ29yeV9wYWdlLWV4cElkXzIzMzE0Ni1leHBJZF8zNDIxMTYtZXhwSWRfMzQyMzA4LWV4cElkXzI0OTI5NC1leHBJZF8xNjc5MjctZXhwSWRfMTY3OTE2LWV4cElkXzE4NTIxMS1leHBJZF8xNjE5ODItZXhwSWRfMTg1MjA1LWV4cElkXzMxMTQzOS1leHBJZF8zMzc4MzgtZXhwSWRfMzMyODY5LWV4cElkXzI0NDMxOC1leHBJZF8zMzc4NDQtZXhwSWRfMjQ2OTA2LWV4cElkXzI0NjIzOS1leHBJZF8xNjgwNTYtdHJhY2VJZF8wNTA1MjQ2ZDI5NmI0YzAzOTAyN2M0NTM4NjY2YmZlZQ==.0\u0026uni=8.1836\u0026next=6.64.2.11541",
              "market_price": "69",
              "name": "小米无线蓝牙双模鼠标 静音版",
              "price": "69",
              "product_desc": "\u003cfont color='#ff4a00'\u003e静音|蓝牙+2.4GHz接收器|可连两台电脑使用|侧按键设计\u003c/font\u003e手感设计，操作舒适|星光黑，石英白双色可选",
              "product_desc_origin": "手感设计，操作舒适|星光黑，石英白双色可选",
              "product_id": 11541,
              "puzzle_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/8a70effa8d88a991a49f821b89a0ace5.png?w=800\u0026h=800",
              "satisfy_per": "99.6%",
              "spm_stat": {
                  "scm": "server.0.0.0.product.1836.0.0",
                  "spm_code": "category_stmt.product.1",
                  "spm_params": "{\"product_id\":11541}"
              }
          },
          {
              "cat_id": 0,
              "comments_total": "238323",
              "is_multi_price": false,
              "is_stock": false,
              "log_code": "bpm=83.250.3881506.6\u0026cdm=0.1.MjAwMS1NaXNob3BSYW5rLXhpYW9taV9jYXRlZ29yeV9wYWdlLWV4cElkXzIzMzE0Ni1leHBJZF8zNDIxMTYtZXhwSWRfMzQyMzA4LWV4cElkXzI0OTI5NC1leHBJZF8xNjc5MjctZXhwSWRfMTY3OTE2LWV4cElkXzE4NTIxMS1leHBJZF8xNjE5ODItZXhwSWRfMTg1MjA1LWV4cElkXzMxMTQzOS1leHBJZF8zMzc4MzgtZXhwSWRfMzMyODY5LWV4cElkXzI0NDMxOC1leHBJZF8zMzc4NDQtZXhwSWRfMjQ2OTA2LWV4cElkXzI0NjIzOS1leHBJZF8xNjgwNTYtdHJhY2VJZF8wNTA1MjQ2ZDI5NmI0YzAzOTAyN2M0NTM4NjY2YmZlZQ==.0\u0026uni=8.1836\u0026next=6.64.2.11418",
              "market_price": "99",
              "name": "小米无线键鼠套装",
              "price": "99",
              "product_desc": "\u003cfont color='#ff4a00'\u003e2.4GHz+低电量提醒\u003c/font\u003e全尺寸104键 / 多功能快捷键 / 简洁轻薄",
              "product_desc_origin": "全尺寸104键 / 多功能快捷键 / 简洁轻薄",
              "product_id": 11418,
              "puzzle_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/74f15a7f5fe1f248b1553a1bd674480c.png?w=800\u0026h=800",
              "satisfy_per": "99.6%",
              "spm_stat": {
                  "scm": "server.0.0.0.product.1836.0.0",
                  "spm_code": "category_stmt.product.2",
                  "spm_params": "{\"product_id\":11418}"
              }
          },
          {
              "cat_id": 0,
              "comments_total": "26105",
              "is_multi_price": false,
              "is_stock": false,
              "log_code": "bpm=83.250.3881506.10\u0026cdm=0.1.MjAwMS1NaXNob3BSYW5rLXhpYW9taV9jYXRlZ29yeV9wYWdlLWV4cElkXzIzMzE0Ni1leHBJZF8zNDIxMTYtZXhwSWRfMzQyMzA4LWV4cElkXzI0OTI5NC1leHBJZF8xNjc5MjctZXhwSWRfMTY3OTE2LWV4cElkXzE4NTIxMS1leHBJZF8xNjE5ODItZXhwSWRfMTg1MjA1LWV4cElkXzMxMTQzOS1leHBJZF8zMzc4MzgtZXhwSWRfMzMyODY5LWV4cElkXzI0NDMxOC1leHBJZF8zMzc4NDQtZXhwSWRfMjQ2OTA2LWV4cElkXzI0NjIzOS1leHBJZF8xNjgwNTYtdHJhY2VJZF8wNTA1MjQ2ZDI5NmI0YzAzOTAyN2M0NTM4NjY2YmZlZQ==.0\u0026uni=8.1836\u0026next=6.64.2.15903",
              "market_price": "169",
              "name": "小米有线机械键盘",
              "price": "169",
              "product_desc": "小米有线机械键盘",
              "product_desc_origin": "小米有线机械键盘",
              "product_id": 15903,
              "puzzle_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/69ad5d61a8404194de3c59123ff08305.png?w=800\u0026h=800",
              "satisfy_per": "99.6%",
              "spm_stat": {
                  "scm": "server.0.0.0.product.1836.0.0",
                  "spm_code": "category_stmt.product.3",
                  "spm_params": "{\"product_id\":15903}"
              }
          },
          {
              "cat_id": 0,
              "comments_total": "284074",
              "is_multi_price": false,
              "is_stock": false,
              "log_code": "bpm=83.250.3881506.4\u0026cdm=0.1.MjAwMS1NaXNob3BSYW5rLXhpYW9taV9jYXRlZ29yeV9wYWdlLWV4cElkXzIzMzE0Ni1leHBJZF8zNDIxMTYtZXhwSWRfMzQyMzA4LWV4cElkXzI0OTI5NC1leHBJZF8xNjc5MjctZXhwSWRfMTY3OTE2LWV4cElkXzE4NTIxMS1leHBJZF8xNjE5ODItZXhwSWRfMTg1MjA1LWV4cElkXzMxMTQzOS1leHBJZF8zMzc4MzgtZXhwSWRfMzMyODY5LWV4cElkXzI0NDMxOC1leHBJZF8zMzc4NDQtZXhwSWRfMjQ2OTA2LWV4cElkXzI0NjIzOS1leHBJZF8xNjgwNTYtdHJhY2VJZF8wNTA1MjQ2ZDI5NmI0YzAzOTAyN2M0NTM4NjY2YmZlZQ==.0\u0026uni=8.1836\u0026next=6.64.2.11255",
              "market_price": "59",
              "name": "小米无线鼠标2",
              "price": "59",
              "product_desc": "\u003cfont color='#ff4a00'\u003e左右手对称设计|静音|2.4GHz接收器\u003c/font\u003eABS塑胶上盖/ABS塑胶中框/ABS塑胶下盖/咬花注塑\r\n铝合金滚轮/滚花+CNC切割",
              "product_desc_origin": "ABS塑胶上盖/ABS塑胶中框/ABS塑胶下盖/咬花注塑\r\n铝合金滚轮/滚花+CNC切割",
              "product_id": 11255,
              "puzzle_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/8c34a2b5213db34d378fafe9492bc670.png?w=800\u0026h=800",
              "satisfy_per": "99.8%",
              "spm_stat": {
                  "scm": "server.0.0.0.product.1836.0.0",
                  "spm_code": "category_stmt.product.4",
                  "spm_params": "{\"product_id\":11255}"
              }
          },
          {
              "cat_id": 0,
              "comments_total": "34822",
              "is_multi_price": false,
              "is_stock": false,
              "log_code": "bpm=83.250.3881506.11\u0026cdm=0.1.MjAwMS1NaXNob3BSYW5rLXhpYW9taV9jYXRlZ29yeV9wYWdlLWV4cElkXzIzMzE0Ni1leHBJZF8zNDIxMTYtZXhwSWRfMzQyMzA4LWV4cElkXzI0OTI5NC1leHBJZF8xNjc5MjctZXhwSWRfMTY3OTE2LWV4cElkXzE4NTIxMS1leHBJZF8xNjE5ODItZXhwSWRfMTg1MjA1LWV4cElkXzMxMTQzOS1leHBJZF8zMzc4MzgtZXhwSWRfMzMyODY5LWV4cElkXzI0NDMxOC1leHBJZF8zMzc4NDQtZXhwSWRfMjQ2OTA2LWV4cElkXzI0NjIzOS1leHBJZF8xNjgwNTYtdHJhY2VJZF8wNTA1MjQ2ZDI5NmI0YzAzOTAyN2M0NTM4NjY2YmZlZQ==.0\u0026uni=8.1836\u0026next=6.64.2.16373",
              "market_price": "39",
              "name": "小米无线鼠标Lite 2",
              "price": "39",
              "product_desc": "贴合手部弧度，缓解手部压力，2.4GHz无线传输，桌面清爽不缠绕，轻量灵动，减轻负担，定位迅速",
              "product_desc_origin": "贴合手部弧度，缓解手部压力，2.4GHz无线传输，桌面清爽不缠绕，轻量灵动，减轻负担，定位迅速",
              "product_id": 16373,
              "puzzle_url": "https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202208101409_51dc5fdb96eda4a6cc87d80f80f65d24.png?w=800\u0026h=800",
              "satisfy_per": "100.0%",
              "spm_stat": {
                  "scm": "server.0.0.0.product.1836.0.0",
                  "spm_code": "category_stmt.product.5",
                  "spm_params": "{\"product_id\":16373}"
              }
          },
          {
              "cat_id": 0,
              "comments_total": "82649",
              "is_multi_price": false,
              "is_stock": false,
              "log_code": "bpm=83.250.3881506.9\u0026cdm=0.1.MjAwMS1NaXNob3BSYW5rLXhpYW9taV9jYXRlZ29yeV9wYWdlLWV4cElkXzIzMzE0Ni1leHBJZF8zNDIxMTYtZXhwSWRfMzQyMzA4LWV4cElkXzI0OTI5NC1leHBJZF8xNjc5MjctZXhwSWRfMTY3OTE2LWV4cElkXzE4NTIxMS1leHBJZF8xNjE5ODItZXhwSWRfMTg1MjA1LWV4cElkXzMxMTQzOS1leHBJZF8zMzc4MzgtZXhwSWRfMzMyODY5LWV4cElkXzI0NDMxOC1leHBJZF8zMzc4NDQtZXhwSWRfMjQ2OTA2LWV4cElkXzI0NjIzOS1leHBJZF8xNjgwNTYtdHJhY2VJZF8wNTA1MjQ2ZDI5NmI0YzAzOTAyN2M0NTM4NjY2YmZlZQ==.0\u0026uni=8.1836\u0026next=6.64.2.13371",
              "market_price": "49",
              "name": "小米超大双料鼠标垫",
              "price": "49",
              "product_desc": "超大尺寸 |皮革触感|天然橡木 |防水耐脏",
              "product_desc_origin": "超大尺寸 |皮革触感|天然橡木 |防水耐脏",
              "product_id": 13371,
              "puzzle_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/b2f5f43d0f10eb72fb74a91c3d9873ea.png?w=800\u0026h=800",
              "satisfy_per": "99.9%",
              "spm_stat": {
                  "scm": "server.0.0.0.product.1836.0.0",
                  "spm_code": "category_stmt.product.6",
                  "spm_params": "{\"product_id\":13371}"
              }
          },
          {
              "cat_id": 0,
              "comments_total": "111586",
              "is_multi_price": false,
              "is_stock": false,
              "log_code": "bpm=83.250.3881506.2\u0026cdm=0.1.MjAwMS1NaXNob3BSYW5rLXhpYW9taV9jYXRlZ29yeV9wYWdlLWV4cElkXzIzMzE0Ni1leHBJZF8zNDIxMTYtZXhwSWRfMzQyMzA4LWV4cElkXzI0OTI5NC1leHBJZF8xNjc5MjctZXhwSWRfMTY3OTE2LWV4cElkXzE4NTIxMS1leHBJZF8xNjE5ODItZXhwSWRfMTg1MjA1LWV4cElkXzMxMTQzOS1leHBJZF8zMzc4MzgtZXhwSWRfMzMyODY5LWV4cElkXzI0NDMxOC1leHBJZF8zMzc4NDQtZXhwSWRfMjQ2OTA2LWV4cElkXzI0NjIzOS1leHBJZF8xNjgwNTYtdHJhY2VJZF8wNTA1MjQ2ZDI5NmI0YzAzOTAyN2M0NTM4NjY2YmZlZQ==.0\u0026uni=8.1836\u0026next=6.64.2.13330",
              "market_price": "99",
              "name": "小米便携鼠标2",
              "price": "99",
              "product_desc": "\u003cfont color='#ff4a00'\u003e低电量提醒|4挡灵敏度调节|蓝牙+2.4GHz接收器|可连两台电脑使用\u003c/font\u003e1节5号电池，可正常使用1年| 圆润小巧，出门便携|软胶阻尼滚轮|金属质感",
              "product_desc_origin": "1节5号电池，可正常使用1年| 圆润小巧，出门便携|软胶阻尼滚轮|金属质感",
              "product_id": 13330,
              "puzzle_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/8a25d248ca68c9060240bc679f0b170a.png?w=800\u0026h=800",
              "satisfy_per": "99.9%",
              "spm_stat": {
                  "scm": "server.0.0.0.product.1836.0.0",
                  "spm_code": "category_stmt.product.7",
                  "spm_params": "{\"product_id\":13330}"
              }
          },
          {
              "cat_id": 0,
              "comments_total": "85668",
              "is_multi_price": false,
              "is_stock": false,
              "log_code": "bpm=83.250.3881506.5\u0026cdm=0.1.MjAwMS1NaXNob3BSYW5rLXhpYW9taV9jYXRlZ29yeV9wYWdlLWV4cElkXzIzMzE0Ni1leHBJZF8zNDIxMTYtZXhwSWRfMzQyMzA4LWV4cElkXzI0OTI5NC1leHBJZF8xNjc5MjctZXhwSWRfMTY3OTE2LWV4cElkXzE4NTIxMS1leHBJZF8xNjE5ODItZXhwSWRfMTg1MjA1LWV4cElkXzMxMTQzOS1leHBJZF8zMzc4MzgtZXhwSWRfMzMyODY5LWV4cElkXzI0NDMxOC1leHBJZF8zMzc4NDQtZXhwSWRfMjQ2OTA2LWV4cElkXzI0NjIzOS1leHBJZF8xNjgwNTYtdHJhY2VJZF8wNTA1MjQ2ZDI5NmI0YzAzOTAyN2M0NTM4NjY2YmZlZQ==.0\u0026uni=8.1836\u0026next=6.64.2.11872",
              "market_price": "99",
              "name": "小米时尚鼠标",
              "price": "99",
              "product_desc": "\u003cfont color='#ff4a00'\u003e静音|蓝牙+2.4GHz接收器|可连两台电脑使用\u003c/font\u003e金属材质外观|时尚纤薄便携|高精度传感器|铝合金CNC滚轮",
              "product_desc_origin": "金属材质外观|时尚纤薄便携|高精度传感器|铝合金CNC滚轮",
              "product_id": 11872,
              "puzzle_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/4d7885b7e3c6dd6015556003057f5e56.png?w=800\u0026h=800",
              "satisfy_per": "99.7%",
              "spm_stat": {
                  "scm": "server.0.0.0.product.1836.0.0",
                  "spm_code": "category_stmt.product.8",
                  "spm_params": "{\"product_id\":11872}"
              }
          },
          {
              "cat_id": 0,
              "comments_total": "127703",
              "is_multi_price": false,
              "is_stock": false,
              "log_code": "bpm=83.250.3881506.1\u0026cdm=0.1.MjAwMS1NaXNob3BSYW5rLXhpYW9taV9jYXRlZ29yeV9wYWdlLWV4cElkXzIzMzE0Ni1leHBJZF8zNDIxMTYtZXhwSWRfMzQyMzA4LWV4cElkXzI0OTI5NC1leHBJZF8xNjc5MjctZXhwSWRfMTY3OTE2LWV4cElkXzE4NTIxMS1leHBJZF8xNjE5ODItZXhwSWRfMTg1MjA1LWV4cElkXzMxMTQzOS1leHBJZF8zMzc4MzgtZXhwSWRfMzMyODY5LWV4cElkXzI0NDMxOC1leHBJZF8zMzc4NDQtZXhwSWRfMjQ2OTA2LWV4cElkXzI0NjIzOS1leHBJZF8xNjgwNTYtdHJhY2VJZF8wNTA1MjQ2ZDI5NmI0YzAzOTAyN2M0NTM4NjY2YmZlZQ==.0\u0026uni=8.1836\u0026next=6.64.2.14338",
              "market_price": "49",
              "name": "定制软木鼠标垫",
              "price": "49",
              "product_desc": "天然软木 | 四季恒温 | 舒适触感 | 轻便收纳",
              "product_desc_origin": "天然软木 | 四季恒温 | 舒适触感 | 轻便收纳",
              "product_id": 14338,
              "puzzle_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/01ca26914f09e96ef017b1cd0d36d6d5.png?w=800\u0026h=800",
              "satisfy_per": "99.9%",
              "spm_stat": {
                  "scm": "server.0.0.0.product.1836.0.0",
                  "spm_code": "category_stmt.product.9",
                  "spm_params": "{\"product_id\":14338}"
              }
          },
          {
              "cat_id": 0,
              "comments_total": "67371",
              "is_multi_price": false,
              "is_stock": false,
              "log_code": "bpm=83.250.3881506.3\u0026cdm=0.1.MjAwMS1NaXNob3BSYW5rLXhpYW9taV9jYXRlZ29yeV9wYWdlLWV4cElkXzIzMzE0Ni1leHBJZF8zNDIxMTYtZXhwSWRfMzQyMzA4LWV4cElkXzI0OTI5NC1leHBJZF8xNjc5MjctZXhwSWRfMTY3OTE2LWV4cElkXzE4NTIxMS1leHBJZF8xNjE5ODItZXhwSWRfMTg1MjA1LWV4cElkXzMxMTQzOS1leHBJZF8zMzc4MzgtZXhwSWRfMzMyODY5LWV4cElkXzI0NDMxOC1leHBJZF8zMzc4NDQtZXhwSWRfMjQ2OTA2LWV4cElkXzI0NjIzOS1leHBJZF8xNjgwNTYtdHJhY2VJZF8wNTA1MjQ2ZDI5NmI0YzAzOTAyN2M0NTM4NjY2YmZlZQ==.0\u0026uni=8.1836\u0026next=6.64.2.12391",
              "market_price": "149",
              "name": "小米小爱鼠标",
              "price": "149",
              "product_desc": "\u003cfont color='#ff4a00'\u003e内置小爱同学|type-c充电|语音输入打字|支持按键自定义|智能家居控制|蓝牙+2.4GHz接收器|可连两台电脑使用\u003c/font\u003e语音输入文字  / 智能控制 / 4000DPI / Type-C 充电",
              "product_desc_origin": "语音输入文字  / 智能控制 / 4000DPI / Type-C 充电",
              "product_id": 12391,
              "puzzle_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/dbc00e7c58b2f330d35f85cb5cfb386f.png?w=800\u0026h=800",
              "satisfy_per": "99.6%",
              "spm_stat": {
                  "scm": "server.0.0.0.product.1836.0.0",
                  "spm_code": "category_stmt.product.10",
                  "spm_params": "{\"product_id\":12391}"
              }
          },
          {
              "cat_id": 0,
              "comments_total": "4169",
              "is_multi_price": false,
              "is_stock": false,
              "log_code": "bpm=83.250.3881506.7\u0026cdm=0.1.MjAwMS1NaXNob3BSYW5rLXhpYW9taV9jYXRlZ29yeV9wYWdlLWV4cElkXzIzMzE0Ni1leHBJZF8zNDIxMTYtZXhwSWRfMzQyMzA4LWV4cElkXzI0OTI5NC1leHBJZF8xNjc5MjctZXhwSWRfMTY3OTE2LWV4cElkXzE4NTIxMS1leHBJZF8xNjE5ODItZXhwSWRfMTg1MjA1LWV4cElkXzMxMTQzOS1leHBJZF8zMzc4MzgtZXhwSWRfMzMyODY5LWV4cElkXzI0NDMxOC1leHBJZF8zMzc4NDQtZXhwSWRfMjQ2OTA2LWV4cElkXzI0NjIzOS1leHBJZF8xNjgwNTYtdHJhY2VJZF8wNTA1MjQ2ZDI5NmI0YzAzOTAyN2M0NTM4NjY2YmZlZQ==.0\u0026uni=8.1836\u0026next=6.64.2.7865",
              "market_price": "599",
              "name": "悦米机械键盘Pro静音版",
              "price": "599",
              "product_desc": "CNC全铝机身，精致由内到外 ／ TTC静音红轴，享受静谧好手感 ／ 细腻触感，经久耐用",
              "product_desc_origin": "CNC全铝机身，精致由内到外 ／ TTC静音红轴，享受静谧好手感 ／ 细腻触感，经久耐用",
              "product_id": 7865,
              "puzzle_url": "https://cdn.cnbj0.fds.api.mi-img.com/b2c-mimall-media/47e3ed27453cd94f2e4596773ac8af5f.png?w=800\u0026h=800",
              "satisfy_per": "99.5%",
              "spm_stat": {
                  "scm": "server.0.0.0.product.1836.0.0",
                  "spm_code": "category_stmt.product.11",
                  "spm_params": "{\"product_id\":7865}"
              }
          }
      ],
      "top_tab": [
          {
              "category_id": 972,
              "img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/e8c74026176502da03d220b2656896f9.png",
              "log_code": "bpm=83.250.3881507.1\u0026uni=8.1836\u0026next=83.250.8.972",
              "name": "打印机",
              "selected": false,
              "spm_stat": {
                  "scm": "server.0.0.0.category.0.0.0",
                  "spm_code": "category_stmt.tab.1",
                  "spm_params": "{\"sales_id\":972}"
              }
          },
          {
              "category_id": 2181,
              "img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f58a2bf3f5e838d5f3e505f086794725.png",
              "log_code": "bpm=83.250.3881507.2\u0026uni=8.1836\u0026next=83.250.8.2181",
              "name": "电纸书",
              "selected": false,
              "spm_stat": {
                  "scm": "server.0.0.0.category.0.0.0",
                  "spm_code": "category_stmt.tab.2",
                  "spm_params": "{\"sales_id\":2181}"
              }
          },
          {
              "category_id": 1836,
              "img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d8f12c0b7ed25711e678c38117a2e3c3.png",
              "log_code": "bpm=83.250.3881507.3\u0026uni=8.1836\u0026next=83.250.8.1836",
              "name": "键鼠",
              "selected": true,
              "spm_stat": {
                  "scm": "server.0.0.0.category.0.0.0",
                  "spm_code": "category_stmt.tab.3",
                  "spm_params": "{\"sales_id\":1836}"
              }
          },
          {
              "category_id": 1940,
              "img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/2cedf3416be1527fc4d78f6121aa621e.png",
              "log_code": "bpm=83.250.3881507.4\u0026uni=8.1836\u0026next=83.250.8.1940",
              "name": "充电器",
              "selected": false,
              "spm_stat": {
                  "scm": "server.0.0.0.category.0.0.0",
                  "spm_code": "category_stmt.tab.4",
                  "spm_params": "{\"sales_id\":1940}"
              }
          },
          {
              "category_id": 2241,
              "img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/6b62ecd4e1ba99291850602876576727.png",
              "log_code": "bpm=83.250.3881507.5\u0026uni=8.1836\u0026next=83.250.8.2241",
              "name": "会议宝mini",
              "selected": false,
              "spm_stat": {
                  "scm": "server.0.0.0.category.0.0.0",
                  "spm_code": "category_stmt.tab.5",
                  "spm_params": "{\"sales_id\":2241}"
              }
          },
          {
              "category_id": 2091,
              "img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/084f90aed97055d5632d3cd03e142cd7.png",
              "log_code": "bpm=83.250.3881507.6\u0026uni=8.1836\u0026next=83.250.8.2091",
              "name": "显示器挂灯",
              "selected": false,
              "spm_stat": {
                  "scm": "server.0.0.0.category.0.0.0",
                  "spm_code": "category_stmt.tab.6",
                  "spm_params": "{\"sales_id\":2091}"
              }
          },
          {
              "category_id": 1911,
              "img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/573a1dac741d55462e6a5058cfff1346.png",
              "log_code": "bpm=83.250.3881507.7\u0026uni=8.1836\u0026next=83.250.8.1911",
              "name": "平板保护壳",
              "selected": false,
              "spm_stat": {
                  "scm": "server.0.0.0.category.0.0.0",
                  "spm_code": "category_stmt.tab.7",
                  "spm_params": "{\"sales_id\":1911}"
              }
          },
          {
              "category_id": 1912,
              "img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/8acc30ee5d87fa150a340f1d837571e8.png",
              "log_code": "bpm=83.250.3881507.8\u0026uni=8.1836\u0026next=83.250.8.1912",
              "name": "平板钢化膜",
              "selected": false,
              "spm_stat": {
                  "scm": "server.0.0.0.category.0.0.0",
                  "spm_code": "category_stmt.tab.8",
                  "spm_params": "{\"sales_id\":1912}"
              }
          },
          {
              "category_id": 1812,
              "img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/043aced1232e3ed6a67a2c8993e59933.jpg",
              "log_code": "bpm=83.250.3881507.9\u0026uni=8.1836\u0026next=83.250.8.1812",
              "name": "笔记本贴纸",
              "selected": false,
              "spm_stat": {
                  "scm": "server.0.0.0.category.0.0.0",
                  "spm_code": "category_stmt.tab.9",
                  "spm_params": "{\"sales_id\":1812}"
              }
          },
          {
              "category_id": 2006,
              "img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/0e75dafd5fa502e8f1bd93e7a96e6cc2.png",
              "log_code": "bpm=83.250.3881507.10\u0026uni=8.1836\u0026next=83.250.8.2006",
              "name": "存储卡 U盘",
              "selected": false,
              "spm_stat": {
                  "scm": "server.0.0.0.category.0.0.0",
                  "spm_code": "category_stmt.tab.10",
                  "spm_params": "{\"sales_id\":2006}"
              }
          },
          {
              "category_id": 1995,
              "img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/34085f99323ffd7c4fbdf271655cc5cb.png",
              "log_code": "bpm=83.250.3881507.11\u0026uni=8.1836\u0026next=83.250.8.1995",
              "name": "触控笔 笔尖",
              "selected": false,
              "spm_stat": {
                  "scm": "server.0.0.0.category.0.0.0",
                  "spm_code": "category_stmt.tab.11",
                  "spm_params": "{\"sales_id\":1995}"
              }
          },
          {
              "category_id": 2041,
              "img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/db49c45805273657b9f76363247be162.png",
              "log_code": "bpm=83.250.3881507.12\u0026uni=8.1836\u0026next=83.250.8.2041",
              "name": "防窥保护膜",
              "selected": false,
              "spm_stat": {
                  "scm": "server.0.0.0.category.0.0.0",
                  "spm_code": "category_stmt.tab.12",
                  "spm_params": "{\"sales_id\":2041}"
              }
          },
          {
              "category_id": 1837,
              "img_url": "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f98a9c4d3e7119febc886986cf8ed3cb.png",
              "log_code": "bpm=83.250.3881507.13\u0026uni=8.1836\u0026next=83.250.8.1837",
              "name": "其他配件",
              "selected": false,
              "spm_stat": {
                  "scm": "server.0.0.0.category.0.0.0",
                  "spm_code": "category_stmt.tab.13",
                  "spm_params": "{\"sales_id\":1837}"
              }
          }
      ]
  },
  "result": "ok"
  }
}