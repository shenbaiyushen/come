module.exports = function HTTP(objAll){
    let {url,...obj} = objAll;
    return new Promise(function(resolve,reject){
      wx.request({
        url: 'https://apis.netstart.cn/xmsc/'+url,
        ...obj,
        // method:"get",
        success(data){ //成功回调
          resolve(data.data);
        },
        fail(err){ //失败回调
          reject(err);
        },
        complete(){ //接口调用结束，回调函数

        }
      })
    })
}