// pages/product/product.js

let {
  data:DATA
} = require("../../api/top_json")

let {
  getProduct
} = require("../../api/product")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    itemIndex:0,
    toptablist:null,
    productlist:null
  },


  // 切换效果
  setTopItem(event) {
    console.log("eventindex",event.currentTarget.dataset.indexs);
    let index = event.currentTarget.dataset.indexs;
    let ids = event.currentTarget.dataset.ids;
    console.log("eventids",event.currentTarget.dataset.ids);
    this.setData({
      itemIndex: index
    });
    getProduct(ids).then(data=>{
      this.setData({
        toptablist:data.data.top_tab,
        productlist:data.data.product
      })
    }).catch(()=>{
      // 请求失败切换静态数据
      this.setData({
        toptablist:DATA.data.top_tab,
        productlist:DATA.data.product,
        itemIndex:2
      })
    })

  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log("options",options);

    let _this = this;//将this传递给变量
    wx.getStorage({
      key: 'runindex',
      success (res) {
        console.log("res",res.data);
        _this.setData({
          itemIndex:res.data
        })
      }
    })

    getProduct(options.id).then(data=>{
      console.log(data.data);
      this.setData({
        toptablist:data.data.top_tab,
        productlist:data.data.product
      })
      // console.log("toptablist",this.data.toptablist);
    }).catch(()=>{
      // 请求失败切换静态数据
      this.setData({
        toptablist:DATA.data.top_tab,
        productlist:DATA.data.product,
        itemIndex:2
      })
    })
 
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})