// pages/pay/pay.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    total:0,
    goodsList:[
      // {
      //   id:1,
      //   name:"小米手机",
      //   img:"https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d9d7f31258a1f84a277918cecab14aaa.jpg?f=webp&w=516&h=420",
      //   price:"2999",
      //   priceOld:"3999",
      //   num:1,
      //   checked:false
      // },
      // {
      //   id:2,
      //   name:"小米手机2",
      //   img:"https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d9d7f31258a1f84a277918cecab14aaa.jpg?f=webp&w=516&h=420",
      //   price:"2999",
      //   priceOld:"3999",
      //   num:1,
      //   checked:false
      // },
    ],
    flag:false,//添加地址时的默认选项
    showflag:false,//显示添加地址
    determine_flag:false,//显示地址页面
    name:'',//姓名
    emid:'',//手机号
    ress:'',//收货地址
    didhs:'',//详细地址
    addressList:[],//地址数据
    modify_flag:false,//点击修改出现修改界面
    modify_index:0,//修改数据的下标
    
    add_index:0,//默认选框
  },
  //输入框触发
  addFun(e){
    console.log(e.target.dataset.name,'对应值');
    console.log(e.detail.value,'值');
    this.setData({
      [e.target.dataset.name] : e.detail.value
    })
  },

  //切换选框
  activeFun(e){
    let loacl_list = wx.getStorageSync('aress')
    loacl_list.forEach((item,index)=>{
      if (index == e.target.dataset.id) {
        console.log(index);
        item.aess = true
      }else{
        item.aess = false
      }
    })
    wx.setStorageSync('aress', loacl_list)
    this.getlistFun()
  },
  //切换默认地址
  defaultFun(e){
    let loacl_list = wx.getStorageSync('aress')
    loacl_list.forEach((item,index)=>{
      if (index == e.target.dataset.id) {
        item.flag = true
      }else{
        item.flag = false
      }
    })
        this.data.addressList = loacl_list
        this.setData(this.data)
   wx.setStorageSync('aress', loacl_list)
  },

    //设置默认地址
    morenFun(){
      this.data.flag = !this.data.flag
      this.setData(this.data)
    },

    //添加新增地址
    setadd(){
      if (this.data.name != ''&&this.data.emid != ''&&this.data.didhs != '') {
      this.data.showflag = ! this.data.showflag
      this.data.modify_flag = false
      let list = []
      list.push({
        name:this.data.name,
        emid:this.data.emid,
        ress:this.data.ress,
        didhs:this.data.didhs,
        flag:this.data.flag,
        aess:false,
      })
      let loacl_list = wx.getStorageSync('aress')
        if (loacl_list && loacl_list != null) {
          if (this.data.flag == true) {
            loacl_list.forEach(item=>{
              item.flag == false
            })
          }
          loacl_list.push(list[0])
          this.data.addressList = loacl_list
          this.setData(this.data)
            wx.setStorageSync('aress', loacl_list)
          }else{
            wx.setStorageSync('aress',[list[0]])
          }
          // 清空数据
      this.setData({
        name:"",
        emid:"",
        ress:"",
        didhs:"",
        flag:""
      })
    }else{
      wx.showToast({
        title: '请输你的收货信息',
        icon:"none",
        duration: 2000
      })
    }
    },


  //修改地址显示
  setlist_Fun(e){
    this.data.modify_flag = true
    this.data.showflag = true
    this.data.modify_index = e.target.dataset.id
    this.data.flag = this.data.addressList[e.target.dataset.id].flag
    this.data.emid = this.data.addressList[e.target.dataset.id].emid
    this.data.ress = this.data.addressList[e.target.dataset.id].ress
    this.data.didhs = this.data.addressList[e.target.dataset.id].didhs
    this.data.name = this.data.addressList[e.target.dataset.id].name
    this.setData(this.data)
  },
    // 确认修改地址
    setaddFun(){
      if (this.data.name != ''&&this.data.emid != ''&&this.data.didhs != '') {
      this.data.showflag = ! this.data.showflag
      this.data.modify_flag = false
      let loacl_list = wx.getStorageSync('aress')
      loacl_list[this.data.modify_index].flag = this.data.flag
      loacl_list[this.data.modify_index].emid = this.data.emid
      loacl_list[this.data.modify_index].ress = this.data.ress
      loacl_list[this.data.modify_index].didhs = this.data.didhs
      loacl_list[this.data.modify_index].name = this.data.name
          this.data.addressList = loacl_list
          this.setData(this.data)
     wx.setStorageSync('aress', loacl_list)
          // 清空数据
      this.setData({
        name:"",
        emid:"",
        ress:"",
        didhs:"",
        flag:false
      })
    }else{
      wx.showToast({
        title: '请输你的收货信息',
        icon:"none",
        duration: 2000
      })
    }


    },
    

  
  //删除地址
  dellist_Fun(e){
    // console.log(e.target.dataset.id,"删除");
    this.data.addressList.splice(e.target.dataset.id,1)
    this.setData(this.data)
    let loacl_list = wx.getStorageSync('aress')
    loacl_list.splice(e.target.dataset.id,1)
    wx.setStorageSync('aress', loacl_list)
  },



  //显示地址栏
  determineFun(){
    this.data.determine_flag = true;
    this.setData(this.data)
  },

  //关闭隐藏地址栏
  deterFun(){
    wx.navigateTo({
      url: '/pages/pay/pay',
    })
  },

  //隐藏添加地址
  setflag(){
    this.data.showflag = false
    this.setData(this.data)
  },

//显示添加地址
  showadd(){
    this.data.showflag = !this.data.showflag 
    this.setData(this.data)
  },





  // 计算总价格
  totalData(){
    let num = 0;
    let data = this.data.goodsList.reduce((total,item)=>{
      if(item.checked){
        num++;
        return total += item.price * item.num;
      }else{
        return total
      }
    },0)
    this.setData({
      total:data,
    })
  },
  






  // 跳转订单页
  goOrder(){
    wx.showModal({
      title: '支付商品',
      success:(res)=>{
        if (res.confirm) {
          // console.log('用户点击确定')
          this.setOrderList(2);//生成订单--待发货
        } else if (res.cancel) {
          // console.log('用户点击取消')
          this.setOrderList(1);//生成订单--待支付
        }
        
        wx.navigateTo({
          url: '/pages/order/order',
        })
      }
    })
    


  },

  // 生成订单
  // 1.待支付 2.待发货 3.待收货 4.待评价
  setOrderList(type){
      
      // 构造订单数据
      let orderData = {
        orderId: new Date().getTime(),
        orderLists:this.data.goodsList,//商品列表
        type:type,
        total:this.data.total,//总价格
        address:{
            name:"张三",
            phone:"13012341234",
            address:"广东省广州市天河区XXX路XXX号"
        }
      };

      // 1.判断缓存有没有数据
      let goodsOrderList = wx.getStorageSync('goodsOrderList');
      if(goodsOrderList){
        // 2.有数据 添加数据
        goodsOrderList.unshift(orderData);
        wx.setStorageSync('goodsOrderList', goodsOrderList)

      }else{
        // 3.没有数据 添加订单
        wx.setStorageSync('goodsOrderList', [orderData])
      }


      // 删除订单数据
      let goodsCarList = wx.getStorageSync('goodsCarList');
      let newlists = goodsCarList.filter(item=>!item.checked); //过滤加入订单数据
      wx.setStorageSync('goodsCarList', newlists)

  },

  // 初始化数据
  getlistFun(){
    if (wx.getStorageSync('aress') && wx.getStorageSync('aress') != null) {
      this.data.addressList =  wx.getStorageSync('aress')
      this.data.addressList.forEach((item,index)=>{
        if (item.flag == true) {
          console.log(index,'初始化下标');
          this.data.add_index = index
          this.setData(this.data)
        }
      })
    }else{
      this.data.addressList
    }
    console.log(this.data.addressList,'收货地址初始化数据');
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getlistFun()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // 获取缓存数据
    let goodsCarList = wx.getStorageSync('goodsCarList');
    let dataLists = goodsCarList.filter(item=>item.checked);
    this.setData({
      goodsList:dataLists
    })

    this.totalData();//计算总价格
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})