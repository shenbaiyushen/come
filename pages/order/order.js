// pages/order/order.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderLists: null,
    index: 0,
    evaluate:false
  },
  // 修改下标
  setIndex(event) {
    let index = event.target.dataset.index;
    this.setData({
      index: index
    })
  },

  // 前往支付
  PassRay(event) {
    console.log("PassRay", event.target.dataset.orderid);

    // 获取特定id
    let PassRay = event.target.dataset.orderid;

    // 拉取数据
    let lists = wx.getStorageSync('goodsOrderList');

    // 获取对应下标
    let i = lists.findIndex(item => item.orderId == PassRay)
    console.log(i);

    wx.showModal({
      title: '支付商品',
      success: (res) => {
        // console.log(res);
        if (res.confirm) {
          // console.log('用户点击确定')
          lists[i].type = 2;
          console.log(lists);
          wx.setStorageSync('goodsOrderList', lists)
          this.setData({
            orderLists: lists
          })
          this.setData({
            index : 2
          }) 
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })

  },

  // 催发货
  PassShipments(event) {

    console.log("PassShipments", event.target.dataset.orderid);

    // 获取特定id
    let PassRay = event.target.dataset.orderid;

    // 拉取数据
    let lists = wx.getStorageSync('goodsOrderList');

    // 获取对应下标
    let i = lists.findIndex(item => item.orderId == PassRay)
    console.log(i);

    wx.showModal({
      title: '是否提醒仓库发货',
      success: (res) => {
        // console.log(res);
        if (res.confirm) {
          // console.log('用户点击确定')
          lists[i].type = 3;
          console.log(lists);
          wx.setStorageSync('goodsOrderList', lists)
          setTimeout(() => {
            this.setData({
              orderLists: lists
            })
          }, 2000)
          this.setData({
            index : 3
          })      
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })


    // lists[i].type=2;
    // console.log(lists);
    // wx.setStorageSync('goodsOrderList',lists)





  },

  // 确认收货
  AffirmShipments(event) {

    console.log("AffirmShipments", event.target.dataset.orderid);

    // 获取特定id
    let PassRay = event.target.dataset.orderid;

    // 拉取数据
    let lists = wx.getStorageSync('goodsOrderList');

    // 获取对应下标
    let i = lists.findIndex(item => item.orderId == PassRay)
    console.log(i);

    wx.showModal({
      title: '是否确定收货',
      success: (res) => {
        // console.log(res);
        if (res.confirm) {
          // console.log('用户点击确定')
          lists[i].type = 4;
          console.log(lists);
          wx.setStorageSync('goodsOrderList', lists)
          setTimeout(() => {
            this.setData({
              orderLists: lists
            })
          }, 2000)
          this.setData({
            index : 4
          }) 
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })

  },

  // 交易完成
  PassDeal(event) {


    // 获取特定id
    let PassRay = event.target.dataset.orderid;

    // 拉取数据
    let lists = wx.getStorageSync('goodsOrderList');

    // 获取对应下标
    let i = lists.findIndex(item => item.orderId == PassRay)

    if(lists[i].evaluate){
      return 0
    }
    
    console.log("PassDeal", event.target.dataset.orderid);

    wx.showModal({
      title: '是否评论',
      success: (res) => {
        // console.log(res);
        if (res.confirm) {
          // console.log('用户点击确定')
          lists[i].evaluate = true;
          console.log(lists);
          wx.setStorageSync('goodsOrderList', lists)
          this.setData({
            orderLists: lists
          })

        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },




  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    let orderLists = wx.getStorageSync('goodsOrderList');
    this.setData({
      orderLists: orderLists
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // 关闭所有页面，打开到应用内的某个页面
    // wx.reLaunch({
    //   url: '/pages/my/my',
    // })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})