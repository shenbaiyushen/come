// pages/car/car.js
let {recommendBlank} = require("../../api/home")
Page({

  /**
   * 页面的初始数据
   */
  data: {
      statusX:0,
      slide:null,
      toggleChecked:false,
      total:0,
      goodsNum:0,
      goodsList:[
        // {
        //   id:1,
        //   name:"小米手机",
        //   img:"https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d9d7f31258a1f84a277918cecab14aaa.jpg?f=webp&w=516&h=420",
        //   price:"2999",
        //   priceOld:"3999",
        //   num:1,
        //   checked:false
        // },
        // {
        //   id:2,
        //   name:"小米手机2",
        //   img:"https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d9d7f31258a1f84a277918cecab14aaa.jpg?f=webp&w=516&h=420",
        //   price:"2999",
        //   priceOld:"3999",
        //   num:1,
        //   checked:false
        // },
        // {
        //   id:3,
        //   name:"小米手机3",
        //   img:"https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d9d7f31258a1f84a277918cecab14aaa.jpg?f=webp&w=516&h=420",
        //   price:"2999",
        //   priceOld:"3999",
        //   num:1,
        //   checked:false
        // },
      ],
      lists:[],//商品推荐
  },
  // 跳转页面
  goPay(){
      // 判断是否登录
      let login = wx.getStorageSync('userInfo');
      if(!login){
        wx.showToast({
          title: '登录后操作!',
          icon:"none",
          duration: 2000
        })
        setTimeout(()=>{
          wx.switchTab({
            url: '/pages/my/my',
          })
        },2000)
        return;
      }


      // 判断是否有数据
      let dataLength = this.data.goodsList.filter(item=>item.checked);
      console.log(dataLength.length);
      if(dataLength.length>=1){//加入购物车数量
          wx.navigateTo({
            url: '/pages/pay/pay',
          })
      }else{
        wx.showToast({
          title: '请选择商品。',
          icon:"none",
          duration: 2000
        })
      }
  },

  // 滑动效果
  touchstart(e){
    // console.log(e);
    // console.log(this.data.statusX);
    this.data.statusX = e.changedTouches[0].clientX;//开始位置
  },
  touchend(e){
    console.log(e.target.dataset.id);
    let clientX = e.changedTouches[0].clientX;//结束位置
    if(this.data.statusX - clientX >0){
      console.log("触发滑动效果");
      this.setData({
        slide:e.target.dataset.id
      })
    }else{
      console.log("不触发滑动效果");
      this.setData({
        slide:null
      })
    }
  },
  // 删除数据
  del(e){
    let id = e.target.dataset.id;
    let index = this.data.goodsList.findIndex(item =>item.id == id);//下标
    this.data.goodsList.splice(index,1);//删除静态数据，不会更新页面
    this.setData(this.data);
    this.totalData();//计算总价格
  },
  // 数量加一
  add(e){
    let id = e.target.dataset.id;
    let index = this.data.goodsList.findIndex(item =>item.id == id);//下标
    // 第一种写法
    // let num = this.data.goodsList[index].num + 1;
    // this.data.goodsList[index].num = num;//修改数据
    // this.setData(this.data);
    // 第二种写法
    this.setData({
      ["goodsList["+index+"].num"]:this.data.goodsList[index].num + 1,
      slide:null
    })
    this.totalData();//计算总价格
  },
  // 数量减一
  sub(e){
    let id = e.target.dataset.id;
    let index = this.data.goodsList.findIndex(item =>item.id == id);//下标
    let num = this.data.goodsList[index].num-1;
    this.setData({
      ["goodsList["+index+"].num"]: (num < 1? 1:num),
      slide:null
    })
    this.totalData();//计算总价格
  },
  // 单选功能
  checkboxClick(e){
    let id = e.target.dataset.id;
    let index = this.data.goodsList.findIndex(item =>item.id == id);//下标
    this.setData({
      ["goodsList["+index+"].checked"]: !this.data.goodsList[index].checked,
      slide:null
    })
    this.totalData();//计算总价格

    // 重新计算全框
    this.idToggleBox();

  },
  // 全选功能
  togglebox(){
    let checked = !this.data.toggleChecked;
    this.data.goodsList.forEach(item=>{
      item.checked = checked
    })

    this.setData({
      "goodsList":this.data.goodsList,
      "toggleChecked": checked,
      slide:null
    })
    this.totalData();//计算总价格
  },
  // 判断是否全选
  idToggleBox(){
    let checked = this.data.goodsList.every(item =>item.checked);//只要一个没有选中false
    this.setData({
      "toggleChecked": checked,
      slide:null
    })
  },
  // 计算总价格
  totalData(){
    let num = 0;
    let data = this.data.goodsList.reduce((total,item)=>{
      if(item.checked){
        num++;
        return total += item.price * item.num;
      }else{
        return total
      }
    },0)
    this.setData({
      total:data,
      goodsNum:num
    })
  },


  //商品推荐
  getlistFun(){
    recommendBlank().then(data=>{
      console.log(data);
      let recom_list = data.data.recom_list;
      let listsArr = [];
      recom_list.forEach(item=>{
          listsArr.push({
            id:item.action.path,
            img:item.image_url,
            name:item.name,
            price:item.price,
            priceOld:item.market_price
        })
      })
      this.data.lists = listsArr
      this.setData(this.data)
      console.log(this.data.lists,'商品推荐');
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getlistFun()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // 获取缓存数据
    let goodsCarList = wx.getStorageSync('goodsCarList');
    this.setData({
      goodsList:goodsCarList
    })
    this.idToggleBox(); //全选按钮
    this.totalData(); //总数量
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    wx.setStorageSync('goodsCarList', this.data.goodsList)
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})