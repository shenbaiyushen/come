// pages/category/category.js

let {
  getCategoryData
} = require("../../api/category")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    itemIndex: 0,
    catelist: null,
    // titlelist:null,
    // grouplist:null,
    tablist: [{
        "tagName": "Xiaomi手机",
        "id": "0"
      },
      {
        "tagName": "智能穿戴",
        "id": "1"
      },
      {
        "tagName": "电视",
        "id": "2"
      },
      {
        "tagName": "大家电",
        "id": "3"
      },
      {
        "tagName": "小家电",
        "id": "4"
      },
      {
        "tagName": "智能家居",
        "id": "5"
      },
      {
        "tagName": "出行运动",
        "id": "6"
      },
      {
        "tagName": "日用百货",
        "id": "7"
      },
      {
        "tagName": "儿童用品",
        "id": "8"
      },
      {
        "tagName": "有品精选",
        "id": "9"
      },
      {
        "tagName": "小米服务",
        "id": "10"
      }
    ]
  },



  // 切换效果
  setItem(event) {
    let index = event.currentTarget.dataset.index;
    this.setData({
      itemIndex: index
    })
  },
    getlistFun(){
      getCategoryData().then((data) => {
        // console.log(data.data);
        data.data.splice(2, 3)
        data.data.splice(0, 1)
        this.setData({
          catelist: data.data,
        })
      }) 
       let num = 0
      if (wx.getStorageSync('btn_index')) {
       num = wx.getStorageSync('btn_index')
     }else{
       num
     }
      this.setData({
        itemIndex:num
      })
    },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getlistFun()
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    console.log("离开");
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.setlistFun()
  },

  setlistFun(){
 // let _this = this; //将this传递给变量
 console.log(wx.getStorageSync('btn_index'),'111');
 let num = wx.getStorageSync('btn_index')
 this.setData({
   itemIndex:num
 })
 console.log( this.data.itemIndex,"564545");
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    console.log(wx.getStorageSync('btn_index'),'222');
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    console.log(wx.getStorageSync('btn_index'),'333');

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})