// pages/search/search.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    find_list:[//搜索发现的数据
      '全部商品','手机','笔记本','冰箱','洗衣机','耳机','电视','显示器'
    ],
    history_list:[//搜索历史数据

    ],
    value:""
  },
//返回上一个路由
  close(){
    wx.navigateBack({
      delta:1,
    })
  },
  // 输入框数据
  inputFun(e){
    if (e.detail.value == '') {
      wx.showToast({
        title: '请输入你要搜索的内容',
        icon:"none",
        duration: 2000
      })
      return
    }
      // 调用方法
   this.coder_pushFun(e.detail.value)
    this.getList()
  },

  //调用存储方法
  coder_pushFun(name){
      //存储数据
        let loacl_list = wx.getStorageSync('search')
        if (loacl_list && loacl_list != null) {
          loacl_list.push(name)
            wx.setStorageSync('search', loacl_list)
          }else{
            wx.setStorageSync('search', [name])
          }
          //跳转页面
        wx.navigateTo({
          url: '/pages/searchResult/searchResult?value='+name,
        })
  },
  // 清除历史数据
  deldataFun(){
    console.log(11);

    wx.removeStorageSync("search")
    this.getList()
  },

// 点击搜索发现去搜索数据
btnFun(e){
    this.setData({
      value:this.data.find_list[e.target.dataset.id]
    })

      // 调用方法
    this.coder_pushFun(this.data.find_list[e.target.dataset.id])
    this.getList()
  },
  
  // 点击历史记录添加
  historyFun(e){
    console.log(this.data.history_list[e.target.dataset.id]);
      this.setData({
        value:this.data.history_list[e.target.dataset.id],
      })
      // 调用方法
      this.coder_pushFun(this.data.history_list[e.target.dataset.id])
      this.getList()
  },

  //进行存储
  getList(){
    let loacl_list = wx.getStorageSync('search')
    // 进行数据去重
    for (let i = 0; i < loacl_list.length; i++) {
      for (let j = i + 1; j < loacl_list.length; j++) {
        if (loacl_list[i] === loacl_list[j]) {
          loacl_list.splice(j, 1);
        }
      }
    }
      this.setData({
        history_list:loacl_list
      })
  console.log(this.data.history_list,'数据');
  },
  /**
   * 生命周期函数--监听页面加载
   */



  onLoad(options) {
   this.getList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})