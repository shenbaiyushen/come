// pages/home/home.js

let {getHomeData,recommendBlank,homeHisearch} = require("../../api/home")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    swiperHeight:200,
      imgs:[
        "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/51b3f561b860023b9739c9ef2ad4be9e.jpg",
        "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d80ff6ad33796a3c5eb7434fe5fb00a0.jpg?f=webp&w=1080&h=540&bg=F5EEE6",
        "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/51b3f561b860023b9739c9ef2ad4be9e.jpg",
        "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/51b3f561b860023b9739c9ef2ad4be9e.jpg",
        "https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/51b3f561b860023b9739c9ef2ad4be9e.jpg",
      ],
      current:0,
      category:[
        {
          img:"/image/01_03.gif",
          text:"精品推荐"
        },
        {
          img:"/image/01_05.gif",
          text:"手机"
        },
        {
          img:"/image/01_07.gif",
          text:"电脑"
        },
        {
          img:"/image/01_09.gif",
          text:"智能穿戴"
        },
        {
          img:"/image/01_11.gif",
          text:"电视"
        },
        {
          img:"/image/01_18.gif",
          text:"大家电"
        },
        {
          img:"/image/01_19.gif",
          text:"智能家居"
        },
        {
          img:"/image/01_20.gif",
          text:"出行运动"
        },
        {
          img:"/image/01_21.gif",
          text:"日用百货"
        },
        {
          img:"/image/01_22.png",
          text:"儿童用品"
        },
      ],
      conIndex:0,
      swiper_height:"200px",
      goodsLists:[
        {
          name:"精选",
          title:"商品推荐",
          lists:[
            // {
                // id:"",
            //   img:"https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d9d7f31258a1f84a277918cecab14aaa.jpg?f=webp&w=516&h=420",
            //   name:"小米手机",
            //   price:29,
            //   priceOld:129
            // }
          ]
        },
        {
          name:"手机",
          title:"探索黑科技",
          lists:[]
        },
        {
          name:"电视",
          title:"数码电器",
          lists:[]
        },
      ]
  },

  // 商品 选项卡切换
  setConIndex(event){
    console.log(event.currentTarget.dataset.id);
    let index = event.currentTarget.dataset.id
    this.setData({
      conIndex:index,
    })

    this.getNavData(index);
  },
  // 轮播图商品切换
  setSwiperGoodsIndex(event){
    console.log("触发效果",event.detail.current);
    this.setData({
      conIndex:event.detail.current,
    })
    this.autoHeight();
  },

  // 获取 手机 家电 数据
  getNavData(index){
    if(this.data.goodsLists[index].lists.length==0){ //没有数据才请求数据
      homeHisearch({query:this.data.goodsLists[index].name}).then((data)=>{
        console.log(data);
        let list_v2 = data.data.list_v2;
        let listsArr = [];
        list_v2.forEach(item=>{
            listsArr.push({
              id:item.body.product_id,
              img:item.body.image,
              name:item.body.name,
              price:item.body.price,
              priceOld:item.body.market_price
          })
        })
        console.log("数据列表==>",listsArr);
        this.setData({
          ["goodsLists["+index+"].lists"]:listsArr
        })


      }).then(()=>{
        this.autoHeight();
      })
    }


    
  },

  // 计算内容高度
  autoHeight() {

    let {conIndex} = this.data;
 
      wx.createSelectorQuery()
        .select('#end' + conIndex).boundingClientRect()
        .select('#start' + conIndex).boundingClientRect().exec(rect => {
          console.log(rect);
          let _space = rect[0].top - rect[1].top;//end高度-start高度=差值
          _space =  _space + 'px';
 
          this.setData({
            swiper_height: _space
          });
 
        })
 
  },

  // 自定义方法
  // 轮播图指示点
  setSwiperIndex(event){
    console.log("触发效果",event.detail.current);
    // this.setData 修改data中数据,数据修改后更新视图
    this.setData({
      current:event.detail.current
    })
  },

  // 获取图片高度
  bindloadImg(e){
    let {height,width} = e.detail;
    // 获取硬件系统信息
    wx.getSystemInfo({
      success:(res)=>{
        this.setData({
          swiperHeight:res.windowWidth *0.95 / width * height
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    getHomeData().then(data=>{
      let sections = data.data.data.sections;
      this.setData({
        imgs:sections[0].body.items
      })
    })
    recommendBlank().then(data=>{
      console.log(data);
      let recom_list = data.data.recom_list;
      let listsArr = [];
      recom_list.forEach(item=>{
          listsArr.push({
            id:item.action.path,
            img:item.image_url,
            name:item.name,
            price:item.price,
            priceOld:item.market_price
        })
      })
      this.setData({
        "goodsLists[0].lists":listsArr
      })
    }).then(()=>{
      this.autoHeight();
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    console.log("onReady");

  },

  //跳转到分类页面得下标
  btn_indexFun(e){
    let key = e.target.dataset.id
    console.log(1);
    wx.switchTab({
      url: "/pages/category/category",
    })
    wx.setStorageSync('btn_index', key)
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.autoHeight();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    console.log("onHide");

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})