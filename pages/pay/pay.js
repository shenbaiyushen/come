// pages/pay/pay.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    total:0,
    goodsList:[
      // {
      //   id:1,
      //   name:"小米手机",
      //   img:"https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d9d7f31258a1f84a277918cecab14aaa.jpg?f=webp&w=516&h=420",
      //   price:"2999",
      //   priceOld:"3999",
      //   num:1,
      //   checked:false
      // },
      // {
      //   id:2,
      //   name:"小米手机2",
      //   img:"https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/d9d7f31258a1f84a277918cecab14aaa.jpg?f=webp&w=516&h=420",
      //   price:"2999",
      //   priceOld:"3999",
      //   num:1,
      //   checked:false
      // },
    ],
    lists:[]
  },



  // 计算总价格
  totalData(){
    let num = 0;
    let data = this.data.goodsList.reduce((total,item)=>{
      if(item.checked){
        num++;
        return total += item.price * item.num;
      }else{
        return total
      }
    },0)
    this.setData({
      total:data,
    })
  },
  // 跳转订单页
  goOrder(){
    wx.showModal({
      title: '支付商品',
      success:(res)=>{
        if (res.confirm) {
          // console.log('用户点击确定')
          this.setOrderList(2);//生成订单--待发货
          wx.reLaunch({
            url: '/pages/order/order',
          })
        } else if (res.cancel) {
          // console.log('用户点击取消')
          this.setOrderList(1);//生成订单--待支付
          wx.reLaunch({
            url: '/pages/order/order',
          })
        }
      }
    })
    


  },
  // 生成订单
  // 1.待支付 2.待发货 3.待收货 4.待评价
  setOrderList(type){
      
      // 构造订单数据
      let orderData = {
        orderId: new Date().getTime(),
        orderLists:this.data.goodsList,//商品列表
        type:type,
        total:this.data.total,//总价格
        address:{
            name:"张三",
            phone:"13012341234",
            address:"广东省广州市天河区XXX路XXX号"
        }
      };

      // 1.判断缓存有没有数据
      let goodsOrderList = wx.getStorageSync('goodsOrderList');
      if(goodsOrderList){
        // 2.有数据 添加数据
        goodsOrderList.unshift(orderData);
        wx.setStorageSync('goodsOrderList', goodsOrderList)

      }else{
        // 3.没有数据 添加订单
        wx.setStorageSync('goodsOrderList', [orderData])
      }


      // 删除订单数据
      let goodsCarList = wx.getStorageSync('goodsCarList');
      let newlists = goodsCarList.filter(item=>!item.checked); //过滤加入订单数据
      wx.setStorageSync('goodsCarList', newlists)

  },

  // 初始化数据
  getlistFun(){
    if (wx.getStorageSync('aress') && wx.getStorageSync('aress') != null) {
      this.data.addressList =  wx.getStorageSync('aress')
      this.data.addressList.forEach(item=>{
        if (item.aess == true) {
          this.data.lists.push(item)
          this.setData(this.data)
        }
      })
    }else{
      this.data.lists
    }
    console.log(this.data.lists,'收货地址初始化数据');
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getlistFun()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // 获取缓存数据
    let goodsCarList = wx.getStorageSync('goodsCarList');
    let dataLists = goodsCarList.filter(item=>item.checked);
    this.setData({
      goodsList:dataLists
    })

    this.totalData();//计算总价格
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})