// pages/searchResult/searchResult.js

let {getMiproduct} = require("../../api/cearchList")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    sreach_list:null,
    value:'',
    lists:[
      {
      img:"https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202207012000_0b9df066c110f201154013ac373df1d9.png",
      name:'小米手机'
      },
        {
          img:"	https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202208122039_eb4948da80a51be1aff73ba182342eca.jpg",
          name:'卫衣'
          },
          {
            img:"	https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/f794e6cf74dc9e3c929f49133ad8a0ee.png",
            name:'充电器'
            },
            {
              img:"https://cdn.cnbj1.fds.api.mi-img.com/mi-mall/a1c2f57d1f09ebb6f8597df1d1e280f6.png",
              name:'打印机'
              },
              {
                img:"	https://cdn.cnbj1.fds.api.mi-img.com/nr-pub/202208112039_cb21539a05b3926928b9c4d8295f2c57.png",
                name:'红米手机'
                },
            
    ],
    sort_index:0,//排序下标
    guide_index:-1,//推荐下标
    valueA:'',
    valueB:'',
    flag:false,
  },

  delFun(){
    this.data.flag = !this.data.flag
    this.setData(this.data)
  },

  // 价格比对
  maxinputFun(e){
    console.log(e.target.dataset.name);
    console.log(e.detail.value);
    this.setData({
      [e.target.dataset.name]:e.detail.value,
    })
  },
  //确认按钮
  btninpoutFun(){
    let datalists = this.data.sreach_list
    this.data.sreach_list = []
    datalists.forEach(item=>{
      if (Number(item.body.price)  > this.data.valueA && Number(item.body.price)  < this.data.valueB ) {
        this.data.sreach_list.push(item)
      }
    })
    console.log(this.data.sreach_list);
    this.data.flag = false
    this.setData(this.data)
  },
  // 重置按钮
  chongFun(){
    this.data.flag = false
    this.data.valueA = ""
    this.data.valueB = ""
    this.setData(this.data)
    this.getcraetedFun(this.data.value)
  },

  // 输入框数据
  inputFun(e){
    if (e.detail.value == '') {
      wx.showToast({
        title: '请输入你要搜索的内容',
        icon:"none",
        duration: 2000
      })
      return
    }
    this.setData({
      guide_index:-1
    })
    this.getcraetedFun(e.detail.value)
  },

  addFun(e){
    console.log(e.target.dataset.id);
    this.data.guide_index = e.target.dataset.id
    this.setData(this.data)
    this.getcraetedFun(this.data.lists[e.target.dataset.id].name)
  },

    //初始化数据
  getcraetedFun(value){
    this.setData({
      value:value
    })
    getMiproduct(value).then(data=>{
      let lists = data.data.list_v2
      console.log(lists[0].body.image,'接口数据');
      this.setData({
        sreach_list:lists,
        value:value
      })
      console.log(this.data.sreach_list);
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getcraetedFun(options.value)
  
  },
//排序
  cutFun(e){
    this.data.sort_index = e.currentTarget.dataset.id
    this.setData(this.data)
    // console.log(e.currentTarget.dataset.id,'切换排序');
    if (e.currentTarget.dataset.id == 0){
      for (let i = 0; i < this.data.sreach_list.length; i++) {
        for (let j = 0; j < this.data.sreach_list.length - i - 1; j++) {
          if (
            this.data.sreach_list[j].body.product_id <
           this.data.sreach_list[j + 1].body.product_id
          ) {
            let num = this.data.sreach_list[j];
            this.data.sreach_list[j] = this.data.sreach_list[j + 1];
            this.data.sreach_list[j + 1] = num;
          }
        }
      }
      this.setData(this.data)
    }else if (e.currentTarget.dataset.id == 1) {
      for (let i = 0; i < this.data.sreach_list.length; i++) {
        for (let j = 0; j < this.data.sreach_list.length - i - 1; j++) {
          if (
            this.data.sreach_list[j].body.comments_total <
           this.data.sreach_list[j + 1].body.comments_total
          ) {
            let num = this.data.sreach_list[j];
            this.data.sreach_list[j] = this.data.sreach_list[j + 1];
            this.data.sreach_list[j + 1] = num;
          }
        }
      }
      this.setData(this.data)
    }else  if (e.currentTarget.dataset.id == 2) {
        for (let i = 0; i < this.data.sreach_list.length; i++) {
          for (let j = 0; j < this.data.sreach_list.length - i - 1; j++) {
            if (
              Number(
              this.data.sreach_list[j].body.price)>
              Number(
             this.data.sreach_list[j + 1].body.price)
            ) {
              let num = this.data.sreach_list[j];
              this.data.sreach_list[j] = this.data.sreach_list[j + 1];
              this.data.sreach_list[j + 1] = num;
            }
          }
        }
        this.setData(this.data)
      }else if (e.currentTarget.dataset.id == 3) {
        this.setData({
          flag:true,
        })
      }
  },




  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})